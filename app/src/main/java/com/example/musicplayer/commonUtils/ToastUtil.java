package com.example.musicplayer.commonUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.example.musicplayer.MusicPlayerApplication;

public class ToastUtil {
    private Toast mToast;
    private String mText;
    private Context mContext;

    public ToastUtil(Context context) {
        this(context, null);
    }

    public ToastUtil(Context context, String text) {
        this(context, text, Toast.LENGTH_SHORT);
    }

    @SuppressLint("ShowToast")
    public ToastUtil(Context context, String text, int duration) {
        mContext = context;
        mToast = Toast.makeText(mContext, text, duration);
    }

    public ToastUtil(Context context, int duration, View view) {
        mContext = context;
        mToast = new Toast(mContext);
        mToast.setDuration(duration);
        setView(view);
    }

    public ToastUtil setText(String text) {
        if (text == null) {
            mText = "";
        } else {
            mText = text;
        }
        mToast.setText(mText);
        return this;
    }

    public String getText() {
        return mText;
    }

    public ToastUtil setView(View view) {
        mToast.setView(view);
        return this;
    }

    public ToastUtil longShow() {
        mToast.setDuration(Toast.LENGTH_LONG);
        return this;
    }

    public ToastUtil shortShow() {
        mToast.setDuration(Toast.LENGTH_SHORT);
        return this;
    }

    public ToastUtil setGravity(int gravity, int xOff, int yOff) {
        mToast.setGravity(gravity, xOff, yOff);
        return this;
    }

    public ToastUtil setAlpha(float alpha) {
        View view = mToast.getView();
        if (view != null) {
            if (alpha < 0.0f)
                alpha = 0.0f;
            else if (alpha > 1.0f)
                alpha = 1.0f;
            view.setAlpha(alpha);
        }
        return this;
    }

    public ToastUtil show() {
        mToast.show();
        return this;
    }

    public void cancel() {
        mToast.cancel();
    }

    public static void makeToast(String text) {
        makeToast(text, false);
    }

    @SuppressLint("ShowToast")
    public static void makeToast(String text, boolean isLongShow) {
        Context context = MusicPlayerApplication.getInstance();
        Toast toast;
        if (isLongShow) {
            toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
        } else {
            toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        }
        if (toast != null) {
            toast.setGravity(Gravity.BOTTOM, 0, 360);
            toast.show();
        }
    }
}