package com.example.musicplayer.commonUtils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.example.musicplayer.R;
import com.example.musicplayer.musicClass.PlayingNotificationInfo;
import com.example.musicplayer.service.MusicService;

//音乐播放的前台服务
public class PlayingNotification {
    private Context mContext;
    private MusicService mMusicService;
    private NotificationManager mNotificationManager;
    //点击通知跳转到的activity
    private PendingIntent mClickPendingIntent;
    //这四个表示通知按键点击的处理事件
    private PendingIntent mPlayPendingIntent;
    private PendingIntent mLastPendingIntent;
    private PendingIntent mNextPendingIntent;
    private PendingIntent mFavoritePendingIntent;
    //通知被删除时的处理
    private PendingIntent mCancelPendingIntent;
    private static final String CHANNEL_ID = "com.example.musicplayer.MusicService";
    private static final String CHANNEL_NAME = "channel1";
    private static final int FOREGROUND_ID = 1;
    //判断是否第一次显示通知
    private boolean mFirst;

    public PlayingNotification(Context context, MusicService musicService) {
        mContext = context;
        mMusicService = musicService;
        mFirst = true;
        init();
    }

    //更新通知
    public void sendNotification(PlayingNotificationInfo info, boolean isForeground) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, CHANNEL_ID);
            builder.setSmallIcon(info.getSmallIcon());
            builder.setContentTitle(info.getContentTitle());
            builder.setContentText(info.getContentText());
            builder.setShowWhen(false);
            builder.setContentIntent(mClickPendingIntent);
            builder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            builder.setLargeIcon(info.getLargeIcon());
            builder.addAction(info.getDrawableFavorite(), "favorite", mFavoritePendingIntent);
            builder.addAction(info.getDrawableLast(), "last", mLastPendingIntent);
            builder.addAction(info.getDrawablePlay(), "play", mPlayPendingIntent);
            builder.addAction(info.getDrawableNext(), "next", mNextPendingIntent);
            builder.setDeleteIntent(mCancelPendingIntent);
            //设置系统自带的多媒体样式
            androidx.media.app.NotificationCompat.MediaStyle style = new androidx.media.app.NotificationCompat.MediaStyle();
            style.setShowActionsInCompactView(1, 2, 3);
            builder.setStyle(style);
            Notification notification = builder.build();
            if (isForeground) {
                mNotificationManager.notify(FOREGROUND_ID, notification);
                mMusicService.startForeground(FOREGROUND_ID, notification);
            } else {
                mNotificationManager.notify(FOREGROUND_ID, notification);
                mMusicService.stopForeground(Service.STOP_FOREGROUND_DETACH);
            }
            //发出通知，改变发送状态
            mFirst = false;
        }
    }

    //删除通知，结束前台服务
    public void destroyNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.deleteNotificationChannel(CHANNEL_ID);
            mMusicService.stopForeground(Service.STOP_FOREGROUND_REMOVE);
        }
    }

    //重置通知发送的状态
    public void resetNotificationState() {
        mFirst = true;
    }

    //通知的一些初始化操作
    private void init() {
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        mClickPendingIntent = PendingIntent.getBroadcast(mContext, 0,
                new Intent(mContext.getResources().getString(R.string.notification_click)), 0);

        mFavoritePendingIntent = PendingIntent.getBroadcast(mContext, 0,
                new Intent(mContext.getResources().getString(R.string.notification_favorite)), 0);

        mLastPendingIntent = PendingIntent.getBroadcast(mContext, 0,
                new Intent(mContext.getResources().getString(R.string.notification_last)), 0);

        mPlayPendingIntent = PendingIntent.getBroadcast(mContext, 0,
                new Intent(mContext.getResources().getString(R.string.notification_play)), 0);

        mNextPendingIntent = PendingIntent.getBroadcast(mContext, 0,
                new Intent(mContext.getResources().getString(R.string.notification_next)), 0);

        mCancelPendingIntent = PendingIntent.getBroadcast(mContext, 0,
                new Intent(mContext.getResources().getString(R.string.notification_cancel)), 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
    }
}