package com.example.musicplayer.commonUtils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Outline;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.view.WindowManager;

import com.example.musicplayer.MusicPlayerApplication;

//dp,sp,px转换工具类
public class DisplayUtil {
    //px转换成dp
    public static int px2dp(Context context, float value) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (value / scale + 0.5f);
    }

    //dp转换成px
    public static int dp2px(Context context, float value) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (value * scale + 0.5f);
    }

    //px转换成sp
    public static int px2sp(Context context, float value) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (value / fontScale + 0.5f);
    }

    //sp转换成px
    public static int sp2px(Context context, float value) {
        final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
        return (int) (value * fontScale + 0.5f);
    }

    //dp转换成sp
    public static int dp2sp(Context context, float value) {
        int px = dp2px(context, value);
        return px2sp(context, px);
    }

    //sp转换成dp
    public static int sp2dp(Context context, float value) {
        int px = sp2px(context, value);
        return px2dp(context, px);
    }

    //返回屏幕的尺寸大小
    public static int[] getScreenSize(Activity activity) {
        int[] screenSize = new int[2];
        try {
            Display display = activity.getWindowManager().getDefaultDisplay();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            screenSize[0] = metrics.widthPixels;
            screenSize[1] = metrics.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return screenSize;
    }

    //屏幕尺寸大小（包含状态栏和导航栏）
    public static int[] getRealScreenSize(Activity activity) {
        int[] screenSize = getScreenSize(activity);
        screenSize[1] = screenSize[1] + getStatusBarHeight(activity.getResources()) +
                getNavigationBarHeight(activity.getResources());
        return screenSize;
    }

    //获取状态栏高度
    public static int getStatusBarHeight(Resources resources) {
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    //获取导航栏高度
    public static int getNavigationBarHeight(Resources resources) {
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        return resources.getDimensionPixelSize(resourceId);
    }

    //设置圆角轮廓
    public static void setRoundRectOutline(View view, final int left, final int top,
                                           final int right, final int bottom, final float radius) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setRoundRect(left, top, right, bottom, radius);
                }
            });
            view.setClipToOutline(true);
        }
    }

    //设置椭圆轮廓
    public static void setRoundOutline(View view, final int left, final int top,
                                       final int right, final int bottom) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.setOutlineProvider(new ViewOutlineProvider() {
                @Override
                public void getOutline(View view, Outline outline) {
                    outline.setOval(left, top, right, bottom);
                }
            });
            view.setClipToOutline(true);
        }
    }

    public static void setDialogWindowType(Window dialogWindow) {
        if (dialogWindow != null) {
            if (MusicPlayerApplication.canDrawOverlays()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    dialogWindow.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                } else {
                    dialogWindow.setType(WindowManager.LayoutParams.TYPE_PHONE);
                }
            }
        }
    }
}