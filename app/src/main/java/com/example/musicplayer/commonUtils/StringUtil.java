package com.example.musicplayer.commonUtils;

import android.annotation.SuppressLint;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class StringUtil {
    private static final String PATTERN = "mm:ss";
    private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    //格式化音乐播放显示的时间
    public static String getMusicFormatTime(int time) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(PATTERN);
        return sdf.format(time);
    }

    public static String getFormatTime(int time) {
        String formatTime = "";
        //hour
        int h = time / (60 * 60 * 1000);
        if (h > 0) {
            formatTime += h;
            if (h <= 9) {
                formatTime = "0" + formatTime;
            }
            formatTime += ":";
        }
        //minute
        time = time % (60 * 60 * 1000);
        int m = time / (60 * 1000);
        if (m == 0) {
            formatTime += "00";
        } else if (m <= 9) {
            formatTime = formatTime + "0" + m;
        } else {
            formatTime += m;
        }
        formatTime += ":";
        //second
        time = time % (60 * 1000);
        int s = time / 1000;
        if (s == 0) {
            formatTime += "00";
        } else if (s <= 9) {
            formatTime = formatTime + "0" + s;
        } else {
            formatTime += s;
        }
        return formatTime;
    }

    public static String getFormatDate() {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
        return sdf.format(new Date());
    }

    public static String getFormatDate(long time) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);
        return sdf.format(time);
    }

    public static String getFormatDate(long time, String pattern) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(time);
    }

    //pattern is "HH:mm", if date1 > date2, return true
    public static boolean compareTwoDate(String date1, String date2) {
        int h1 = Integer.parseInt(date1.substring(0, 2));
        int h2 = Integer.parseInt(date2.substring(0, 2));
        if (h1 > h2) {
            return true;
        } else if (h1 < h2) {
            return false;
        } else {
            int m1 = Integer.parseInt(date1.substring(3, 5));
            int m2 = Integer.parseInt(date2.substring(3, 5));
            return m1 > m2;
        }
    }

    //得到指定范围的随机整数
    public static int getRandom(int start, int end) {
        Random ran = new Random();
        if (start > end)
            return 0;
        return ran.nextInt(end - start + 1) + start;
    }

    public static String ByteToHexString(byte[] array) {
        if (array == null) {
            return null;
        }
        StringBuilder string = new StringBuilder();
        for (byte b : array) {
            String hex = Integer.toHexString(b & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            string.append(hex.toUpperCase());
        }
        return string.toString();
    }

    public static byte[] HexStringToByte(String hexString) {
        if (hexString == null || hexString.length() < 1) {
            return null;
        }
        if (hexString.length() % 2 == 1) {
            hexString = "0" + hexString;
        }
        byte[] array = new byte[hexString.length() / 2];
        for (int i = 0; i < hexString.length() / 2; i++) {
            int high = Integer.parseInt(hexString.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexString.substring(i * 2 + 1, i * 2 + 2), 16);
            array[i] = (byte) (high * 16 + low);
        }
        return array;
    }
}