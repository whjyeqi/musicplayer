package com.example.musicplayer.commonUtils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;

import java.util.Locale;

public class LanguageUtil {
    //中文
    public static final int CHINESE = 0;
    //英语
    public static final int ENGLISH = 1;
    //日语
    public static final int JAPANESE = 2;
    //韩语
    public static final int KOREAN = 3;
    //泰语
    public static final int THAI = 4;
    //法语
    public static final int FRENCH = 5;
    //当前设置的语言
    private static int sLanguage = CHINESE;

    //更新语言
    public static Context loadLanguage(Context context) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        Locale locale;
        switch (sLanguage) {
            case CHINESE:
                locale = Locale.CHINESE;
                break;
            case ENGLISH:
                locale = Locale.ENGLISH;
                break;
            case JAPANESE:
                locale = Locale.JAPANESE;
                break;
            case KOREAN:
                locale = Locale.KOREAN;
                break;
            case THAI:
                locale = new Locale("th");
                break;
            case FRENCH:
                locale = Locale.FRENCH;
                break;
            default:
                sLanguage = CHINESE;
                locale = Locale.CHINESE;
                break;
        }

        configuration.setLocale(locale);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration.setLocales(new LocaleList(locale));
            return context.createConfigurationContext(configuration);
        } else {
            resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        }

        return context;
    }

    //设置当前语言
    public static void setLanguage(int language) {
        if (hasLanguage(language))
            sLanguage = language;
        else
            sLanguage = CHINESE;
    }

    //返回当前设置的语言
    public static int getLanguage() {
        return sLanguage;
    }

    //是否包含某种语言
    public static boolean hasLanguage(int language) {
        switch (language) {
            case CHINESE:
            case ENGLISH:
            case JAPANESE:
            case KOREAN:
            case THAI:
            case FRENCH:
                return true;
            default:
                return false;
        }
    }
}