package com.example.musicplayer.fragment;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.adapter.BaseFragmentPagerAdapter;
import com.example.musicplayer.commonUtils.DisplayUtil;
import com.example.musicplayer.commonUtils.TabUnderLineUtil;
import com.example.musicplayer.fragment.activityMainFragmentTab2.Tab1Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab2.Tab2Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab2.Tab3Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab2.Tab4Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab2.Tab5Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab2.Tab6Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab2.Tab7Fragment;
import com.example.musicplayer.settings.MainActivitySettings;

import java.util.ArrayList;
import java.util.List;

public class MainTab2Fragment extends Fragment implements View.OnClickListener {
    private HorizontalScrollView mScrollViewTopBar;
    private List<TextView> mTextViews = new ArrayList<TextView>();
    private ImageView mImageViewLine;
    private List<Fragment> mFragments = new ArrayList<Fragment>();
    private ViewPager mViewPager;
    private TabUnderLineUtil mUnderLineUtil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_tab2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        addListener();
    }

    private void initView() {
        if (getActivity() != null) {
            mScrollViewTopBar = getActivity().findViewById(R.id.scrollview_activity_main_tab2_top_bar);
            TextView TextViewTab1 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab2_tab1);
            TextView TextViewTab2 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab2_tab2);
            TextView TextViewTab3 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab2_tab3);
            TextView TextViewTab4 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab2_tab4);
            TextView TextViewTab5 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab2_tab5);
            TextView TextViewTab6 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab2_tab6);
            TextView TextViewTab7 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab2_tab7);
            mImageViewLine = getActivity().findViewById(R.id.imageview_activity_main_fragment_tab2_line);
            Tab1Fragment Tab1Fragment = new Tab1Fragment();
            Tab2Fragment Tab2Fragment = new Tab2Fragment();
            Tab3Fragment Tab3Fragment = new Tab3Fragment();
            Tab4Fragment Tab4Fragment = new Tab4Fragment();
            Tab5Fragment Tab5Fragment = new Tab5Fragment();
            Tab6Fragment Tab6Fragment = new Tab6Fragment();
            Tab7Fragment Tab7Fragment = new Tab7Fragment();
            mViewPager = getActivity().findViewById(R.id.viewpager_activity_main_fragment_tab2);

            mTextViews.add(TextViewTab1);
            mTextViews.add(TextViewTab2);
            mTextViews.add(TextViewTab3);
            mTextViews.add(TextViewTab4);
            mTextViews.add(TextViewTab5);
            mTextViews.add(TextViewTab6);
            mTextViews.add(TextViewTab7);
            mFragments.add(Tab1Fragment);
            mFragments.add(Tab2Fragment);
            mFragments.add(Tab3Fragment);
            mFragments.add(Tab4Fragment);
            mFragments.add(Tab5Fragment);
            mFragments.add(Tab6Fragment);
            mFragments.add(Tab7Fragment);
            mViewPager.setAdapter(new BaseFragmentPagerAdapter(getActivity().getSupportFragmentManager(), 1, mFragments));
            mViewPager.setCurrentItem(0);
            updateTopTab(0);
        }
    }

    private void addListener() {
        for (TextView view : mTextViews)
            view.setOnClickListener(this);
        //第一个textview监听，当布局发生改变时，可获取它的宽度，立即初始化tab的下划线
        mTextViews.get(0).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mUnderLineUtil = new TabUnderLineUtil(mImageViewLine, mTextViews, 0);
                //完成后，移除监听器
                mTextViews.get(0).getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (mUnderLineUtil != null)
                    mUnderLineUtil.updateUnderLine(position, positionOffset);
            }

            @Override
            public void onPageSelected(int position) {
                updateTopTab(position);
                //判断当前tab是否超出范围，并滚动scrollview
                //gap表示选中的tab距离屏幕左右边的最小距离
                int gap = MainActivitySettings.getGapToEdge();
                int[] loc = new int[2];
                //屏幕的宽高
                int[] screenSize = DisplayUtil.getScreenSize(getActivity());
                //得到在屏幕上的位置
                mTextViews.get(position).getLocationOnScreen(loc);
                int width = mTextViews.get(position).getWidth();
                if (loc[0] + width >= screenSize[0] - gap)
                    mScrollViewTopBar.smoothScrollBy(loc[0] + width - screenSize[0] + gap, 0);
                else if (loc[0] < gap)
                    mScrollViewTopBar.smoothScrollBy(loc[0] - gap, 0);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //更新顶部栏的选择状态
    private void updateTopTab(int position) {
        for (int i = 0; i < mTextViews.size(); i++) {
            if (i == position)
                setTabStatus(mTextViews.get(i), true);
            else
                setTabStatus(mTextViews.get(i), false);
        }
    }

    //更改选择栏的状态
    private void setTabStatus(TextView view, boolean selected) {
        if (selected) {
            view.setTextSize(getResources().getInteger(R.integer.activity_main_viewpager_tab_selected));
            view.setTextColor(getResources().getColor(R.color.activity_main_viewpager_tab_selected));
            view.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        } else {
            view.setTextSize(getResources().getInteger(R.integer.activity_main_viewpager_tab_not_selected));
            view.setTextColor(getResources().getColor(R.color.activity_main_viewpager_tab_not_selected));
            view.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textview_activity_main_fragment_tab2_tab1:
                if (mViewPager.getCurrentItem() != 0) {
                    mViewPager.setCurrentItem(0);
                    updateTopTab(0);
                }
                break;
            case R.id.textview_activity_main_fragment_tab2_tab2:
                if (mViewPager.getCurrentItem() != 1) {
                    mViewPager.setCurrentItem(1);
                    updateTopTab(1);
                }
                break;
            case R.id.textview_activity_main_fragment_tab2_tab3:
                if (mViewPager.getCurrentItem() != 2) {
                    mViewPager.setCurrentItem(2);
                    updateTopTab(2);
                }
                break;
            case R.id.textview_activity_main_fragment_tab2_tab4:
                if (mViewPager.getCurrentItem() != 3) {
                    mViewPager.setCurrentItem(3);
                    updateTopTab(3);
                }
                break;
            case R.id.textview_activity_main_fragment_tab2_tab5:
                if (mViewPager.getCurrentItem() != 4) {
                    mViewPager.setCurrentItem(4);
                    updateTopTab(4);
                }
                break;
            case R.id.textview_activity_main_fragment_tab2_tab6:
                if (mViewPager.getCurrentItem() != 5) {
                    mViewPager.setCurrentItem(5);
                    updateTopTab(5);
                }
                break;
            case R.id.textview_activity_main_fragment_tab2_tab7:
                if (mViewPager.getCurrentItem() != 6) {
                    mViewPager.setCurrentItem(6);
                    updateTopTab(6);
                }
                break;
            default:
                break;
        }
    }
}