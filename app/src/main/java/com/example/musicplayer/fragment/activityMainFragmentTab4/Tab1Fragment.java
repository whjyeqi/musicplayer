package com.example.musicplayer.fragment.activityMainFragmentTab4;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.activity.MusicListActivity;
import com.example.musicplayer.activity.MusicMenuActivity;
import com.example.musicplayer.musicTools.MusicListType;
import com.example.musicplayer.musicClass.MusicInfo;

public class Tab1Fragment extends Fragment implements View.OnClickListener {
    private LinearLayout mLinearLayoutMusicListFavorite;
    private LinearLayout mLinearLayoutMusicListLocal;
    private LinearLayout mLinearLayoutMusicListMenu;
    private LinearLayout mLinearLayoutMusicListPurchased;
    private TextView mTextViewMusicListFavorite;
    private TextView mTextViewMusicListLocal;
    private TextView mTextViewMusicMenu;
    private Button mExitApp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_activity_main_fragment_tab4_tab1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        addListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        updateViewOnResume();
    }

    private void initView() {
        if (getActivity() != null) {
            mLinearLayoutMusicListFavorite = getActivity().findViewById(R.id.linear_layout_music_list_favorite);
            mLinearLayoutMusicListLocal = getActivity().findViewById(R.id.linear_layout_music_list_local);
            mLinearLayoutMusicListMenu = getActivity().findViewById(R.id.linear_layout_music_list_menu);
            mLinearLayoutMusicListPurchased = getActivity().findViewById(R.id.linear_layout_music_list_purchased);
            mTextViewMusicListFavorite = getActivity().findViewById(R.id.textview_music_list_favorite);
            mTextViewMusicListLocal = getActivity().findViewById(R.id.textview_music_list_local);
            mTextViewMusicMenu = getActivity().findViewById(R.id.textview_music_list_menu);
            mExitApp = getActivity().findViewById(R.id.button_exit);
        }
    }

    private void addListener() {
        mLinearLayoutMusicListFavorite.setOnClickListener(this);
        mLinearLayoutMusicListLocal.setOnClickListener(this);
        mLinearLayoutMusicListMenu.setOnClickListener(this);
        mLinearLayoutMusicListPurchased.setOnClickListener(this);
        mExitApp.setOnClickListener(this);
    }

    //视图每次出现时，需要的更新操作
    private void updateViewOnResume() {
        updateCountsTextView();
    }

    //更新展示歌曲和歌单数量的TextView
    private void updateCountsTextView() {
        mTextViewMusicListFavorite.setText(String.valueOf(MusicInfo.getFavoriteMusicCounts()));
        mTextViewMusicListLocal.setText(String.valueOf(MusicInfo.getLocalMusicCounts()));
        mTextViewMusicMenu.setText(String.valueOf(MusicInfo.getMusicMenuCounts()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_layout_music_list_favorite:
                doClickMusicListFavorite();
                break;
            case R.id.linear_layout_music_list_local:
                doClickMusicListLocal();
                break;
            case R.id.linear_layout_music_list_menu:
                doClickMusicListMenu();
                break;
            case R.id.linear_layout_music_list_purchased:
                doClickMusicListPurchased();
                break;
            case R.id.button_exit:
                MusicPlayerApplication.getInstance().exit();
                break;
            default:
                break;
        }
    }

    //跳转显示喜爱歌曲列表
    private void doClickMusicListFavorite() {
        startMusicListActivity(MusicListType.TYPE_FAVORITE);
    }

    //跳转显示本地歌曲列表
    private void doClickMusicListLocal() {
        startMusicListActivity(MusicListType.TYPE_LOCAL);
    }

    //跳转显示本地歌单
    private void doClickMusicListMenu() {
        Intent intent = new Intent(getActivity(), MusicMenuActivity.class);
        startActivity(intent);
        if (getActivity() != null)
            getActivity().overridePendingTransition(R.anim.normal_activity_enter, R.anim.slow_activity_exit);
    }

    //跳转显示已购买歌曲信息
    private void doClickMusicListPurchased() {
        startMusicListActivity(MusicListType.TYPE_PURCHASED);
    }

    //启动音乐列表的activity
    private void startMusicListActivity(MusicListType musicListType) {
        Intent intent = new Intent(getActivity(), MusicListActivity.class);
        intent.putExtra("musicListType", musicListType);
        startActivity(intent);
        if (getActivity() != null)
            getActivity().overridePendingTransition(R.anim.music_list_activity_enter, R.anim.fake_anim);
    }
}