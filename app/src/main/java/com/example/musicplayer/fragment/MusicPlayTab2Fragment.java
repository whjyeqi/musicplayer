package com.example.musicplayer.fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.DrawUtil;
import com.example.musicplayer.commonUtils.StringUtil;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.service.MusicService;

public class MusicPlayTab2Fragment extends Fragment implements View.OnClickListener {
    private ImageView mImageViewBig;
    private ImageView mImageViewFavorite;
    private ImageView mImageViewSing;
    private ImageView mImageViewTune;
    private ImageView mImageViewDownload;
    private ImageView mImageViewComment;
    private ImageView mImageViewMore;
    private ImageView mImageViewChange;
    private ImageView mImageViewLast;
    private ImageView mImageViewPlay;
    private ImageView mImageViewNext;
    private ImageView mImageViewList;
    private TextView mTextViewLyric;
    private TextView mTextViewTitle;
    private TextView mTextViewArtist;
    private TextView mTextViewCurrent;
    private TextView mTextViewDuration;
    private SeekBar mSeekBar;
    private Dialog mDialogBigImage;
    private ImageView mImageViewFullImage;
    //设置回调监听对象
    private CallBackListener mCallBackListener;
    private MusicService.MusicBinder mMusicBinder;
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int current = mMusicBinder.getCurrent();
                        mSeekBar.setProgress(current);
                        mTextViewCurrent.setText(StringUtil.getMusicFormatTime(current));
                        mHandler.postDelayed(mRunnable, getResources().getInteger(R.integer.refresh_time));
                    }
                });
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_music_play_tab2, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        addListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMusicBinder = mCallBackListener.getMusicBinder();
        updateView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mHandler != null)
            mHandler.removeCallbacks(mRunnable);
    }

    private void initView() {
        FragmentActivity fa = getActivity();
        mImageViewBig = fa.findViewById(R.id.imageview_music_play_tab2_big);
        mImageViewFavorite = fa.findViewById(R.id.imageview_music_play_tab2_favorite);
        mImageViewSing = fa.findViewById(R.id.imageview_music_play_tab2_sing);
        mImageViewTune = fa.findViewById(R.id.imageview_music_play_tab2_tune);
        mImageViewDownload = fa.findViewById(R.id.imageview_music_play_tab2_download);
        mImageViewComment = fa.findViewById(R.id.imageview_music_play_tab2_comment);
        mImageViewMore = fa.findViewById(R.id.imageview_music_play_tab2_more);
        mImageViewChange = fa.findViewById(R.id.imageview_music_play_tab2_change);
        mImageViewLast = fa.findViewById(R.id.imageview_music_play_tab2_last);
        mImageViewPlay = fa.findViewById(R.id.imageview_music_play_tab2_play);
        mImageViewNext = fa.findViewById(R.id.imageview_music_play_tab2_next);
        mImageViewList = fa.findViewById(R.id.imageview_music_play_tab2_list);
        mTextViewLyric = fa.findViewById(R.id.textview_music_play_tab2_lyric);
        mTextViewTitle = fa.findViewById(R.id.textview_music_play_tab2_title);
        mTextViewArtist = fa.findViewById(R.id.textview_music_play_tab2_artist);
        mTextViewCurrent = fa.findViewById(R.id.textview_music_play_tab2_current);
        mTextViewDuration = fa.findViewById(R.id.textview_music_play_tab2_duration);
        mSeekBar = fa.findViewById(R.id.seekbar_music_play);
        initDialog();
    }

    private void initDialog() {
        mDialogBigImage = new Dialog(getActivity(), R.style.FullActivity);
        WindowManager.LayoutParams attr = getActivity().getWindow().getAttributes();
        attr.width = WindowManager.LayoutParams.MATCH_PARENT;
        attr.height = WindowManager.LayoutParams.MATCH_PARENT;
        mDialogBigImage.getWindow().setAttributes(attr);
        mImageViewFullImage = getImageView();
        mDialogBigImage.setContentView(mImageViewFullImage);
        Window window = mDialogBigImage.getWindow();
        if (window != null)
            window.setWindowAnimations(R.style.DialogBigImage);
        mImageViewFullImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogBigImage.dismiss();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    getActivity().getWindow().setStatusBarColor(Color.TRANSPARENT);
                }
            }
        });
    }

    private ImageView getImageView() {
        ImageView imageView = new ImageView(getActivity());
        imageView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        return imageView;
    }

    //更新这个fragment的界面
    private void updateView() {
        updateChangingMusic();
        MusicService.PlayMode initPlayMode = MusicService.PlayMode.RANDOM;
        if (mMusicBinder != null) {
            initPlayMode = mMusicBinder.getMode();
        }
        updateModeChanged(initPlayMode, false);
    }

    //切换歌曲时更新信息
    private void updateChangingMusic() {
        if (mMusicBinder != null) {
            Bitmap bitmap = mMusicBinder.getAlbumBitmap();
            if (bitmap != null) {
                Bitmap bitmap1 = DrawUtil.copyBitmap(bitmap);
                mCallBackListener.setBackground(bitmap1);
                mImageViewBig.setImageBitmap(bitmap);
            }
            mTextViewTitle.setText(mMusicBinder.getTitle());
            mTextViewArtist.setText(mMusicBinder.getArtist());
            mTextViewDuration.setText(StringUtil.getMusicFormatTime(mMusicBinder.getDuration()));
            mTextViewCurrent.setText(StringUtil.getMusicFormatTime(mMusicBinder.getCurrent()));
            mSeekBar.setMax(mMusicBinder.getDuration());
            mSeekBar.setProgress(mMusicBinder.getCurrent());
            changeMusicFavorite(mMusicBinder.getMusicFavorite() == 1);
            changeMusicState(mMusicBinder.isPlaying());
        }
    }

    private void addListener() {
        mImageViewBig.setOnClickListener(this);
        mImageViewFavorite.setOnClickListener(this);
        mImageViewSing.setOnClickListener(this);
        mImageViewTune.setOnClickListener(this);
        mImageViewDownload.setOnClickListener(this);
        mImageViewComment.setOnClickListener(this);
        mImageViewMore.setOnClickListener(this);
        mImageViewChange.setOnClickListener(this);
        mImageViewLast.setOnClickListener(this);
        mImageViewPlay.setOnClickListener(this);
        mImageViewNext.setOnClickListener(this);
        mImageViewList.setOnClickListener(this);
        mTextViewLyric.setOnClickListener(this);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser)
                    mTextViewCurrent.setText(StringUtil.getMusicFormatTime(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mHandler.removeCallbacks(mRunnable);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mMusicBinder.seekTo(mSeekBar.getProgress());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageview_music_play_tab2_big:
                doClickPicture();
                break;
            case R.id.imageview_music_play_tab2_favorite:
                doClickFavorite();
                break;
            case R.id.imageview_music_play_tab2_sing:
                doClickSing();
                break;
            case R.id.imageview_music_play_tab2_tune:
                doClickTune();
                break;
            case R.id.imageview_music_play_tab2_download:
                doClickDownload();
                break;
            case R.id.imageview_music_play_tab2_comment:
                doClickComment();
                break;
            case R.id.imageview_music_play_tab2_more:
                doClickMore();
                break;
            case R.id.imageview_music_play_tab2_change:
                doClickChange();
                break;
            case R.id.imageview_music_play_tab2_last:
                doClickLast();
                break;
            case R.id.imageview_music_play_tab2_play:
                doClickPlay();
                break;
            case R.id.imageview_music_play_tab2_next:
                doClickNext();
                break;
            case R.id.imageview_music_play_tab2_list:
                doClickList();
                break;
            case R.id.textview_music_play_tab2_lyric:
                doClickLyric();
                break;
            default:
                break;
        }
    }

    private void doClickLyric() {
        mCallBackListener.switchFragment(2);
    }

    //点击触发展示歌曲列表
    private void doClickList() {
        mCallBackListener.showMusicListDialog();
    }

    private void doClickNext() {
        mMusicBinder.startNext();
        updateChangingMusic();
    }

    private void doClickPlay() {
        if (mMusicBinder != null) {
            mMusicBinder.changePlayingState();
        }
    }

    private void doClickLast() {
        mMusicBinder.startLast();
        updateChangingMusic();
    }

    //改变播放模式
    private void doClickChange() {
        mMusicBinder.changeMode();
    }

    private void doClickMore() {
    }

    private void doClickComment() {
    }

    private void doClickDownload() {
    }

    private void doClickTune() {
    }

    private void doClickSing() {
    }

    //点击喜爱图片时调用
    private void doClickFavorite() {
        mMusicBinder.changeMusicFavorite();
    }

    private void doClickPicture() {
        mImageViewFullImage.setImageBitmap(mMusicBinder.getAlbumBitmap());
        mDialogBigImage.show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setStatusBarColor(DrawUtil.mixColor(getResources().getColor(R.color.music_play_background_1)
                    , getResources().getColor(R.color.transparent_black)));
        }
    }

    //播放模式改变时调用
    public void updateModeChanged(MusicService.PlayMode mode, boolean showToast) {
        String text = "已切换到";
        switch (mode) {
            case RANDOM:
                mImageViewChange.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab2_random));
                text += "随机播放";
                break;
            case REPEAT:
                mImageViewChange.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab2_repeat));
                text += "顺序播放";
                break;
            case REPEAT_ONE:
                mImageViewChange.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab2_repeat_one));
                text += "单曲循环";
                break;
        }
        if (showToast) {
            new ToastUtil(getContext(), text).shortShow().setGravity(Gravity.CENTER, 0, 300).setAlpha(0.7f).show();
        }
    }

    public void updateMusicPrepared() {
        updateChangingMusic();
    }

    public void changeMusicState(boolean isPlaying) {
        if (isPlaying) {
            mImageViewPlay.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab2_start));
            mHandler.post(mRunnable);
        } else {
            mImageViewPlay.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab2_pause));
            mHandler.removeCallbacks(mRunnable);
        }
    }

    //改变音乐喜爱的属性
    public void changeMusicFavorite(boolean isFavorite) {
        if (isFavorite) {
            mImageViewFavorite.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab2_favorite));
        } else {
            mImageViewFavorite.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab2_not_favorite));
        }
    }

    //改变音乐正在播放的歌词
    public void changeMusicLyric(String lyric) {
        mTextViewLyric.setText(lyric);
    }

    public void setOnCallBackListener(CallBackListener callBackListener) {
        mCallBackListener = callBackListener;
    }

    //声明回调接口
    public interface CallBackListener {
        public MusicService.MusicBinder getMusicBinder();

        public void setBackground(Bitmap bitmap);

        public void showMusicListDialog();

        //切换这个activity的fragment页面
        public void switchFragment(int position);
    }
}