package com.example.musicplayer.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.musicplayer.fragment.dialogFragment.LyricFeaturesDialogFragment;
import com.example.musicplayer.musicTools.Lyric;
import com.example.musicplayer.R;
import com.example.musicplayer.adapter.MusicLyricAdapter;
import com.example.musicplayer.service.MusicService;

public class MusicPlayTab3Fragment extends Fragment implements View.OnClickListener {
    private TextView mTextViewTitle;
    private TextView mTextViewArtist;
    private ImageView mImageViewComment;
    private ImageView mImageViewLyric;
    private ImageView mImageViewPlay;
    private ListView mListView;
    private Tab3CallBackListener mTab3CallBackListener;
    private MusicService.MusicBinder mMusicBinder;
    private MusicLyricAdapter mAdapter;
    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        int nowPosition = mAdapter.getNowPosition();
                        if (mMusicBinder.getCurrent() >= Lyric.getStartTime(nowPosition + 1)) {
                            mAdapter.setNowPosition(nowPosition + 1);
                            scrollTo(mAdapter.getNowPosition(), 500);
                            mAdapter.notifyDataSetChanged();
                            mTab3CallBackListener.musicLyricChanged(Lyric.getLyric(nowPosition + 1));
                        } else if (mMusicBinder.getCurrent() < Lyric.getStartTime(nowPosition)) {
                            if (nowPosition >= 1)
                                nowPosition--;
                            mAdapter.setNowPosition(nowPosition);
                            scrollTo(mAdapter.getNowPosition(), 500);
                            mAdapter.notifyDataSetChanged();
                            mTab3CallBackListener.musicLyricChanged(Lyric.getLyric(mAdapter.getNowPosition()));
                        }
                        mHandler.postDelayed(mRunnable, getResources().getInteger(R.integer.refresh_time));
                    }
                });
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_music_play_tab3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        addListener();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mMusicBinder = mTab3CallBackListener.getMusicBinder();
        initListView();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(mRunnable);
    }

    private void initView() {
        mTextViewTitle = getView().findViewById(R.id.textview_music_play_tab3_title);
        mTextViewArtist = getView().findViewById(R.id.textview_music_play_tab3_artist);
        mImageViewComment = getView().findViewById(R.id.imageview_music_play_tab3_comment);
        mImageViewLyric = getView().findViewById(R.id.imageview_music_play_tab3_lyric);
        mImageViewPlay = getView().findViewById(R.id.imageview_music_play_tab3_play);
        mListView = getView().findViewById(R.id.listview_music_play_tab3_lyric);
    }

    private void addListener() {
        mImageViewComment.setOnClickListener(this);
        mImageViewLyric.setOnClickListener(this);
        mImageViewPlay.setOnClickListener(this);
    }

    //更新fragment的显示内容
    private void updateView() {
        if (mMusicBinder != null) {
            changeMusicState(mMusicBinder.isPlaying());
            mTextViewTitle.setText(mMusicBinder.getTitle());
            mTextViewArtist.setText(mMusicBinder.getArtist());
        } else {
            changeMusicState(false);
        }
    }

    private void initListView() {
        //读进去当前歌词文件并初始化
        if (mMusicBinder != null) {
            Lyric.create(mMusicBinder.getLyricPath(), getResources());
            mAdapter = new MusicLyricAdapter(Lyric.getLyrics(), getActivity(), 0);
            mListView.setAdapter(mAdapter);
            mTab3CallBackListener.musicLyricChanged(Lyric.getLyric(0));
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mAdapter.setNowPosition(position);
                    mMusicBinder.seekTo(Lyric.getStartTime(position));
                    scrollTo(position, 500);
                    mAdapter.notifyDataSetChanged();
                    mTab3CallBackListener.musicLyricChanged(Lyric.getLyric(position));
                }
            });
        }
    }

    private void scrollTo(int position, int time) {
        mListView.smoothScrollToPositionFromTop(position, getResources().getInteger(R.integer.offset_lyric), time);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageview_music_play_tab3_comment:
                doClickComment();
                break;
            case R.id.imageview_music_play_tab3_lyric:
                doClickLyric();
                break;
            case R.id.imageview_music_play_tab3_play:
                doClickPlay();
                break;
            default:
                break;
        }
    }

    private void doClickComment() {

    }

    //点击弹出歌词功能选择对话框
    private void doClickLyric() {
        //歌词功能选择对话框
        LyricFeaturesDialogFragment lyricDialog = new LyricFeaturesDialogFragment();
        lyricDialog.setLyricFeaturesListener(new LyricFeaturesDialogFragment.LyricFeaturesListener() {
            @Override
            public void changeChinese() {
                Lyric.changeChinese();
                mAdapter.notifyDataSetChanged();
            }
        });
        if (getActivity() != null) {
            lyricDialog.show(getActivity().getSupportFragmentManager(), "lyricFeaturesDialog");
        }
    }

    //处理播放和暂停事件
    private void doClickPlay() {
        if (mMusicBinder != null) {
            mMusicBinder.changePlayingState();
        }
    }

    //服务通知音乐播放状态改变时被调用
    public void changeMusicState(boolean state) {
        if (state) {
            mImageViewPlay.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab3_start));
            mHandler.post(mRunnable);
        } else {
            mImageViewPlay.setImageDrawable(getResources().getDrawable(R.drawable.activity_music_play_tab3_pause));
            mHandler.removeCallbacks(mRunnable);
        }
    }

    //音乐准备后更新歌词界面
    public void updateMusicPrepared() {
        updateView();
        initListView();
    }

    public void setTab3CallBackListener(Tab3CallBackListener callBack) {
        mTab3CallBackListener = callBack;
    }

    //声明回调接口
    public interface Tab3CallBackListener {
        public MusicService.MusicBinder getMusicBinder();

        //音乐歌词改变时调用
        public void musicLyricChanged(String lyric);
    }
}