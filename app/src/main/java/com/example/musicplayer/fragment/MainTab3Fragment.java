package com.example.musicplayer.fragment;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.adapter.BaseFragmentPagerAdapter;
import com.example.musicplayer.commonUtils.TabUnderLineUtil;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.fragment.activityMainFragmentTab3.Tab1Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab3.Tab2Fragment;

import java.util.ArrayList;
import java.util.List;

public class MainTab3Fragment extends Fragment implements View.OnClickListener {
    private List<TextView> mTextViews = new ArrayList<TextView>();
    private ImageView mImageViewLine;
    private ImageView mImageViewPublish;
    private List<Fragment> mFragments = new ArrayList<Fragment>();
    private ViewPager mViewPager;
    private TabUnderLineUtil mUnderLineUtil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_tab3, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        addListener();
    }

    private void initView() {
        if (getActivity() != null) {
            TextView textView1 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab3_tab1);
            TextView textView2 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab3_tab2);
            mImageViewPublish = getActivity().findViewById(R.id.imageview_activity_main_fragment_tab3_publish);
            mImageViewLine = getActivity().findViewById(R.id.imageview_activity_main_fragment_tab3_line);
            mViewPager = getActivity().findViewById(R.id.viewpager_activity_main_fragment_tab3);
            Tab1Fragment tab1Fragment = new Tab1Fragment();
            Tab2Fragment tab2Fragment = new Tab2Fragment();

            mTextViews.add(textView1);
            mTextViews.add(textView2);
            mFragments.add(tab1Fragment);
            mFragments.add(tab2Fragment);
            mViewPager.setAdapter(new BaseFragmentPagerAdapter(getActivity().getSupportFragmentManager(), 1, mFragments));
            mViewPager.setCurrentItem(0);
            updateTopTab(0);
        }
    }

    private void addListener() {
        for (TextView view : mTextViews)
            view.setOnClickListener(this);
        //第一个textview监听，当布局发生改变时，可获取它的宽度，立即初始化tab的下划线
        mTextViews.get(0).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mUnderLineUtil = new TabUnderLineUtil(mImageViewLine, mTextViews, 0);
                //完成后，移除监听器
                mTextViews.get(0).getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        mImageViewPublish.setOnClickListener(this);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (mUnderLineUtil != null)
                    mUnderLineUtil.updateUnderLine(position, positionOffset);
            }

            @Override
            public void onPageSelected(int position) {
                updateTopTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //更新顶部栏的选择状态
    private void updateTopTab(int position) {
        for (int i = 0; i < mTextViews.size(); i++) {
            if (i == position)
                setTabStatus(mTextViews.get(i), true);
            else
                setTabStatus(mTextViews.get(i), false);
        }
    }

    //更改选择栏的状态
    private void setTabStatus(TextView view, boolean selected) {
        if (selected) {
            view.setTextSize(getResources().getInteger(R.integer.activity_main_viewpager_tab_selected));
            view.setTextColor(getResources().getColor(R.color.activity_main_viewpager_tab_selected));
            view.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        } else {
            view.setTextSize(getResources().getInteger(R.integer.activity_main_viewpager_tab_not_selected));
            view.setTextColor(getResources().getColor(R.color.activity_main_viewpager_tab_not_selected));
            view.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        }
    }

    //点击发送动态的事件
    private void doClickPublish() {
        ToastUtil.makeToast("这是发布动态的功能");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textview_activity_main_fragment_tab3_tab1:
                if (mViewPager.getCurrentItem() != 0) {
                    mViewPager.setCurrentItem(0);
                    updateTopTab(0);
                }
                break;
            case R.id.textview_activity_main_fragment_tab3_tab2:
                if (mViewPager.getCurrentItem() != 1) {
                    mViewPager.setCurrentItem(1);
                    updateTopTab(1);
                }
                break;
            case R.id.imageview_activity_main_fragment_tab3_publish:
                doClickPublish();
                break;
            default:
                break;
        }
    }
}