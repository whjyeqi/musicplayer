package com.example.musicplayer.fragment;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.adapter.BaseFragmentPagerAdapter;
import com.example.musicplayer.commonUtils.TabUnderLineUtil;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.fragment.activityMainFragmentTab1.Tab1Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab1.Tab2Fragment;
import com.example.musicplayer.fragment.activityMainFragmentTab1.Tab3Fragment;

import java.util.ArrayList;
import java.util.List;

public class MainTab1Fragment extends Fragment implements View.OnClickListener {
    private List<TextView> mTextViews = new ArrayList<TextView>();
    private ImageView mImageViewIdentify;
    private ImageView mImageViewLine;
    private List<Fragment> mFragments = new ArrayList<Fragment>();
    private ViewPager mViewPager;
    private TabUnderLineUtil mUnderLineUtil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main_tab1, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        addListener();
    }

    //获取组件
    private void initView() {
        if (getActivity() != null) {
            TextView TextViewTab1 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab1_tab1);
            TextView TextViewTab2 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab1_tab2);
            TextView TextViewTab3 = getActivity().findViewById(R.id.textview_activity_main_fragment_tab1_tab3);
            mImageViewIdentify = getActivity().findViewById(R.id.imageview_activity_main_fragment_tab1_identify);
            mImageViewLine = getActivity().findViewById(R.id.imageview_activity_main_fragment_tab1_line);
            Tab1Fragment Tab1Fragment = new Tab1Fragment();
            Tab2Fragment Tab2Fragment = new Tab2Fragment();
            Tab3Fragment Tab3Fragment = new Tab3Fragment();
            mViewPager = getActivity().findViewById(R.id.viewpager_activity_main_fragment_tab1);

            mFragments.add(Tab1Fragment);
            mFragments.add(Tab2Fragment);
            mFragments.add(Tab3Fragment);
            mTextViews.add(TextViewTab1);
            mTextViews.add(TextViewTab2);
            mTextViews.add(TextViewTab3);
            mViewPager.setAdapter(new BaseFragmentPagerAdapter(getActivity().getSupportFragmentManager(), 1, mFragments));
            mViewPager.setCurrentItem(0);
            updateTopTab(0);
        }
    }

    //设置监听器
    private void addListener() {
        for (TextView view : mTextViews)
            view.setOnClickListener(this);
        //第一个textview监听，当布局发生改变时，可获取它的宽度，立即初始化tab的下划线
        mTextViews.get(0).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mUnderLineUtil = new TabUnderLineUtil(mImageViewLine, mTextViews, 0);
                //完成后，移除监听器
                mTextViews.get(0).getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        mImageViewIdentify.setOnClickListener(this);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (mUnderLineUtil != null)
                    mUnderLineUtil.updateUnderLine(position, positionOffset);
            }

            @Override
            public void onPageSelected(int position) {
                updateTopTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //更新顶部栏的选择状态
    private void updateTopTab(int position) {
        for (int i = 0; i < mTextViews.size(); i++) {
            if (i == position)
                setTabStatus(mTextViews.get(i), true);
            else
                setTabStatus(mTextViews.get(i), false);
        }
    }

    //更改选择栏的状态
    private void setTabStatus(TextView view, boolean selected) {
        if (selected) {
            view.setTextSize(getResources().getInteger(R.integer.activity_main_viewpager_tab_selected));
            view.setTextColor(getResources().getColor(R.color.activity_main_viewpager_tab_selected));
            view.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        } else {
            view.setTextSize(getResources().getInteger(R.integer.activity_main_viewpager_tab_not_selected));
            view.setTextColor(getResources().getColor(R.color.activity_main_viewpager_tab_not_selected));
            view.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textview_activity_main_fragment_tab1_tab1:
                if (mViewPager.getCurrentItem() != 0) {
                    mViewPager.setCurrentItem(0);
                    updateTopTab(0);
                }
                break;
            case R.id.textview_activity_main_fragment_tab1_tab2:
                if (mViewPager.getCurrentItem() != 1) {
                    mViewPager.setCurrentItem(1);
                    updateTopTab(1);
                }
                break;
            case R.id.textview_activity_main_fragment_tab1_tab3:
                if (mViewPager.getCurrentItem() != 2) {
                    mViewPager.setCurrentItem(2);
                    updateTopTab(2);
                }
                break;
            case R.id.imageview_activity_main_fragment_tab1_identify:
                doClickIdentify();
                break;
            default:
                break;
        }
    }

    //点击识别歌曲功能
    private void doClickIdentify() {
        ToastUtil.makeToast("这是听歌识曲的功能");
    }
}