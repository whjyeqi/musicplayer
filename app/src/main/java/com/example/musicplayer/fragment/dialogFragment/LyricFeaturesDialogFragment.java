package com.example.musicplayer.fragment.dialogFragment;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.view.LyricFeaturesScrollView;

import java.util.ArrayList;
import java.util.List;

public class LyricFeaturesDialogFragment extends DialogFragment implements View.OnClickListener, View.OnTouchListener {
    private List<CardView> mCardViews = new ArrayList<CardView>();
    private List<TextView> mTextViews = new ArrayList<TextView>();
    private LinearLayout mCancel;
    private TextView mTextViewCancel;
    private LyricFeaturesScrollView mScrollView;
    private LyricFeaturesListener mLyricFeaturesListener;
    //记录touch事件开始的x,y坐标
    private int mTouchX;
    private int mTouchY;
    //记录Y方向的最小偏移量，超过这个量就处理左右滑动事件
    private static final int MIN_X = 10;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lyric_features_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        addListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        //设置对话框的布局
        Window window = getDialog().getWindow();
        if (window != null) {
            window.getDecorView().setPadding(0, 0, 0, 0);
            window.getDecorView().setBackgroundColor(getResources().getColor(R.color.fragment_lyric_features_dialog_background));
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.BOTTOM;
            window.setAttributes(lp);
        }
        if (window != null)
            window.setWindowAnimations(R.style.LyricFeaturesDialogFragment);
    }

    private void initView() {
        mCancel = getView().findViewById(R.id.lyric_features_dialog_close);
        mTextViewCancel = getView().findViewById(R.id.textview_lyric_features_dialog_close);
        mScrollView = getView().findViewById(R.id.scroll_view_lyric_features_dialog);
        View view = getView();
        mCardViews.add((CardView) view.findViewById(R.id.card_change));
        mCardViews.add((CardView) view.findViewById(R.id.card_poster));
        mCardViews.add((CardView) view.findViewById(R.id.card_size));
        mCardViews.add((CardView) view.findViewById(R.id.card_adjust));
        mCardViews.add((CardView) view.findViewById(R.id.card_desktop));
        mCardViews.add((CardView) view.findViewById(R.id.card_search));
        mCardViews.add((CardView) view.findViewById(R.id.card_error));
        mCardViews.add((CardView) view.findViewById(R.id.card_report));

        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_change));
        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_poster));
        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_size));
        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_adjust));
        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_desktop));
        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_search));
        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_error));
        mTextViews.add((TextView) view.findViewById(R.id.textview_lyric_features_dialog_report));
    }

    //添加监听器
    @SuppressLint("ClickableViewAccessibility")
    private void addListener() {
        for (CardView cardView : mCardViews) {
            cardView.setOnClickListener(this);
            cardView.setOnTouchListener(this);
        }
        mCancel.setOnClickListener(this);
        mCancel.setOnTouchListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.card_change:
                doClickChange();
                break;
            case R.id.card_poster:
                doClickPoster();
                break;
            case R.id.card_size:
                doClickSize();
                break;
            case R.id.card_adjust:
                doClickAdjust();
                break;
            case R.id.card_desktop:
                doClickDesktop();
                break;
            case R.id.card_search:
                doClickSearch();
                break;
            case R.id.card_error:
                doClickError();
                break;
            case R.id.card_report:
                doClickReport();
                break;
            case R.id.lyric_features_dialog_close:
                doClickCancel();
                break;
            default:
                break;
        }
    }

    //取消当前对话框
    private void doClickCancel() {
        dismiss();
    }

    private void doClickReport() {

    }

    private void doClickError() {

    }

    private void doClickSearch() {

    }

    private void doClickDesktop() {

    }

    private void doClickAdjust() {

    }

    private void doClickSize() {

    }

    private void doClickPoster() {
    }

    //简繁转换事件
    private void doClickChange() {
        mLyricFeaturesListener.changeChinese();
        dismiss();
    }

    //功能键按压状态变化时调用此方法，改变一些属性
    private void pressStateChanged(View v, boolean isPressed) {
        Resources r = getResources();
        switch (v.getId()) {
            case R.id.lyric_features_dialog_close:
                if (isPressed)
                    mTextViewCancel.setTextColor(r.getColor(R.color.fragment_lyric_features_dialog_cancel_pressed));
                else
                    mTextViewCancel.setTextColor(r.getColor(R.color.fragment_lyric_features_dialog_cancel));
                break;
            case R.id.card_change:
                featuresPressed(0, isPressed);
                break;
            case R.id.card_poster:
                featuresPressed(1, isPressed);
                break;
            case R.id.card_size:
                featuresPressed(2, isPressed);
                break;
            case R.id.card_adjust:
                featuresPressed(3, isPressed);
                break;
            case R.id.card_desktop:
                featuresPressed(4, isPressed);
                break;
            case R.id.card_search:
                featuresPressed(5, isPressed);
                break;
            case R.id.card_error:
                featuresPressed(6, isPressed);
                break;
            case R.id.card_report:
                featuresPressed(7, isPressed);
                break;
        }
    }

    //改变功能按键的颜色和字体颜色
    private void featuresPressed(int position, boolean isPressed) {
        Resources r = getResources();
        if (isPressed) {
            mCardViews.get(position).setCardBackgroundColor(r.getColor(R.color.fragment_lyric_features_dialog_background));
            mTextViews.get(position).setTextColor(r.getColor(R.color.fragment_lyric_features_dialog_selection_text_pressed));
        } else {
            mCardViews.get(position).setCardBackgroundColor(r.getColor(R.color.fragment_lyric_features_dialog_selection_card));
            mTextViews.get(position).setTextColor(r.getColor(R.color.fragment_lyric_features_dialog_selection_text));
        }
    }

    //设置监听器
    public void setLyricFeaturesListener(LyricFeaturesListener listener) {
        mLyricFeaturesListener = listener;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //获取按下的坐标
                mTouchX = x;
                mTouchY = y;
                pressStateChanged(v, true);
                break;
            case MotionEvent.ACTION_MOVE:
                int dx = x - mTouchX;
                //超过一定范围，取消按压效果
                if (Math.abs(dx) >= MIN_X || y <= 0 || y >= v.getMeasuredHeight()) {
                    pressStateChanged(v, false);
                    return false;
                }
                break;
            case MotionEvent.ACTION_UP:
                //释放位置在范围内，触发点击事件
                if (x >= 0 && x <= v.getMeasuredWidth() && y >= 0 && y <= v.getMeasuredHeight() && Math.abs(x - mTouchX) < MIN_X)
                    v.performClick();
                pressStateChanged(v, false);
                break;
            case MotionEvent.ACTION_CANCEL:
                pressStateChanged(v, false);
                break;
        }
        return true;
    }

    //监听器，监听对话框的点击事件
    public interface LyricFeaturesListener {
        //繁简转换事件触发
        public void changeChinese();
    }
}