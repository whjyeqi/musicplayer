package com.example.musicplayer.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musicplayer.ActivityStackManager;
import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.activity.MainActivity;
import com.example.musicplayer.commonUtils.DisplayUtil;
import com.example.musicplayer.fragment.dialogFragment.MusicListDialogFragment;
import com.example.musicplayer.view.MusicPlayingProgressView;

public class MusicTabViewService extends Service {
    private static final int CLICK_EVENT_MIN_DELTA = 5;
    private static final int STANDARD_LOC_DELTA = 10;
    private static float sAlphaDelta;
    private static int sMoveDelta;
    private static CallBackListener mCallBackListener;
    private MusicTabViewBinder mBinder = new MusicTabViewBinder();
    private MusicService.MusicBinder mMusicBinder;
    private WindowManager mManager;
    private WindowManager.LayoutParams mParams;
    private View mView;
    private boolean mViewAdded = false;
    private boolean mCanMove = false;
    private boolean mIsMoving = false;
    private ImageView mBitmapImageView;
    private ImageView mImageViewPlayState;
    private ImageView mImageViewPlayList;
    private ImageView mCancelAdjust;
    private TextView mTextViewPlayInfo;
    private MusicPlayingProgressView mProgressView;
    private MusicListDialogFragment mMusicListDialogFragment;
    private int mCurrentX;
    private int mCurrentY;
    private int mMinY = 0;
    private int mMaxY;
    private int mViewHeight;
    private int mStartMarginBottom;
    //flag to judge if appear or disappear
    private boolean mAnimationAppear = true;
    private Handler mAppearHandler = new Handler();
    private Runnable mAlphaAnimation = new Runnable() {
        @Override
        public void run() {
            float currentAlpha = mView.getAlpha();
            //if begin is not attached, let it be visible
            if (currentAlpha == 0.0f) {
                showMusicTabView(true);
            }
            if (mAnimationAppear) {
                currentAlpha = Math.min(1.0f, currentAlpha + sAlphaDelta);
            } else {
                currentAlpha = Math.max(0.0f, currentAlpha - sAlphaDelta);
            }
            mView.setAlpha(currentAlpha);
            if (currentAlpha > 0.0f && currentAlpha < 1.0f) {
                mAppearHandler.postDelayed(mAlphaAnimation, MusicPlayerApplication.getRefreshTime());
            } else if (currentAlpha == 0.0f) {
                showMusicTabView(false);
            } else {
                showMusicTabView(true);
            }
        }
    };

    private Handler mMoveHandler = new Handler();
    private Runnable mMoveAnimation = new Runnable() {
        @Override
        public void run() {
            if (sMoveDelta != 0) {
                mIsMoving = true;
                mCurrentY += sMoveDelta;
                if (sMoveDelta < 0) {
                    mCurrentY = Math.max(mCurrentY, 0);
                } else {
                    mCurrentY = Math.min(mCurrentY, mStartMarginBottom);
                }
                updateViewLoc();
                if (mCurrentY != 0 && mCurrentY != mStartMarginBottom) {
                    mMoveHandler.postDelayed(mMoveAnimation, MusicPlayerApplication.getRefreshTime());
                } else {
                    mIsMoving = false;
                }
            } else {
                mIsMoving = false;
            }
        }
    };

    static {
        sAlphaDelta = (float) MusicPlayerApplication.getRefreshTime() / (float) MusicPlayerApplication.getAnimationTime();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mManager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        mParams = new WindowManager.LayoutParams();
        createView();
        addListener();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        updateViewLoc();
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        showMusicTabView(false);
        mManager = null;
        mParams = null;
    }

    @SuppressLint({"InflateParams", "ClickableViewAccessibility"})
    private void createView() {
        mMusicListDialogFragment = new MusicListDialogFragment();
        mViewHeight = (int) getResources().getDimension(R.dimen.music_tab_view_height);
        Activity activity = ActivityStackManager.getInstance().getTopActivity();
        if (activity != null) {
            mMaxY = DisplayUtil.getRealScreenSize(activity)[1] - DisplayUtil.getStatusBarHeight(getResources()) - mViewHeight;
        }
        mStartMarginBottom = (int) getResources().getDimension(R.dimen.music_tab_view_margin_bottom);
        mView = LayoutInflater.from(this).inflate(R.layout.music_tab_view, null);
        mBitmapImageView = mView.findViewById(R.id.circle_imageview);
        mImageViewPlayState = mView.findViewById(R.id.imageview_music_state);
        mImageViewPlayList = mView.findViewById(R.id.imageview_music_list);
        mCancelAdjust = mView.findViewById(R.id.imageview_cancel_adjust);
        mTextViewPlayInfo = mView.findViewById(R.id.textview_music_info);
        mProgressView = mView.findViewById(R.id.music_playing_progress_view);
        int imageSize = (int) getResources().getDimension(R.dimen.music_tab_view_image_size);
        DisplayUtil.setRoundOutline(mBitmapImageView, 0, 0, imageSize, imageSize);

        //add touch listen that can move this tav view
        MusicTabViewTouchListener touchListener = new MusicTabViewTouchListener();
        mView.setOnTouchListener(touchListener);
        mBitmapImageView.setOnTouchListener(touchListener);
        mTextViewPlayInfo.setOnTouchListener(touchListener);
        mImageViewPlayState.setOnTouchListener(touchListener);
        mImageViewPlayList.setOnTouchListener(touchListener);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            mParams.type = WindowManager.LayoutParams.TYPE_PHONE;
        }
        //set not opaque
        mParams.format = PixelFormat.RGBA_8888;
        mParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;
        mParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        mParams.height = mViewHeight;
        mParams.gravity = Gravity.BOTTOM;
        mCurrentX = mParams.x = 0;
        mCurrentY = mParams.y = mStartMarginBottom;
        appear(true);
    }

    private void addListener() {
        mBitmapImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBackListener != null && mMusicBinder != null && mMusicBinder.getMusicCounts() > 0) {
                    mCallBackListener.jumpToMusicPlayActivity();
                }
            }
        });
        mTextViewPlayInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBackListener != null && mMusicBinder != null && mMusicBinder.getMusicCounts() > 0) {
                    mCallBackListener.jumpToMusicPlayActivity();
                }
            }
        });
        mBitmapImageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!mCanMove) {
                    setAdjustMode(true);
                }
                return true;
            }
        });
        mTextViewPlayInfo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!mCanMove) {
                    setAdjustMode(true);
                }
                return true;
            }
        });
        mImageViewPlayState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMusicBinder != null) {
                    mMusicBinder.changePlayingState();
                }
            }
        });
        mImageViewPlayList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCallBackListener != null && mMusicBinder != null && mMusicBinder.getMusicCounts() > 0
                        && !mMusicListDialogFragment.isAdded()) {
                    mCallBackListener.showMusicList(mMusicListDialogFragment);
                }
            }
        });
        mCancelAdjust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAdjustMode(false);
            }
        });
        mMusicListDialogFragment.setCallBackListener(new MusicListDialogFragment.CallBackListener() {
            @Override
            public MusicService.MusicBinder getBinder() {
                return mMusicBinder;
            }

            @Override
            public void onMusicListShow() {
                appear(false);
            }

            @Override
            public void onMusicListDismiss() {
                appear(true);
            }
        });
    }

    private void updateViewLoc() {
        mCurrentY = Math.max(mCurrentY, mMinY);
        mCurrentY = Math.min(mCurrentY, mMaxY);
        //judge the standard location
        int standardY = getStandardLocY();
        if (Math.abs(mCurrentY - standardY) <= STANDARD_LOC_DELTA) {
            mCurrentY = standardY;
        }
        mParams.x = mCurrentX;
        mParams.y = mCurrentY;
        mManager.updateViewLayout(mView, mParams);
    }

    private void updateMusicTab() {
        if (mMusicBinder != null) {
            if (mMusicBinder.hasMusic()) {
                Bitmap bitmap = mMusicBinder.getAlbumBitmap();
                if (bitmap == null) {
                    mBitmapImageView.setImageResource(R.mipmap.default1);
                } else {
                    mBitmapImageView.setImageBitmap(bitmap);
                }
                String title = mMusicBinder.getTitle();
                String artist = mMusicBinder.getArtist();
                String textViewMusicInfo = title + " - " + artist;
                mTextViewPlayInfo.setText(textViewMusicInfo);
                mTextViewPlayInfo.setTextColor(getResources().getColor(R.color.black));
                mImageViewPlayList.setImageResource(R.drawable.main_activity_play_list);
            } else {
                mBitmapImageView.setImageResource(R.mipmap.default1);
                mTextViewPlayInfo.setText(R.string.main_activity_music_info);
                mTextViewPlayInfo.setTextColor(getResources().getColor(R.color.light_gray));
                mImageViewPlayList.setImageResource(R.drawable.main_activity_no_list);
            }
            updatePlayingStateChanged();
        }
    }

    //当播放状态变化时调用
    private void updatePlayingStateChanged() {
        if (mMusicBinder.hasMusic()) {
            mProgressView.setColorEnable(true);
            setImageViewMusicState(true, mMusicBinder.isPlaying());
        } else {
            setImageViewMusicState(false, true);
            mProgressView.setColorEnable(false);
        }
        updateProgressView();
    }

    //更新进度条
    private void updateProgressView() {
        //更新圆形播放进度条
        if (mMusicBinder != null) {
            float duration = (float) mMusicBinder.getDuration();
            if (duration > 0) {
                float sweepAngle = (float) mMusicBinder.getCurrent() / duration * 360;
                mProgressView.setSweepAngle(sweepAngle);
            }
        }
    }

    //改变播放图标
    private void setImageViewMusicState(boolean hasMusic, boolean state) {
        if (!hasMusic) {
            mImageViewPlayState.setImageResource(R.drawable.main_activity_no_playings);
        } else if (state) {
            mImageViewPlayState.setImageResource(R.drawable.main_activity_playings);
        } else {
            mImageViewPlayState.setImageResource(R.drawable.main_activity_pauses);
        }
    }

    private void showMusicTabView(boolean show) {
        if (mManager != null) {
            if (show && !mViewAdded) {
                mManager.addView(mView, mParams);
                mViewAdded = true;
            }
            if (!show && mViewAdded) {
                mManager.removeView(mView);
                mViewAdded = false;
            }
        }
    }

    private void appear(boolean appear) {
        mAnimationAppear = appear;
        mAppearHandler.post(mAlphaAnimation);
    }

    private void moveToBottom(boolean moveToBottom) {
        if (!mCanMove) {
            int counts = MusicPlayerApplication.getAnimationTime() / MusicPlayerApplication.getRefreshTime();
            if (moveToBottom) {
                if (mCurrentY != mStartMarginBottom && !mIsMoving) {
                    return;
                }
                sMoveDelta = (-mCurrentY) / counts;
                if (sMoveDelta == 0) {
                    mCurrentY = 0;
                    updateViewLoc();
                }
            } else {
                if (mCurrentY != 0 && !mIsMoving) {
                    return;
                }
                sMoveDelta = (mStartMarginBottom - mCurrentY) / counts;
                if (sMoveDelta == 0) {
                    mCurrentY = mStartMarginBottom;
                    updateViewLoc();
                }
            }
            if (sMoveDelta != 0) {
                mMoveHandler.post(mMoveAnimation);
            }
        }
    }

    private void setAdjustMode(boolean isAdjustMode) {
        mCanMove = isAdjustMode;
        View mContainerView = mView.findViewById(R.id.linear_layout_container);
        if (mCanMove) {
            mCancelAdjust.setVisibility(View.VISIBLE);
            mContainerView.setBackgroundResource(R.drawable.music_tab_view_background_adjust);
        } else {
            mCancelAdjust.setVisibility(View.GONE);
            mContainerView.setBackgroundResource(R.drawable.music_tab_view_background);
        }
    }

    private int getStandardLocY() {
        if (MusicPlayerApplication.getInstance().getTopActivity() instanceof MainActivity) {
            return mStartMarginBottom;
        } else {
            return 0;
        }
    }

    public static void setCallBackListener(CallBackListener listener) {
        mCallBackListener = listener;
    }

    public static void removeCallBackListener() {
        mCallBackListener = null;
    }

    private class MusicTabViewTouchListener implements View.OnTouchListener {
        private int mStartTouchY;
        private int mStartLayoutY;
        private int mStartRelativeX;
        private int mStartRelativeY;
        private boolean mShouldPerformClick = true;
        //flag to judge if execute touch event
        private boolean mHasDownEvent = false;

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (!mCanMove) {
                mHasDownEvent = false;
                return false;
            }
            if (event.getAction() != MotionEvent.ACTION_DOWN && !mHasDownEvent) {
                return false;
            }
            int y = (int) event.getRawY();
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mStartTouchY = y;
                    mStartLayoutY = mCurrentY;
                    mStartRelativeX = (int) event.getX();
                    mStartRelativeY = (int) event.getY();
                    mShouldPerformClick = true;
                    mHasDownEvent = true;
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (mShouldPerformClick) {
                        int relativeX = (int) event.getX();
                        int relativeY = (int) event.getY();
                        mShouldPerformClick = relativeX >= 0 && relativeX <= v.getMeasuredWidth()
                                && relativeY >= 0 && relativeY <= v.getMeasuredHeight()
                                && Math.abs(relativeX - mStartRelativeX) <= CLICK_EVENT_MIN_DELTA
                                && Math.abs(relativeY - mStartRelativeY) <= CLICK_EVENT_MIN_DELTA;
                    }
                    if (!mShouldPerformClick) {
                        int dy = y - mStartTouchY;
                        mCurrentY = mStartLayoutY - dy;
                        updateViewLoc();
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (mShouldPerformClick) {
                        v.performClick();
                    }
                    break;
                case MotionEvent.ACTION_CANCEL:
                    break;
            }
            if (event.getAction() != MotionEvent.ACTION_DOWN && event.getAction() != MotionEvent.ACTION_MOVE) {
                mHasDownEvent = false;
            }
            return true;
        }
    }

    public class MusicTabViewBinder extends Binder {
        public void setMusicBinder(MusicService.MusicBinder musicBinder) {
            mMusicBinder = musicBinder;
            updateMusicTab();
        }

        public void refreshView() {
            if (mMusicBinder != null) {
                updateProgressView();
            }
        }

        public void updateMusicTab() {
            MusicTabViewService.this.updateMusicTab();
        }

        public void updatePlayingState() {
            updatePlayingStateChanged();
        }

        public void shouldShowView(boolean shouldShow) {
            appear(shouldShow);
        }

        public void shouldMoveView(boolean moveToBottom) {
            moveToBottom(moveToBottom);
        }

        public void updateMusicListView() {
            if (mMusicListDialogFragment != null && mMusicListDialogFragment.isResumed()) {
                mMusicListDialogFragment.updateWhenChangingMusic();
            }
        }

        public boolean isViewAdded() {
            return mViewAdded;
        }
    }

    public static interface CallBackListener {
        //try to show current music list
        void showMusicList(MusicListDialogFragment musicListDialogFragment);

        //jump to music play activity
        void jumpToMusicPlayActivity();
    }
}