package com.example.musicplayer.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.example.musicplayer.ActivityStackManager;
import com.example.musicplayer.DataLoadManager;
import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.MusicStateChangedListener;
import com.example.musicplayer.commonUtils.PlayingNotification;
import com.example.musicplayer.musicTools.MusicListType;
import com.example.musicplayer.musicTools.MusicSortTool;
import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.R;
import com.example.musicplayer.musicClass.MusicMenu;
import com.example.musicplayer.musicClass.PlayingNotificationInfo;
import com.example.musicplayer.settings.MainActivitySettings;
import com.example.musicplayer.settings.MusicData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MusicService extends Service {
    private static final String TAG = "MusicService";
    //delete music from current music list
    public static final int DELETE_ONE = 0;
    public static final int DELETE_CURRENT = 1;
    public static final int DELETE_STATE_EMPTY = 2;
    //当前的播放音乐列表
    private List<MusicInfo> mMusicList = new ArrayList<MusicInfo>();
    private MusicBinder mMusicBinder = new MusicBinder();
    private MediaPlayer mPlayer = null;
    //音频处理服务
    private AudioManager mAudioManager;
    private AudioFocusRequest mAudioFocusRequest;
    //记录是否持有音频焦点
    private boolean mHasAudioFocus = false;
    //当前音乐列表播放歌曲索引
    private int mIndex = -1;
    //记录是否是在播放中被打断，用于恢复播放状态
    private boolean mInterruptedWhilePlaying = false;
    private PlayMode mPlayMode = PlayMode.RANDOM;
    //record the volume
    private float mCurrentVolume = 0.0f;
    //if now increasing the volume
    private boolean mVolumeIncreasing = true;
    private boolean mIsPlaying = false;
    private MusicServiceBroadcastReceiver mMusicServiceBroadcastReceiver;
    //监听音乐状态改变的监听者
    private List<MusicStateChangedListener> mMusicStateListener = new ArrayList<MusicStateChangedListener>();
    //播放前台发送通知类
    private PlayingNotification mPlayingNotification;
    //播放列表类型
    private MusicListType mMusicListType;
    private MusicMenu mCurrentMusicMenu;

    private Handler mMusicPlayingHandler;
    private Runnable mMusicPlayingRunnable = new Runnable() {
        //时间累计
        private int mTimeCounts = 0;
        //当前音乐id
        private int mCurrentMusicId = 0;
        //线程循环的时间间隔
        private static final int TIME_INTERVAL = 500;

        @Override
        public void run() {
            int musicId = mMusicBinder.getId();
            if (musicId != mCurrentMusicId) {
                mCurrentMusicId = musicId;
                mTimeCounts = 0;
            } else if (mMusicBinder.isPlaying()) {
                mTimeCounts += TIME_INTERVAL;
                int duration = mMusicBinder.getDuration();
                int limitTime = Math.min(duration, MainActivitySettings.getTimeToIncreaseListenCounts());
                if (mTimeCounts > duration)
                    mTimeCounts -= duration;
                if (mTimeCounts >= limitTime && mTimeCounts - TIME_INTERVAL < limitTime) {
                    MusicInfo temp = MusicInfo.getMusicInfoById(mCurrentMusicId);
                    if (temp != null) {
                        temp.addListenCounts();
                    }
                }
            }
            mMusicPlayingHandler.postDelayed(mMusicPlayingRunnable, TIME_INTERVAL);
        }
    };

    private Handler mSaveHandler;
    //保存一次音乐播放状态
    private Runnable mSaveRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "run(): save the music playing state");
            writeMusicData();
        }
    };

    private Handler mVolumeHandler = new Handler();
    private VolumeRunnable mVolumeRunnable = new VolumeRunnable(MusicPlayerApplication.getInstance());

    private class VolumeRunnable implements Runnable {
        private float mVolumeDelta;
        private int mRefreshTime;

        public VolumeRunnable(Context context) {
            float time = context.getResources().getInteger(R.integer.music_volume_change_time);
            mRefreshTime = context.getResources().getInteger(R.integer.music_volume_change_refresh_time);
            mVolumeDelta = (float) mRefreshTime / time;
            if (mVolumeDelta == 0.0f) {
                mVolumeDelta = 0.01f;
            }
        }

        @Override
        public void run() {
            if (mPlayer != null) {
                //if pause, let it be in playing state
                if (!mPlayer.isPlaying()) {
                    mPlayer.start();
                }
                if (mVolumeIncreasing) {
                    mCurrentVolume += mVolumeDelta;
                    mCurrentVolume = Math.min(1.0f, mCurrentVolume);
                } else {
                    mCurrentVolume -= mVolumeDelta;
                    mCurrentVolume = Math.max(0.0f, mCurrentVolume);
                }
                mMusicBinder.setPlayVolume(mCurrentVolume);
                if (mCurrentVolume > 0.0f && mCurrentVolume < 1.0f) {
                    mVolumeHandler.postDelayed(mVolumeRunnable, mRefreshTime);
                } else if (mCurrentVolume == 0.0f) {
                    mPlayer.pause();
                } else {
                    mPlayer.start();
                }
            }
        }
    }

    //监听音频焦点的变化
    private AudioManager.OnAudioFocusChangeListener mAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            switch (focusChange) {
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                    mHasAudioFocus = false;
                    if (mMusicBinder.isPlaying()) {
                        mInterruptedWhilePlaying = true;
                        mMusicBinder.pause();
                    }
                    break;
                case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                    if (mMusicBinder.isPlaying()) {
                        mMusicBinder.setPlayVolume(0.3f);
                    }
                    break;
                case AudioManager.AUDIOFOCUS_LOSS:
                    mHasAudioFocus = false;
                    mMusicBinder.pause();
                    break;
                case AudioManager.AUDIOFOCUS_GAIN:
                    mHasAudioFocus = true;
                    if (!mMusicBinder.isPlaying() && mInterruptedWhilePlaying) {
                        mMusicBinder.start();
                        mInterruptedWhilePlaying = false;
                    } else if (mMusicBinder.isPlaying()) {
                        mMusicBinder.start();
                    }
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate(): MusicService create");
        super.onCreate();
        //注册广播
        mMusicServiceBroadcastReceiver = new MusicServiceBroadcastReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(getString(R.string.notification_click));
        filter.addAction(getString(R.string.notification_favorite));
        filter.addAction(getString(R.string.notification_play));
        filter.addAction(getString(R.string.notification_last));
        filter.addAction(getString(R.string.notification_next));
        filter.addAction(getString(R.string.notification_cancel));
        filter.addAction("android.intent.action.PHONE_STATE");
        registerReceiver(mMusicServiceBroadcastReceiver, filter);
        //获取音频服务
        mAudioManager = (AudioManager) getSystemService(Service.AUDIO_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand(): MusicService start");
        init();
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMusicBinder;
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy(): MusicService destroy");
        super.onDestroy();
        mMusicList = null;
        if (mPlayer != null)
            mPlayer.release();
        mPlayer = null;
        unregisterReceiver(mMusicServiceBroadcastReceiver);
        abandonAudioFocus();
        //移除音乐状态监听线程
        mMusicPlayingHandler.removeCallbacks(mMusicPlayingRunnable);
        if (mPlayingNotification != null)
            mPlayingNotification.destroyNotification();
    }

    //一些初始化操作
    private void init() {
        if (mPlayer == null) {
            mPlayer = new MediaPlayer();
            mMusicBinder.setPlayVolume(mCurrentVolume);
            mMusicList.clear();
            MusicData musicData = null;
            if (DataLoadManager.getInstance() != null) {
                musicData = DataLoadManager.getInstance().readMusicData();
            }
            List<Integer> list = new ArrayList<Integer>();
            if (musicData != null) {
                list = musicData.getPlayList();
            }

            if (musicData != null && list.size() > 0) {
                mMusicListType = MusicListType.valueOf(musicData.getMusicListType());
                mPlayMode = PlayMode.valueOf(musicData.getPlayMode());
                mCurrentMusicMenu = MusicInfo.getMusicMenuByName(musicData.getMusicMenuName());
                int musicId = musicData.getMusicId();
                int currentTime = musicData.getCurrentTime();
                for (int i = 0; i < list.size(); i++) {
                    MusicInfo musicInfo = MusicInfo.getMusicInfoById(list.get(i));
                    if (musicInfo != null)
                        mMusicList.add(musicInfo);
                }
                if (mMusicList.size() == 0) {
                    initNoMusicData();
                    initCurrentMusic(0, 0);
                } else {
                    int index = 0;
                    for (int i = 0; i < mMusicList.size(); i++)
                        if (mMusicList.get(i).getId() == musicId) {
                            index = i;
                            break;
                        }
                    initCurrentMusic(index, currentTime);
                }
            } else {
                initNoMusicData();
                initCurrentMusic(0, 0);
            }
            if (mMusicPlayingHandler == null)
                mMusicPlayingHandler = new Handler();
            //执行线程监听音乐播放状态
            mMusicPlayingHandler.post(mMusicPlayingRunnable);
        }
    }

    //获取音乐状态信息失败时，加载
    private void initNoMusicData() {
        mMusicListType = MusicListType.TYPE_LOCAL;
        mPlayMode = PlayMode.RANDOM;
        initMusicList();
    }

    private void initCurrentMusic(int index, int currentTime) {
        mIndex = -1;
        if (index >= 0 && index < mMusicList.size())
            mIndex = index;
        addListener();
        mPlayingNotification = new PlayingNotification(this, this);
        //初始化播放本地音乐
        if (mMusicList.size() > 0) {
            try {
                mPlayer.reset();
                mPlayer.setDataSource(mMusicList.get(mIndex).getPath());
                mPlayer.prepare();
                mPlayer.seekTo(currentTime);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //初始化当前播放的音乐列表
    private void initMusicList() {
        mMusicList.clear();
        List<MusicInfo> list = MusicInfo.getLocalMusic();
        if (mMusicListType == null)
            mMusicListType = MusicListType.TYPE_LOCAL;
        //判断歌曲列表类型，向播放列表添加歌曲
        if (mMusicListType == MusicListType.TYPE_FAVORITE) {
            for (int i = 0; i < list.size(); i++)
                if (list.get(i).getFavorite() == 1)
                    mMusicList.add(list.get(i));
        } else if (mMusicListType == MusicListType.TYPE_LOCAL || mMusicListType == MusicListType.TYPE_PURCHASED) {
            mMusicList = MusicInfo.getLocalMusic();
        } else {
            if (mCurrentMusicMenu != null) {
                List<Integer> musicId = MusicSortTool.sort(mCurrentMusicMenu.getName());
                for (int i = 0; i < musicId.size(); i++) {
                    MusicInfo temp = MusicInfo.getMusicInfoById(musicId.get(i));
                    if (temp != null)
                        mMusicList.add(temp);
                }
            }
        }
    }

    //添加歌曲准备完成和播放完毕的监听器
    private void addListener() {
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (mMusicStateListener != null)
                    for (MusicStateChangedListener listener : mMusicStateListener)
                        listener.musicPrepared(MusicInfo.getMusicInfoById(mMusicBinder.getId()));
                if (mPlayer != null && mMusicList.size() > 0)
                    sendNotification();
                doAfterMusicPrepared();
                //check if need to exit app
                if (TimedOffService.getExitAppFlag()) {
                    MusicPlayerApplication.getInstance().exit();
                }
            }
        });
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (mMusicStateListener != null)
                    for (MusicStateChangedListener listener : mMusicStateListener)
                        listener.musicCompletion(MusicInfo.getMusicInfoById(mMusicBinder.getId()));
                mMusicBinder.startNext();
            }
        });
    }

    //发送更新通知
    private void sendNotification() {
        //当前列表有歌曲，才能更新通知
        if (mMusicList.size() > 0) {
            PlayingNotificationInfo info = new PlayingNotificationInfo(
                    mMusicList.get(mIndex).getTitle()
                    , mMusicList.get(mIndex).getArtist() + " - " + mMusicList.get(mIndex).getAlbum()
                    , mMusicList.get(mIndex).getBitmap()
                    , R.drawable.main_icon_blue
                    , mMusicBinder.getMusicFavorite() != 1 ? R.drawable.notification_not_favorite : R.drawable.notification_favorite
                    , R.drawable.notification_last
                    , mMusicBinder.isPlaying() ? R.drawable.notification_start : R.drawable.notification_pause
                    , R.drawable.notification_next);
            mPlayingNotification.sendNotification(info, mMusicBinder.isPlaying());
        }
    }

    //切换歌曲播放前一些操作
    private void doAfterMusicPrepared() {
        if (mMusicListType == MusicListType.TYPE_MENU && mCurrentMusicMenu != null) {
            mCurrentMusicMenu.setListenCounts();
        }
        writeMusicData();
    }

    //写入音乐状态信息
    private void writeMusicData() {
        if (mPlayer != null) {
            String musicListType = mMusicListType == null ? MusicListType.TYPE_LOCAL.toString() : mMusicListType.toString();
            String playMode = mPlayMode == null ? PlayMode.RANDOM.toString() : mPlayMode.toString();
            String musicMenuName = mCurrentMusicMenu == null ? " " : mCurrentMusicMenu.getName();
            int id = 0;
            int time = 0;
            List<Integer> list = new ArrayList<Integer>();
            for (int i = 0; i < mMusicList.size(); i++)
                list.add(mMusicList.get(i).getId());
            if (list.size() > 0) {
                id = mMusicList.get(mIndex).getId();
                time = mPlayer.getCurrentPosition();
            }
            if (DataLoadManager.getInstance() != null) {
                DataLoadManager.getInstance().writeMusicData(new MusicData(musicListType, playMode
                        , musicMenuName, id, time, list));
            }
        }
    }

    //尝试获取音频焦点
    private boolean gainAudioFocus() {
        if (mHasAudioFocus)
            return true;
        int result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            if (mAudioFocusRequest == null)
                mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                        .setOnAudioFocusChangeListener(mAudioFocusChangeListener).build();
            result = mAudioManager.requestAudioFocus(mAudioFocusRequest);
        } else {
            result = mAudioManager.requestAudioFocus(mAudioFocusChangeListener, AudioManager.STREAM_MUSIC,
                    AudioManager.AUDIOFOCUS_GAIN);
        }
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
            mHasAudioFocus = true;
        return mHasAudioFocus;
    }

    //放弃音频焦点
    private void abandonAudioFocus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && mAudioFocusRequest != null) {
            mAudioManager.abandonAudioFocusRequest(mAudioFocusRequest);
        } else {
            mAudioManager.abandonAudioFocus(mAudioFocusChangeListener);
        }
    }

    private void setPlayOrPause(boolean goPlaying) {
        mVolumeIncreasing = goPlaying;
        mVolumeHandler.post(mVolumeRunnable);
    }

    public class MusicBinder extends Binder {

        //音乐播放列表是否有音乐
        public boolean hasMusic() {
            return mPlayer != null && mMusicList.size() > 0;
        }

        public void setPlayVolume(float volume) {
            if (volume >= 0.0f && volume <= 1.0f) {
                mCurrentVolume = volume;
                mPlayer.setVolume(mCurrentVolume, mCurrentVolume);
            }
        }

        //开始播放歌曲
        public void start() {
            if (mPlayer != null && mMusicList.size() > 0) {
                if (gainAudioFocus()) {
                    //mPlayer.start();
                    mIsPlaying = true;
                    setPlayOrPause(true);
                    sendNotification();
                    if (mMusicStateListener != null && mMusicList.get(mIndex) != null) {
                        for (MusicStateChangedListener listener : mMusicStateListener) {
                            listener.musicPlayStateChanged(mMusicList.get(mIndex), true);
                        }
                    }
                }
            }
        }

        //根据索引播放一首歌
        public void start(int index) {
            if (mPlayer != null && mMusicList.size() > 0 && index < mMusicList.size() && index >= 0) {
                mIndex = index;
                try {
                    mPlayer.reset();
                    mPlayer.setDataSource(mMusicList.get(mIndex).getPath());
                    mPlayer.prepare();
                    start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //播放指定类型的音乐列表，从给定音乐开始
        public void start(MusicListType type, MusicMenu musicMenu, MusicInfo musicInfo) {
            //重置当前列表
            mPlayer.reset();
            //初始化类型和歌单对象
            mMusicListType = type;
            mCurrentMusicMenu = musicMenu;
            if (mMusicListType == MusicListType.TYPE_MENU && !MusicInfo.hasMusicMenu(mCurrentMusicMenu.getName()))
                return;
            initMusicList();
            mIndex = -1;
            if (musicInfo != null && mMusicList.size() > 0) {
                for (int i = 0; i < mMusicList.size(); i++)
                    if (mMusicList.get(i).getId() == musicInfo.getId()) {
                        mIndex = i;
                        break;
                    }
            }
            if (mIndex == -1)
                mIndex = 0;
            start(mIndex);
        }

        //暂停播放
        public void pause() {
            if (mPlayer != null && mMusicList.size() > 0) {
                //mPlayer.pause();
                mIsPlaying = false;
                setPlayOrPause(false);
                sendNotification();
                if (mMusicStateListener != null && mMusicList.get(mIndex) != null) {
                    for (MusicStateChangedListener listener : mMusicStateListener) {
                        listener.musicPlayStateChanged(mMusicList.get(mIndex), false);
                    }
                }
            }
        }

        public void changePlayingState() {
            if (isPlaying()) {
                pause();
            } else {
                start();
            }
        }

        //判断是否播放中
        public boolean isPlaying() {
            if (mPlayer != null && mMusicList.size() > 0) {
                return mIsPlaying;
            } else {
                return false;
            }
        }

        //播放上一首歌曲
        public void startLast() {
            if (mPlayer != null && mMusicList.size() > 0) {
                MusicInfo currentMusic = mMusicList.get(mIndex);
                if (mMusicList.size() == 1) {
                    mIndex = 0;
                } else {
                    switch (mPlayMode) {
                        case RANDOM:
                            Random ran = new Random();
                            mIndex = ran.nextInt(mMusicList.size());
                            break;
                        case REPEAT:
                            mIndex = (mIndex + mMusicList.size() - 1) % mMusicList.size();
                            break;
                        case REPEAT_ONE:
                            break;
                        default:
                            mIndex = 0;
                            break;
                    }
                }
                if (mMusicStateListener != null) {
                    for (MusicStateChangedListener listener : mMusicStateListener) {
                        listener.musicPlayLast(currentMusic, mMusicList.get(mIndex));
                    }
                }
                start(mIndex);
            }
        }

        //播放下一首歌曲
        public void startNext() {
            if (mPlayer != null && mMusicList.size() > 0) {
                MusicInfo currentMusic = mMusicList.get(mIndex);
                if (mMusicList.size() == 1) {
                    mIndex = 0;
                } else {
                    switch (mPlayMode) {
                        case RANDOM:
                            Random ran = new Random();
                            mIndex = ran.nextInt(mMusicList.size());
                            break;
                        case REPEAT:
                            mIndex = (mIndex + 1) % mMusicList.size();
                            break;
                        case REPEAT_ONE:
                            break;
                        default:
                            mIndex = 0;
                            break;
                    }
                }
                if (mMusicStateListener != null) {
                    for (MusicStateChangedListener listener : mMusicStateListener) {
                        listener.musicPlayNext(currentMusic, mMusicList.get(mIndex));
                    }
                }
                start(mIndex);
            }
        }

        //跳转至给定时间处开始播放
        public void seekTo(int time) {
            if (mPlayer != null && mMusicList.size() > 0 && time <= mPlayer.getDuration()) {
                mPlayer.seekTo(time);
                start();
            }
        }

        //向播放列表中加入音乐
        public void addMusic(MusicListType type, MusicMenu musicMenu, List<MusicInfo> musicList) {
            if (hasMusic()) {
                for (int i = 0; i < musicList.size(); i++)
                    mMusicList.add(mIndex + i + 1, musicList.get(i));
            } else if (musicList.size() > 0) {
                mMusicListType = type;
                if (mMusicListType == MusicListType.TYPE_MENU)
                    mCurrentMusicMenu = musicMenu;
                //重置当前列表，初始化
                mPlayer.reset();
                mMusicList.clear();
                mMusicList = musicList;
                mIndex = 0;
                start(mIndex);
            }
            //保存音乐播放状态
            writeMusicData();
        }

        //得到播放列表的歌曲数量
        public int getMusicCounts() {
            return mMusicList.size();
        }

        //得到当前歌曲在列表中的索引
        public int getNowMusicPosition() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mIndex;
            else
                return 0;
        }

        //得到当前歌曲的id
        public int getId() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mMusicList.get(mIndex).getId();
            else
                return -1;
        }

        //得到当前歌曲的歌名
        public String getTitle() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mMusicList.get(mIndex).getTitle();
            else
                return "";
        }

        //得到当前歌曲的作者
        public String getArtist() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mMusicList.get(mIndex).getArtist();
            else
                return "";
        }

        //获取专辑的图片
        public Bitmap getAlbumBitmap() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mMusicList.get(mIndex).getBitmap();
            else
                return null;
        }

        //获取当前歌曲的时长
        public int getDuration() {
            if (mPlayer != null && mMusicList.size() > 0)
                return mPlayer.getDuration();
            else
                return 0;
        }

        //获取当前播放的时间进度
        public int getCurrent() {
            if (mPlayer != null)
                return mPlayer.getCurrentPosition();
            else
                return 0;
        }

        //获取歌曲的路径
        public String getPath() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mMusicList.get(mIndex).getPath();
            else
                return null;
        }

        //获取歌曲歌词文件的路径
        public String getLyricPath() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mMusicList.get(mIndex).getLyricPath();
            return null;
        }

        //获取当前播放的模式
        public PlayMode getMode() {
            return mPlayMode;
        }

        //得到歌曲被标志为喜爱的状态
        public int getMusicFavorite() {
            if (mIndex >= 0 && mIndex < mMusicList.size())
                return mMusicList.get(mIndex).getFavorite();
            return 0;
        }

        //切换播放模式
        public void changeMode() {
            switch (mPlayMode) {
                case RANDOM:
                    mPlayMode = PlayMode.REPEAT;
                    break;
                case REPEAT:
                    mPlayMode = PlayMode.REPEAT_ONE;
                    break;
                case REPEAT_ONE:
                    mPlayMode = PlayMode.RANDOM;
                    break;
            }
            MusicInfo musicInfo = mMusicList.get(mIndex);
            if (mMusicStateListener != null && musicInfo != null && musicInfo.getId() == getId()) {
                for (MusicStateChangedListener listener : mMusicStateListener) {
                    listener.musicPlayModeChanged(musicInfo, mPlayMode);
                }
            }
        }

        //当前歌曲加到我喜欢
        public void changeToFavorite() {
            if (mIndex >= 0 && mIndex < mMusicList.size()) {
                MusicInfo.changeToFavorite(mMusicList.get(mIndex));
                sendNotification();
                if (mMusicStateListener != null) {
                    for (MusicStateChangedListener listener : mMusicStateListener) {
                        listener.musicFavoriteChanged(mMusicList.get(mIndex), true);
                    }
                }
            }
        }

        //改变歌曲的喜爱属性
        public void changeMusicFavorite() {
            if (mIndex >= 0 && mIndex < mMusicList.size()) {
                changeMusicFavorite(mMusicList.get(mIndex));
            }
        }

        //更爱favorite属性
        public void changeMusicFavorite(MusicInfo music) {
            MusicInfo.changeMusicFavorite(music);
            sendNotification();
            if (mMusicStateListener != null && music != null && music.getId() == getId()) {
                for (MusicStateChangedListener listener : mMusicStateListener) {
                    listener.musicFavoriteChanged(music, music.getFavorite() == 1);
                }
            }
        }

        //得到当前播放列表
        public List<MusicInfo> getMusicList() {
            return mMusicList;
        }

        //delete one music and return a state
        public int deleteOneMusic(int position) {
            if (position != mIndex) {
                mMusicList.remove(position);
                if (position < mIndex)
                    mIndex--;
                //用handler发送消息保存状态
                if (mSaveHandler == null)
                    mSaveHandler = new Handler();
                mSaveHandler.removeCallbacks(mSaveRunnable);
                mSaveHandler.postDelayed(mSaveRunnable, 1000);
                return DELETE_ONE;
            } else {
                if (mMusicList.size() == 1) {
                    return deleteAll();
                } else {
                    int target = position;
                    if (mPlayMode == PlayMode.RANDOM) {
                        target = new Random().nextInt(mMusicList.size());
                    }
                    if (target == position)
                        target = (target + 1) % mMusicList.size();
                    mIndex = target;
                    mMusicList.remove(position);
                    if (position < mIndex)
                        mIndex--;
                    try {
                        mPlayer.reset();
                        mPlayer.setDataSource(mMusicList.get(mIndex).getPath());
                        mPlayer.prepare();
                        start();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return DELETE_CURRENT;
        }

        //delete all the music in music list
        public int deleteAll() {
            if (mPlayer != null) {
                MusicInfo lastMusicInfo = mMusicList.get(getNowMusicPosition());
                mPlayer.stop();
                mMusicList.clear();
                if (mMusicStateListener != null)
                    for (MusicStateChangedListener listener : mMusicStateListener)
                        listener.musicEmpty(lastMusicInfo);
                //保存状态信息
                writeMusicData();
                abandonAudioFocus();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    stopForeground(Service.STOP_FOREGROUND_REMOVE);
                }
            }
            return DELETE_STATE_EMPTY;
        }

        public void saveMusicPlayState() {
            writeMusicData();
        }

        //注册音乐状态改变监听器
        public void addMusicStateChangedListener(MusicStateChangedListener listener) {
            mMusicStateListener.remove(null);
            if (!mMusicStateListener.contains(listener))
                mMusicStateListener.add(listener);
        }

        //移除监听
        public void removeMusicStateChangedListener(MusicStateChangedListener listener) {
            if (mMusicStateListener != null)
                mMusicStateListener.remove(listener);
        }
    }

    public class MusicServiceBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (getString(R.string.notification_favorite).equals(intent.getAction())) {
                mMusicBinder.changeMusicFavorite();
            } else if (getString(R.string.notification_play).equals(intent.getAction())) {
                mMusicBinder.changePlayingState();
            } else if (getString(R.string.notification_last).equals(intent.getAction())) {
                mMusicBinder.startLast();
            } else if (getString(R.string.notification_next).equals(intent.getAction())) {
                mMusicBinder.startNext();
            } else if (getString(R.string.notification_cancel).equals(intent.getAction())) {
                if (mPlayingNotification != null)
                    mPlayingNotification.resetNotificationState();
            } else if (getString(R.string.notification_click).equals(intent.getAction())) {
                Intent activityIntent = new Intent(getApplicationContext(),
                        ActivityStackManager.getInstance().getTopActivity().getClass());
                activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(activityIntent);
            } else if ("android.intent.action.PHONE_STATE".equals(intent.getAction())) {
                TelephonyManager tm = (TelephonyManager) getSystemService(Service.TELEPHONY_SERVICE);
                switch (tm.getCallState()) {
                    case TelephonyManager.CALL_STATE_RINGING:
                    case TelephonyManager.CALL_STATE_OFFHOOK:
                        Log.d(TAG, "onReceive(): call state changed");
                        break;
                    case TelephonyManager.CALL_STATE_IDLE:
                        Log.d(TAG, "onReceive(): call state changed to idle");
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public enum PlayMode {
        RANDOM, REPEAT, REPEAT_ONE;
    }
}