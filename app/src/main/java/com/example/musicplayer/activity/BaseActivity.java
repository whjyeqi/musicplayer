package com.example.musicplayer.activity;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Binder;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.musicplayer.DataLoadManager;
import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.MusicStateChangedListener;
import com.example.musicplayer.commonUtils.LanguageUtil;
import com.example.musicplayer.fragment.dialogFragment.MusicListDialogFragment;
import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.service.MusicService;
import com.example.musicplayer.service.MusicTabViewService;
import com.example.musicplayer.service.TimedOffService;
import com.example.musicplayer.settings.SettingsItem;

//set app language and set music state change listener
public class BaseActivity extends AppCompatActivity implements MusicStateChangedListener
        , MusicTabViewService.CallBackListener {
    private static final String TAG = "BaseActivity";
    //if it should enable MusicStateChangedListener
    private boolean mEnableMusicStateChangedListener = false;
    private boolean mEnableMusicTabView = false;
    private boolean mIsResumed = false;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LanguageUtil.loadLanguage(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsResumed = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsResumed = false;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        MusicTabViewService.MusicTabViewBinder binder = getMusicTabViewServiceBinder();
        if (hasFocus) {
            if (mEnableMusicTabView && binder != null && binder.isViewAdded()) {
                if (this instanceof MainActivity) {
                    binder.shouldMoveView(false);
                } else {
                    binder.shouldMoveView(true);
                }
            }
            showOrHideMusicTabView();
        } else {
            if (mIsResumed && binder != null) {
                binder.shouldShowView(false);
            }
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateMusicListDialogFragment();
    }

    protected void setEnableMusicListener(boolean enable) {
        mEnableMusicStateChangedListener = enable;
    }

    protected void setEnableMusicTabView(boolean enable) {
        mEnableMusicTabView = enable;
        showOrHideMusicTabView();
    }

    public boolean isEnableMusicTabView() {
        return mEnableMusicTabView;
    }

    public void showOrHideMusicTabView() {
        if (mEnableMusicTabView) {
            MusicTabViewService.setCallBackListener(this);
        } else {
            MusicTabViewService.removeCallBackListener();
        }
        MusicTabViewService.MusicTabViewBinder binder = getMusicTabViewServiceBinder();
        if (binder != null) {
            binder.shouldShowView(mEnableMusicTabView);
        }
    }

    public void updateMusicListDialogFragment() {
        if (mEnableMusicTabView) {
            MusicTabViewService.MusicTabViewBinder binder = getMusicTabViewServiceBinder();
            if (binder != null) {
                binder.updateMusicListView();
            }
        }
    }

    protected void registerMusicListener(MusicService.MusicBinder musicBinder) {
        if (musicBinder != null) {
            musicBinder.addMusicStateChangedListener(this);
        }
    }

    protected void removeMusicListener(MusicService.MusicBinder musicBinder) {
        if (musicBinder != null) {
            musicBinder.removeMusicStateChangedListener(this);
        }
    }

    protected MusicService.MusicBinder getMusicServiceBinder() {
        Binder binder = MusicPlayerApplication.getInstance().getMusicServiceBinder();
        if (binder != null) {
            return (MusicService.MusicBinder) binder;
        }
        return null;
    }

    protected MusicTabViewService.MusicTabViewBinder getMusicTabViewServiceBinder() {
        Binder binder = MusicPlayerApplication.getInstance().getMusicTabViewServiceBinder();
        if (binder != null) {
            return (MusicTabViewService.MusicTabViewBinder) binder;
        }
        return null;
    }

    public void notExitTemporarily() {
        DataLoadManager dataLoadManager = DataLoadManager.getInstance();
        if (dataLoadManager != null) {
            dataLoadManager.writeOneBaseData(DataLoadManager.TIME_TO_EXIT_ON, false);
            dataLoadManager.writeOneBaseData(DataLoadManager.TIMING_SET, SettingsItem.TimedOffItem.TIMING_DEFAULT);
        }
        //stop timed off service
        TimedOffService.stopTimedOffService();
    }

    protected void onMusicCompletion(MusicInfo musicInfo) {
    }

    protected void onMusicPrepared(MusicInfo musicInfo) {
    }

    protected void onMusicEmpty(MusicInfo musicInfo) {
    }

    protected void onMusicFavoriteChanged(MusicInfo musicInfo, boolean result) {
    }

    protected void onMusicPlayStateChanged(MusicInfo musicInfo, boolean isPlaying) {
    }

    protected void onMusicPlayModeChanged(MusicInfo musicInfo, MusicService.PlayMode mode) {

    }

    protected void onPlayNext(MusicInfo beforeMusic, MusicInfo afterMusic) {
    }

    protected void onPlayLast(MusicInfo beforeMusic, MusicInfo afterMusic) {
    }

    @Override
    public void musicCompletion(MusicInfo musicInfo) {
        if (mEnableMusicStateChangedListener) {
            onMusicCompletion(musicInfo);
        }
    }

    @Override
    public void musicPrepared(MusicInfo musicInfo) {
        if (mEnableMusicStateChangedListener) {
            onMusicPrepared(musicInfo);
        }
        updateMusicListDialogFragment();
    }

    @Override
    public void musicEmpty(MusicInfo musicInfo) {
        if (mEnableMusicStateChangedListener) {
            onMusicEmpty(musicInfo);
        }
    }

    @Override
    public void musicFavoriteChanged(MusicInfo musicInfo, boolean result) {
        if (mEnableMusicStateChangedListener) {
            onMusicFavoriteChanged(musicInfo, result);
        }
    }

    @Override
    public void musicPlayStateChanged(MusicInfo musicInfo, boolean isPlaying) {
        if (mEnableMusicStateChangedListener) {
            onMusicPlayStateChanged(musicInfo, isPlaying);
        }
    }

    @Override
    public void musicPlayModeChanged(MusicInfo musicInfo, MusicService.PlayMode mode) {
        if (mEnableMusicStateChangedListener) {
            onMusicPlayModeChanged(musicInfo, mode);
        }
    }

    @Override
    public void musicPlayNext(MusicInfo beforeMusic, MusicInfo afterMusic) {
        if (mEnableMusicStateChangedListener) {
            onPlayNext(beforeMusic, afterMusic);
        }
    }

    @Override
    public void musicPlayLast(MusicInfo beforeMusic, MusicInfo afterMusic) {
        if (mEnableMusicStateChangedListener) {
            onPlayLast(beforeMusic, afterMusic);
        }
    }

    @Override
    public void showMusicList(MusicListDialogFragment musicListDialogFragment) {
        if (musicListDialogFragment != null) {
            musicListDialogFragment.show(getSupportFragmentManager(), "musicListDialog");
        }
    }

    @Override
    public void jumpToMusicPlayActivity() {
        if (mEnableMusicTabView) {
            MusicPlayerApplication.getInstance().jumpToMusicPlayActivity();
        }
    }
}