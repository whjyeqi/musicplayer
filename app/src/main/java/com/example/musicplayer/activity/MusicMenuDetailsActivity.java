package com.example.musicplayer.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.DisplayUtil;
import com.example.musicplayer.commonUtils.DrawUtil;
import com.example.musicplayer.dialogTools.SortMusicDialog;
import com.example.musicplayer.musicTools.MusicListType;
import com.example.musicplayer.musicTools.MusicSortTool;
import com.example.musicplayer.musicTools.MusicSortType;
import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.musicClass.MusicMenu;
import com.example.musicplayer.service.MusicService;
import com.example.musicplayer.settings.MusicMenuSettings;
import com.example.musicplayer.user.User;
import com.example.musicplayer.view.MusicSimpleDisplay;
import com.example.musicplayer.view.NewScrollView;
import com.example.musicplayer.view.SimpleDisplayView;
import com.example.musicplayer.windowTools.WindowTools;

import java.util.ArrayList;
import java.util.List;

public class MusicMenuDetailsActivity extends BaseActivity implements View.OnClickListener {
    public static final String TAG = "MusicMenuDetailActivity";
    public static final int RESULT_CODE = 200;
    private ImageView mImageViewPlay;
    private ImageView mImageViewPlay1;
    private TextView mTextViewPlayInfo;
    private TextView mTextViewPlayInfo1;
    private ImageView mImageViewManage;
    private ImageView mImageViewManage1;
    private ImageView mImageViewSort;
    private ImageView mImageViewSort1;
    private ImageView mImageViewAdd;
    private ImageView mImageViewAdd1;
    private ImageView mImageViewLocation;
    private ImageView mImageViewLocation1;
    private LinearLayout mMusicContainer;
    private LinearLayout mEmptyView;
    private Button mButtonAdd;
    private LinearLayout mToolbar;
    private TextView mToolbarText;
    private LinearLayout mImageViewFinish;
    private ImageView mImageViewMenu;
    private RelativeLayout mControlTab;
    private RelativeLayout mControlTabMove;
    private RelativeLayout mInfoTab;
    private ImageView mImageViewCollect;
    private ImageView mImageViewComment;
    private ImageView mImageViewShare;
    private ImageView mImageViewAlbum;
    private ImageView mImageViewUser;
    private TextView mTextViewCollect;
    private TextView mTextViewComment;
    private TextView mTextViewShare;
    private TextView mTextViewMenuName;
    private TextView mTextViewEditIntroduction;
    private TextView mTextViewViewIntroduction;
    private TextView mListenCounts;
    private NewScrollView mScrollView;
    private List<Integer> mMusic = new ArrayList<Integer>();
    private List<MusicSimpleDisplay> mDisplay = new ArrayList<MusicSimpleDisplay>();
    private String mMusicMenuName;
    private Bitmap mBitmapAlbum;
    private int mScrollDistanceLimit;
    private int mRealScreenHeight;
    private int mScrollContentHeight;
    //对歌单是否调整
    private boolean mStateChanged = false;
    //是否更新整个页面
    private boolean mShouldUpdate = true;
    private boolean mIsToBottom = true;
    private MusicService.MusicBinder mMusicBinder;
    //更新视图线程
    private Handler mUpdateHandler;
    private Runnable mUpdateMusicRunnable = new Runnable() {
        @Override
        public void run() {
            updateMusicView();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_menu_details);
        if ((mMusicBinder = getMusicServiceBinder()) == null) {
            finish();
        }
        //设置沉浸式状态栏
        WindowTools.setStatusBarTransparent(getWindow());
        initView();
        addListener();
        Intent intent = getIntent();
        mMusicMenuName = intent.getStringExtra("musicMenuName");
        mScrollDistanceLimit = (int) getResources().getDimension(R.dimen.activity_music_menu_details_info_tab_height) -
                (int) getResources().getDimension(R.dimen.activity_music_menu_details_toolbar_height);
        mRealScreenHeight = DisplayUtil.getRealScreenSize(this)[1];
        setEnableMusicListener(true);
        //show music playing info tab
        setEnableMusicTabView(true);
        registerMusicListener(mMusicBinder);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && mShouldUpdate) {
            updateAll();
            mShouldUpdate = false;
        }
        if (mMusicBinder != null && hasFocus) {
            registerMusicListener(mMusicBinder);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mMusicBinder != null) {
            removeMusicListener(mMusicBinder);
        }
        mMusicBinder = null;
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult(): requestCode is " + requestCode + ", resultCode is " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == MusicListManageActivity.RESULT_CODE && data != null) {
            if (data.getBooleanExtra("result", true)) {
                mStateChanged = true;
                //更新视图
                updateAll();
            }
        }
        if (resultCode == EditMusicMenuDetailsActivity.RESULT_CODE && data != null) {
            if (data.getBooleanExtra("result", true)) {
                mStateChanged = true;
                String newName = data.getStringExtra("musicMenuName");
                updateIntroductionChanged(newName);
                updateView();
            }
        }
    }

    private void initView() {
        mImageViewPlay = findViewById(R.id.imageview_music_menu_details_play);
        mTextViewPlayInfo = findViewById(R.id.textview_music_menu_details_play_info);
        mImageViewManage = findViewById(R.id.imageview_music_menu_details_manage);
        mImageViewSort = findViewById(R.id.imageview_music_menu_details_sort);
        mImageViewAdd = findViewById(R.id.imageview_music_menu_details_add);
        mImageViewLocation = findViewById(R.id.imageview_music_menu_details_location);
        mImageViewPlay1 = findViewById(R.id.imageview_music_menu_details_play1);
        mTextViewPlayInfo1 = findViewById(R.id.textview_music_menu_details_play_info1);
        mImageViewManage1 = findViewById(R.id.imageview_music_menu_details_manage1);
        mImageViewSort1 = findViewById(R.id.imageview_music_menu_details_sort1);
        mImageViewAdd1 = findViewById(R.id.imageview_music_menu_details_add1);
        mImageViewLocation1 = findViewById(R.id.imageview_music_menu_details_location1);
        mImageViewFinish = findViewById(R.id.imageview_music_menu_details_finish);
        mImageViewMenu = findViewById(R.id.imageview_music_menu_details_menu);
        mToolbarText = findViewById(R.id.textview_music_menu_details_toolbar);
        mButtonAdd = findViewById(R.id.button_add_music);
        mMusicContainer = findViewById(R.id.linear_layout_music_list_container);
        mToolbar = findViewById(R.id.toolbar_music_menu_details);
        mControlTab = findViewById(R.id.relative_layout_control_tab);
        mControlTabMove = findViewById(R.id.relative_layout_control_tab_move);
        mInfoTab = findViewById(R.id.relative_layout_info_tab);
        mImageViewCollect = findViewById(R.id.imageview_music_menu_details_collect);
        mImageViewComment = findViewById(R.id.imageview_music_menu_details_comment);
        mImageViewShare = findViewById(R.id.imageview_music_menu_details_share);
        mImageViewAlbum = findViewById(R.id.imageview_music_menu_details_album);
        mImageViewUser = findViewById(R.id.imageview_music_menu_details_user);
        mTextViewCollect = findViewById(R.id.textview_music_menu_details_collect);
        mTextViewComment = findViewById(R.id.textview_music_menu_details_comment);
        mTextViewShare = findViewById(R.id.textview_share);
        mTextViewMenuName = findViewById(R.id.textview_music_menu_details_menu_name);
        mTextViewEditIntroduction = findViewById(R.id.textview_music_menu_details_view1);
        mTextViewViewIntroduction = findViewById(R.id.textview_music_menu_details_introduction);
        mListenCounts = findViewById(R.id.textview_music_menu_details_listen_counts);
        mEmptyView = findViewById(R.id.linear_layout_music_menu_details_empty);
        mScrollView = findViewById(R.id.scrollview_content);
        //设置轮廓
        DisplayUtil.setRoundRectOutline(mImageViewAlbum,
                0,
                0,
                (int) getResources().getDimension(R.dimen.activity_music_menu_details_image_size),
                (int) getResources().getDimension(R.dimen.activity_music_menu_details_image_size),
                (int) getResources().getDimension(R.dimen.big_image_radius));
        DisplayUtil.setRoundOutline(mImageViewUser,
                0,
                0,
                (int) getResources().getDimension(R.dimen.user_photo_size_small),
                (int) getResources().getDimension(R.dimen.user_photo_size_small));
        //set current user's name
        ((TextView) findViewById(R.id.textview_current_user_name)).setText(User.getInstance().getUserName());
    }

    private void addListener() {
        mImageViewPlay.setOnClickListener(this);
        mTextViewPlayInfo.setOnClickListener(this);
        mImageViewManage.setOnClickListener(this);
        mImageViewSort.setOnClickListener(this);
        mImageViewAdd.setOnClickListener(this);
        mImageViewLocation.setOnClickListener(this);
        mImageViewPlay1.setOnClickListener(this);
        mTextViewPlayInfo1.setOnClickListener(this);
        mImageViewManage1.setOnClickListener(this);
        mImageViewSort1.setOnClickListener(this);
        mImageViewAdd1.setOnClickListener(this);
        mImageViewLocation1.setOnClickListener(this);
        mImageViewFinish.setOnClickListener(this);
        mImageViewMenu.setOnClickListener(this);
        mButtonAdd.setOnClickListener(this);
        mImageViewCollect.setOnClickListener(this);
        mImageViewComment.setOnClickListener(this);
        mImageViewShare.setOnClickListener(this);
        mImageViewAlbum.setOnClickListener(this);
        mTextViewCollect.setOnClickListener(this);
        mTextViewComment.setOnClickListener(this);
        mTextViewShare.setOnClickListener(this);
        mTextViewMenuName.setOnClickListener(this);
        mTextViewEditIntroduction.setOnClickListener(this);
        setScrollViewListener();
    }

    private void addListener(final MusicSimpleDisplay display) {
        display.getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doClickPlay(display.getNumber() - 1);
            }
        });
        display.getView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                doClickManage();
                return true;
            }
        });
        display.getImageViewVideo().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        display.getImageViewMore().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void setScrollViewListener() {
        mScrollView.setOnScrollListener(new NewScrollView.OnScrollListener() {
            @Override
            public void onScrollChanged(int y, int oldY) {
                if (y != oldY) {
                    updateToolbar();
                }
                if (y >= mScrollContentHeight - mRealScreenHeight) {
                    setLocationState(false);
                } else if (y <= 0) {
                    setLocationState(true);
                }
            }
        });
    }

    //更新整个页面
    private void updateAll() {
        updateStateChanged();
        if (mUpdateHandler == null) {
            mUpdateHandler = new Handler();
        }
        mUpdateHandler.postDelayed(mUpdateMusicRunnable, 0);
    }

    //增删或调整歌曲顺序后，更新
    private void updateStateChanged() {
        updateView();
    }

    //更新视图显示
    private void updateView() {
        MusicMenu musicMenu = MusicInfo.getMusicMenuByName(mMusicMenuName);
        //歌单中歌曲数量
        int musicCounts = musicMenu == null ? 0 : musicMenu.getCounts();
        mScrollContentHeight = mInfoTab.getHeight();
        if (musicCounts == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
            mControlTabMove.setVisibility(View.GONE);
        } else {
            mEmptyView.setVisibility(View.GONE);
            mControlTabMove.setVisibility(View.VISIBLE);
            //显示歌单音乐数量
            String text = getResources().getString(R.string.play_all_music) + "(" + musicCounts + ")";
            mTextViewPlayInfo.setText(text);
            mTextViewPlayInfo1.setText(text);
        }
        if (musicCounts >= MusicMenuSettings.getLeastMusicCountsUseLocation()) {
            mImageViewLocation.setVisibility(View.VISIBLE);
            mImageViewLocation1.setVisibility(View.VISIBLE);
        } else {
            mImageViewLocation.setVisibility(View.GONE);
            mImageViewLocation1.setVisibility(View.GONE);
        }
        mTextViewMenuName.setText(mMusicMenuName);
        setAlbumBitmap();
        setTabBackground();
        updateMusicMenuIntroduction();
        updateToolbar();
        updateMusicMenuListenCounts();
    }

    //设置歌单图片
    private void setAlbumBitmap() {
        //获取歌单封面图
        MusicMenu musicMenu = MusicInfo.getMusicMenuByName(mMusicMenuName);
        if (musicMenu == null)
            mBitmapAlbum = DrawUtil.getDefaultMusicMenuBitmap();
        else
            mBitmapAlbum = musicMenu.getBitmap();
        mImageViewUser.setImageBitmap(mBitmapAlbum);
        Bitmap bitmap = DrawUtil.copyBitmap(mBitmapAlbum);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawColor(Color.parseColor("#44000000"));
        mImageViewAlbum.setImageBitmap(bitmap);
    }

    //更新播放次数
    private void updateMusicMenuListenCounts() {
        MusicMenu musicMenu = MusicInfo.getMusicMenuByName(mMusicMenuName);
        if (musicMenu != null) {
            int listenCounts = musicMenu.getListenCounts();
            mListenCounts.setText(String.valueOf(listenCounts));
        }
    }

    //更新简介
    private void updateMusicMenuIntroduction() {
        MusicMenu musicMenu = MusicInfo.getMusicMenuByName(mMusicMenuName);
        if (musicMenu != null) {
            String text = musicMenu.getIntroduction();
            if (text != null)
                mTextViewViewIntroduction.setText(text);
        }
        mTextViewViewIntroduction.setTextColor(DrawUtil.getBodyTextColorOfPicture(mBitmapAlbum, DrawUtil.VIBRANT));
    }

    //更新toolbar显示
    private void updateToolbar() {
        if (mScrollView.getScrollY() >= mScrollDistanceLimit) {
            mControlTab.setVisibility(View.VISIBLE);
        } else {
            mControlTab.setVisibility(View.GONE);
        }
        float distance = mScrollView.getScrollY();
        //toolbar的透明度
        int alpha = (int) (distance * 1.1f / (float) mScrollDistanceLimit * 255);
        //toolbar文字的透明度
        float alphaText = distance * 2 / (float) mScrollDistanceLimit;
        alpha = alpha < 0 ? 0 : Math.min(alpha, 255);
        alphaText = alphaText < 0 ? 0 : Math.min(alphaText, 2);
        mToolbar.getBackground().setAlpha(alpha);
        if (alphaText <= 1.0f) {
            mToolbarText.setText(R.string.music_menu);
            mToolbarText.setAlpha(1 - alphaText);
        } else {
            mToolbarText.setText(mMusicMenuName);
            mToolbarText.setAlpha(alphaText - 1);
        }
    }

    //设置顶部栏的背景色
    private void setTabBackground() {
        Bitmap bitmap = DrawUtil.getColorBitmap(Color.WHITE);
        int startColor = DrawUtil.getMainColorOfPicture(mBitmapAlbum, DrawUtil.DARK_VIBRANT);
        int endColor = DrawUtil.getMainColorOfPicture(mBitmapAlbum, DrawUtil.VIBRANT);
        bitmap = DrawUtil.getSlantedGradientBitmap(bitmap, startColor, endColor);
        mInfoTab.setBackground(new BitmapDrawable(getResources(), bitmap));
        mToolbar.setBackground(new BitmapDrawable(getResources(), bitmap));
    }

    //在末尾插入歌曲项视图
    private void addNewMusicView(int index) {
        MusicSimpleDisplay display = new MusicSimpleDisplay(SimpleDisplayView.TYPE_DISPLAY, index);
        mMusicContainer.addView(display.getView());
        mDisplay.add(display);
        addListener(display);
    }

    //更新歌曲列表项视图
    private void updateMusicView() {
        MusicMenu musicMenu = MusicInfo.getMusicMenuByName(mMusicMenuName);
        //获取排序结果
        mMusic = MusicSortTool.sort(mMusicMenuName);
        int num = mMusic.size() - mDisplay.size();
        if (num > 0) {
            int index = mDisplay.size();
            for (int i = 1; i <= num; i++)
                addNewMusicView(index + i);
        }
        if (num < 0) {
            for (int i = -1; i >= num; i--) {
                mDisplay.remove(mDisplay.size() - 1);
                mMusicContainer.removeViewAt(mMusicContainer.getChildCount() - 1);
            }
        }
        if (mDisplay.size() > 0 && mInfoTab != null) {
            mScrollContentHeight = mInfoTab.getHeight();
            mScrollContentHeight += mControlTabMove.getHeight()
                    + mDisplay.get(0).getViewHeight() * mDisplay.size();
        } else {
            mScrollContentHeight = 0;
        }
        //收听次数是否可见
        boolean isVisible = musicMenu != null && (musicMenu.getMusicSortType() == MusicSortType.SORT_LISTEN_COUNTS);
        for (int i = 0; i < mMusic.size(); i++) {
            mDisplay.get(i).update(MusicInfo.getMusicInfoById(mMusic.get(i)));
            mDisplay.get(i).enableListenCountsInfo(isVisible);
            mDisplay.get(i).markAsPlaying((mMusicBinder != null) && (mMusic.get(i) == mMusicBinder.getId()));
        }
    }

    //歌单编辑简介，更新信息
    private void updateIntroductionChanged(String newName) {
        if (newName != null)
            mMusicMenuName = newName;
        mTextViewMenuName.setText(mMusicMenuName);
        updateMusicMenuIntroduction();
    }

    //播放歌单全部歌曲
    private void doClickPlay(int index) {
        if (mMusic.size() > 0) {
            if (index < 0 || index >= mMusic.size()) {
                index = 0;
            }
            MusicInfo clickMusic = mDisplay.get(index).getMusicInfo();
            if (mMusicBinder != null && clickMusic != null && clickMusic.getId() != mMusicBinder.getId()) {
                mMusicBinder.start(MusicListType.TYPE_MENU, MusicInfo.getMusicMenuByName(mMusicMenuName)
                        , mDisplay.get(index).getMusicInfo());
                jumpToMusicPlayActivity();
            }
        }
    }

    //管理此歌单
    private void doClickManage() {
        Intent intent = new Intent(this, MusicListManageActivity.class);
        intent.putExtra("musicListType", MusicListType.TYPE_MENU);
        intent.putExtra("musicMenuName", mMusicMenuName);
        jumpToActivity(intent);
    }

    //点击歌曲排序选择功能
    private void doClickSort() {
        final MusicMenu musicMenu = MusicInfo.getMusicMenuByName(mMusicMenuName);
        //获取歌单的当前排序类型
        MusicSortType type = musicMenu == null ? MusicSortType.SORT_DEFAULT : musicMenu.getMusicSortType();
        //构造排序选择对话框
        SortMusicDialog sortMusicDialog = new SortMusicDialog(this, type);
        final Dialog dialog = sortMusicDialog.getDialog();
        View sortDefault = sortMusicDialog.getSortDefault();
        View sortDefaultReverse = sortMusicDialog.getSortDefaultReverse();
        View sortTitle = sortMusicDialog.getSortTitle();
        View sortArtist = sortMusicDialog.getSortArtist();
        View sortListenCounts = sortMusicDialog.getSortListenCounts();
        View cancel = sortMusicDialog.getCancel();
        //监听事件
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        sortDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicMenu != null)
                    musicMenu.setMusicSortType(MusicSortType.SORT_DEFAULT);
                updateMusicView();
                dialog.dismiss();
            }
        });
        sortDefaultReverse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicMenu != null)
                    musicMenu.setMusicSortType(MusicSortType.SORT_DEFAULT_REVERSE);
                updateMusicView();
                dialog.dismiss();
            }
        });
        sortTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicMenu != null)
                    musicMenu.setMusicSortType(MusicSortType.SORT_TITLE);
                updateMusicView();
                dialog.dismiss();
            }
        });
        sortArtist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicMenu != null)
                    musicMenu.setMusicSortType(MusicSortType.SORT_ARTIST);
                updateMusicView();
                dialog.dismiss();
            }
        });
        sortListenCounts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicMenu != null)
                    musicMenu.setMusicSortType(MusicSortType.SORT_LISTEN_COUNTS);
                updateMusicView();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    //从本地音乐中添加歌曲到歌单
    private void doClickAdd() {
        Intent intent = new Intent(this, MusicListManageActivity.class);
        intent.putExtra("musicListType", MusicListType.TYPE_LOCAL);
        jumpToActivity(intent);
    }

    private void doClickAddTo() {
        doClickAdd();
    }

    private void doClickLocation() {
        int firstIndex = firstItemIndex();
        int markedIndex = markedItemIndex();
        if (mIsToBottom) {
            if (firstIndex >= markedIndex) {
                mScrollView.smoothScrollTo(0, mScrollContentHeight);
            } else {
                scrollToMarkedItem();
            }
        } else {
            if (firstIndex <= markedIndex) {
                mScrollView.smoothScrollTo(0, 0);
            } else {
                scrollToMarkedItem();
            }
        }
    }

    private void doClickFinish() {
        finishActivity();
    }

    private void doClickMenu() {

    }

    private void doClickCollect() {

    }

    private void doClickComment() {

    }

    private void doClickShare() {

    }

    private void doClickIntroduction() {

    }

    //编辑歌单详情
    private void doClickEditIntroduction() {
        Intent intent = new Intent(this, EditMusicMenuDetailsActivity.class);
        intent.putExtra("musicMenuName", mMusicMenuName);
        jumpToActivity(intent);
    }

    //跳转到其它activity
    private void jumpToActivity(Intent intent) {
        startActivityForResult(intent, 1);
        overridePendingTransition(R.anim.activity_vertical_enter, R.anim.fake_anim);
    }

    //结束activity
    private void finishActivity() {
        Intent intent = new Intent();
        intent.putExtra("result", mStateChanged);
        this.setResult(RESULT_CODE, intent);
        finish();
        overridePendingTransition(R.anim.slow_activity_enter, R.anim.normal_activity_exit);
    }

    //计算第一个可见item项的索引
    private int firstItemIndex() {
        if (mMusic.size() == 0) {
            return 0;
        } else {
            int itemHeight = mDisplay.get(0).getViewHeight();
            int rest = (int) mScrollView.getScrollY() - mScrollDistanceLimit;
            if (rest <= 0) {
                return 0;
            } else {
                return rest / itemHeight;
            }
        }
    }

    //获取当前播放歌曲的索引
    private int markedItemIndex() {
        if (mDisplay.size() <= 0) {
            return -1;
        } else {
            for (int i = 0; i < mDisplay.size(); i++) {
                if (mDisplay.get(i).isMarkAsPlaying()) {
                    return i;
                }
            }
            return -1;
        }
    }

    private void setLocationState(boolean isToBottom) {
        mIsToBottom = isToBottom;
        if (mIsToBottom) {
            mImageViewLocation.setImageResource(R.drawable.icon_to_bottom);
            mImageViewLocation1.setImageResource(R.drawable.icon_to_bottom);
        } else {
            mImageViewLocation.setImageResource(R.drawable.icon_to_top);
            mImageViewLocation1.setImageResource(R.drawable.icon_to_top);
        }
    }

    //滑动到标记的歌曲处
    private void scrollToMarkedItem() {
        int markedIndex = Math.max(markedItemIndex(), 0);
        int distance = mScrollDistanceLimit + mDisplay.get(0).getViewHeight() * markedIndex;
        mScrollView.smoothScrollTo(0, distance);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageview_music_menu_details_play:
            case R.id.imageview_music_menu_details_play1:
            case R.id.textview_music_menu_details_play_info:
            case R.id.textview_music_menu_details_play_info1:
                doClickPlay(0);
                break;
            case R.id.imageview_music_menu_details_manage:
            case R.id.imageview_music_menu_details_manage1:
                doClickManage();
                break;
            case R.id.imageview_music_menu_details_sort:
            case R.id.imageview_music_menu_details_sort1:
                doClickSort();
                break;
            case R.id.imageview_music_menu_details_add:
            case R.id.imageview_music_menu_details_add1:
                doClickAddTo();
                break;
            case R.id.imageview_music_menu_details_location:
            case R.id.imageview_music_menu_details_location1:
                doClickLocation();
                break;
            case R.id.button_add_music:
                doClickAdd();
                break;
            case R.id.imageview_music_menu_details_finish:
                doClickFinish();
                break;
            case R.id.imageview_music_menu_details_menu:
                doClickMenu();
                break;
            case R.id.imageview_music_menu_details_collect:
            case R.id.textview_music_menu_details_collect:
                doClickCollect();
                break;
            case R.id.imageview_music_menu_details_comment:
            case R.id.textview_music_menu_details_comment:
                doClickComment();
                break;
            case R.id.imageview_music_menu_details_share:
            case R.id.textview_share:
                doClickShare();
                break;
            case R.id.imageview_music_menu_details_album:
            case R.id.textview_music_menu_details_menu_name:
                doClickIntroduction();
                break;
            case R.id.textview_music_menu_details_view1:
                doClickEditIntroduction();
                break;
        }
    }

    @Override
    public void onMusicPrepared(MusicInfo musicInfo) {
        if (musicInfo != null && mDisplay != null) {
            for (int i = 0; i < mDisplay.size(); i++) {
                MusicInfo temp = mDisplay.get(i).getMusicInfo();
                mDisplay.get(i).markAsPlaying((temp != null) && (temp.getId() == musicInfo.getId()));
            }
        }
    }

    @Override
    public void onMusicEmpty(MusicInfo musicInfo) {
        if (mMusic != null && musicInfo != null) {
            for (int i = 0; i < mMusic.size(); i++)
                if (mMusic.get(i) == musicInfo.getId() && mDisplay != null && mDisplay.size() > i)
                    mDisplay.get(i).markAsPlaying(false);
        }
    }
}