package com.example.musicplayer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.musicplayer.R;
import com.example.musicplayer.adapter.BaseFragmentPagerAdapter;
import com.example.musicplayer.fragment.musicmenu.CollectionMusicMenuFragment;
import com.example.musicplayer.fragment.musicmenu.MyMusicMenuFragment;
import com.example.musicplayer.windowTools.WindowTools;

import java.util.ArrayList;
import java.util.List;

public class MusicMenuActivity extends BaseActivity implements View.OnClickListener {
    public static final String TAG = "MusicMenuActivity";
    private TextView mTextViewMine;
    private TextView mTextViewCollection;
    private ViewPager mViewPager;
    private MyMusicMenuFragment mMyMusicMenuFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_menu);
        WindowTools.lightStatusBar(getWindow());

        initView();
        addListener();
        //show music playing info tab
        setEnableMusicTabView(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slow_activity_enter, R.anim.normal_activity_exit);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            overridePendingTransition(R.anim.slow_activity_enter, R.anim.normal_activity_exit);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult(): requestCode is " + requestCode + ", resultCode is " + resultCode);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == MusicMenuManageActivity.RESULT_CODE && data != null) {
            if (data.getBooleanExtra("result", true))
                mMyMusicMenuFragment.updateStateChanged();
        }
        if (resultCode == MusicMenuDetailsActivity.RESULT_CODE && data != null) {
            if (data.getBooleanExtra("result", true))
                mMyMusicMenuFragment.updateStateChanged();
        }
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar_music_menu);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //添加向上操作
            actionBar.setDisplayHomeAsUpEnabled(true);
            //去除toolbar中的标题
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mTextViewMine = findViewById(R.id.textview_music_menu_my);
        mTextViewCollection = findViewById(R.id.textview_music_menu_collection);
        mViewPager = findViewById(R.id.viewpager_music_menu);
        mMyMusicMenuFragment = new MyMusicMenuFragment();
        List<Fragment> list = new ArrayList<Fragment>();
        list.add(mMyMusicMenuFragment);
        list.add(new CollectionMusicMenuFragment());
        mViewPager.setAdapter(new BaseFragmentPagerAdapter(getSupportFragmentManager(), 1, list));
        mViewPager.setCurrentItem(0);
        mTextViewCollection.setTextColor(getResources().getColor(R.color.activity_music_menu_tab_unselected));
    }

    private void addListener() {
        mTextViewMine.setOnClickListener(this);
        mTextViewCollection.setOnClickListener(this);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateTopTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //更新页面顶部栏的选择状态
    private void updateTopTab(int position) {
        switch (position) {
            case 0:
                mTextViewMine.setTextColor(getResources().getColor(R.color.activity_music_menu_tab_selected));
                mTextViewCollection.setTextColor(getResources().getColor(R.color.activity_music_menu_tab_unselected));
                break;
            case 1:
                mTextViewMine.setTextColor(getResources().getColor(R.color.activity_music_menu_tab_unselected));
                mTextViewCollection.setTextColor(getResources().getColor(R.color.activity_music_menu_tab_selected));
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textview_music_menu_my:
                updateTopTab(0);
                mViewPager.setCurrentItem(0);
                break;
            case R.id.textview_music_menu_collection:
                updateTopTab(1);
                mViewPager.setCurrentItem(1);
                break;
            default:
                break;
        }
    }
}