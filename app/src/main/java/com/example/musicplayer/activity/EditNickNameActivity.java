package com.example.musicplayer.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.user.User;

public class EditNickNameActivity extends BaseSettingsActivity {
    private static final int NAME_LENGTH_LIMIT = 20;
    private EditText mEditNickName;
    private TextView mInputDescription;
    private Button mConfirm;
    private View mClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setEnableMusicTabView(false);
        loadEditView();
        mEditNickName.setText(User.getInstance().getUserName());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mEditNickName.requestFocus();
    }

    @Override
    protected int setActivityTitle() {
        return R.string.title_edit_nick_name_activity;
    }

    private void loadEditView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        @SuppressLint("InflateParams") View editView = inflater.inflate(R.layout.edit_nick_name_view, null);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT
                , RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
        ((RelativeLayout) findViewById(R.id.relative_layout_base_settings)).addView(editView, lp);
        //edit text
        mEditNickName = editView.findViewById(R.id.edittext_input_nick_name);
        mEditNickName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                update(s.toString());
            }
        });
        //clear
        mClear = editView.findViewById(R.id.clear_nick_name);
        mClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditNickName.setText("");
            }
        });
        //nick name description
        mInputDescription = editView.findViewById(R.id.nick_name_description);
        //button confirm
        mConfirm = editView.findViewById(R.id.button_save_nick_name);
        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mConfirm.isEnabled()) {
                    String toastText = User.getInstance().setUserName(mEditNickName.getText().toString()) ?
                            getString(R.string.save_success) : getString(R.string.save_fail);
                    ToastUtil.makeToast(toastText);
                    finishActivity();
                }
            }
        });
    }

    private void update(String nickName) {
        if (nickName.length() == 0) {
            mClear.setVisibility(View.GONE);
            mInputDescription.setVisibility(View.INVISIBLE);
            mConfirm.setEnabled(false);
        } else {
            mClear.setVisibility(View.VISIBLE);
            mInputDescription.setVisibility(View.VISIBLE);
            String des;
            if (nickName.length() <= NAME_LENGTH_LIMIT) {
                mConfirm.setEnabled(true);
                des = nickName.length() + " / " + NAME_LENGTH_LIMIT;
                mInputDescription.setTextColor(getResources().getColor(R.color.deep_silver));
            } else {
                mConfirm.setEnabled(false);
                des = getString(R.string.nick_name_too_long);
                mInputDescription.setTextColor(getResources().getColor(R.color.red));
            }
            mInputDescription.setText(des);
        }
    }
}