package com.example.musicplayer.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.example.musicplayer.DataLoadManager;
import com.example.musicplayer.R;
import com.example.musicplayer.settings.SettingsItem;
import com.example.musicplayer.view.SettingsView;
import com.example.musicplayer.windowTools.WindowTools;

import java.util.ArrayList;
import java.util.List;

public class BaseSettingsActivity extends BaseActivity {
    protected DataLoadManager mDataLoadManager;
    private LinearLayout mSettingsContainer;
    private List<SettingsView> mSettingsViews = new ArrayList<SettingsView>();

    private SettingsView.SettingsClickListener mSettingsClickListener = new SettingsView.SettingsClickListener() {
        @Override
        public void doClick(SettingsView settingsView, boolean isChecked) {
            onSettingsItemClick(settingsView, isChecked);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_settings);
        WindowTools.lightStatusBar(getWindow());
        getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        setEnableMusicTabView(true);
        //set toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_base_settings);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        initView();
        //data load manager
        if ((mDataLoadManager = DataLoadManager.getInstance()) == null) {
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        for (int i = 0; i < mSettingsViews.size(); i++) {
            updateSettingsItem(mSettingsViews.get(i));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slow_activity_enter, R.anim.normal_activity_exit);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        //find view
        ((TextView) findViewById(R.id.settings_title)).setText(setActivityTitle());
        mSettingsContainer = findViewById(R.id.settings_container);
    }

    protected int setActivityTitle() {
        return R.string.description_null;
    }

    @SuppressLint("InflateParams")
    protected View setSettingsDivideView() {
        View divideView = LayoutInflater.from(this).inflate(R.layout.base_settings_divide_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.base_settings_divide_height));
        divideView.setLayoutParams(lp);
        return divideView;
    }

    protected View setSettingsDivideView(String groupLabel) {
        return setSettingsDivideView();
    }

    protected void updateSettingsItem(SettingsView settingsView) {
    }

    protected LinearLayout getSettingsContainer() {
        return mSettingsContainer;
    }

    protected SettingsView.SettingsClickListener getSettingsClickListener() {
        return mSettingsClickListener;
    }

    protected SettingsView getSettingsView(int settingsItem) {
        for (int i = 0; i < mSettingsViews.size(); i++) {
            if (mSettingsViews.get(i).getSettingsItem() == settingsItem) {
                return mSettingsViews.get(i);
            }
        }
        return null;
    }

    protected void setSettingsItem(int settingsItem) {
        SettingsView view = SettingsItem.createSettingsItem(settingsItem);
        view.setClickListener(mSettingsClickListener);
        mSettingsContainer.addView(view.getSettingsView());
        mSettingsViews.add(view);
    }

    protected void setSettingsDivideItem() {
        mSettingsContainer.addView(setSettingsDivideView());
    }

    protected void setSettingsDivideItem(String groupLabel) {
        mSettingsContainer.addView(setSettingsDivideView(groupLabel));
    }

    protected void onSettingsItemClick(SettingsView settingsView, boolean isChecked) {
    }

    protected void startActivityInSettings(Intent intent) {
        startActivity(intent);
        overridePendingTransition(R.anim.normal_activity_enter, R.anim.slow_activity_exit);
    }

    protected void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slow_activity_enter, R.anim.normal_activity_exit);
    }
}