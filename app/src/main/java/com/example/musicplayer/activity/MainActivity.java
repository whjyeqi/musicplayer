package com.example.musicplayer.activity;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musicplayer.DataLoadManager;
import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.adapter.BaseFragmentPagerAdapter;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.fragment.MainTab1Fragment;
import com.example.musicplayer.fragment.MainTab2Fragment;
import com.example.musicplayer.fragment.MainTab3Fragment;
import com.example.musicplayer.fragment.MainTab4Fragment;
import com.example.musicplayer.fragment.dialogFragment.MusicListDialogFragment;
import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.service.MusicService;
import com.example.musicplayer.service.MusicTabViewService;
import com.example.musicplayer.view.CircleImageView;
import com.example.musicplayer.view.MusicPlayingProgressView;
import com.example.musicplayer.windowTools.WindowTools;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseActivity implements MusicListDialogFragment.CallBackListener {
    private static final String TAG = "MainActivity";
    private List<View> mBottomTabs;
    private List<ImageView> mBottomTabImages;
    private List<TextView> mBottomTabText;
    private CircleImageView mCircleImageView;
    private ImageView mImageViewMusicState;
    private ImageView mImageViewMusicList;
    private TextView mTextViewMusicInfo;
    private ViewPager mViewPagerMain;
    private MusicPlayingProgressView mProgressView;
    private MusicListDialogFragment mMusicListDialogFragment;
    //记录是否是第一次create
    private boolean firstStart = true;
    //记录上一次按下back键时间，实现按两次退出
    private long mPreTime;
    //按一次退出键时弹出，提示要按两次才能退出
    private ToastUtil mToastUtil;
    public static Bitmap sScreenShot;
    private MusicService.MusicBinder mMusicBinder;
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected(): bind MusicService successfully");
            mMusicBinder = (MusicService.MusicBinder) service;
            if (mMusicBinder != null) {
                registerMusicListener(mMusicBinder);
            }
            updateMusicTab();
            if (MusicPlayerApplication.canDrawOverlays()) {
                Intent intent = new Intent(MainActivity.this, MusicTabViewService.class);
                bindService(intent, mMusicTabViewConnection, BIND_AUTO_CREATE);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected(): MusicService disconnected");
            mMusicBinder = null;
        }
    };

    private MusicTabViewService.MusicTabViewBinder mMusicTabViewBinder;
    private ServiceConnection mMusicTabViewConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected(): bind MusicTabViewService successfully");
            mMusicTabViewBinder = (MusicTabViewService.MusicTabViewBinder) service;
            mMusicTabViewBinder.setMusicBinder(mMusicBinder);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected(): MusicTabViewService disconnected");
            mMusicTabViewBinder = null;
        }
    };

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mMusicBinder != null) {
                        if (mMusicTabViewBinder != null) {
                            mMusicTabViewBinder.refreshView();
                        }
                        if (!mMusicBinder.isPlaying()) {
                            setImageViewMusicState(true, false);
                            return;
                        }
                        updateProgressView();
                    }
                    mHandler.postDelayed(mRunnable, getResources().getInteger(R.integer.refresh_time));
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate(): ");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WindowTools.lightStatusBar(getWindow());
        if (DataLoadManager.getInstance() != null) {
            DataLoadManager.getInstance().readBaseData();
        }

        firstStart = true;

        initView();
        addListener();
        startService();
        bindMusicService();
        setEnableMusicListener(true);
        //show music playing info tab
        setEnableMusicTabView(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //重置上一次点击返回键的时间
        mPreTime = 0;
        Log.d(TAG, "onResume(): activity first start -> " + firstStart);
        if (!firstStart) {
            updateMusicTab();
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (mMusicBinder != null && hasFocus) {
            registerMusicListener(mMusicBinder);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause(): ");
        firstStart = false;
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy(): ");
        super.onDestroy();
        unbindService(mMusicTabViewConnection);
        unbindService(mConnection);
        mHandler.removeCallbacks(mRunnable);
        if (mMusicBinder != null) {
            //移除音乐状态监听器
            removeMusicListener(mMusicBinder);
            mMusicBinder.pause();
            mMusicBinder = null;
        }
        stopService(new Intent(this, MusicService.class));
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        Log.d(TAG, "onConfigurationChanged(): ");
        super.onConfigurationChanged(newConfig);
        //重新加载音乐列表
        if (mMusicListDialogFragment.isResumed()) {
            mMusicListDialogFragment.updateWhenChangingMusic();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.d(TAG, "onKeyDown(): back key is pressed");
            long currentTime = System.currentTimeMillis();
            if ((currentTime - mPreTime) > 2000) {
                mToastUtil = new ToastUtil(this, "再次点击返回键切换到桌面")
                        .shortShow()
                        .setGravity(Gravity.BOTTOM, 0, 360)
                        .setAlpha(0.7f)
                        .show();
                mPreTime = currentTime;
            } else {
                onBackPressed();
                if (mToastUtil != null) {
                    mToastUtil.cancel();
                }
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    //设置应用按返回建再次进入不会出现启动页面，并且需要按两次才能退出
    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed(): ");
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    //得到组件对象并初始化viewPager
    private void initView() {
        Log.d(TAG, "initView(): ");
        mBottomTabs = new ArrayList<View>();
        mBottomTabs.add(findViewById(R.id.linear_layout_tab1));
        mBottomTabs.add(findViewById(R.id.linear_layout_tab2));
        mBottomTabs.add(findViewById(R.id.linear_layout_tab3));
        mBottomTabs.add(findViewById(R.id.linear_layout_tab4));
        mBottomTabImages = new ArrayList<ImageView>();
        mBottomTabImages.add((ImageView) findViewById(R.id.imageview_tab1));
        mBottomTabImages.add((ImageView) findViewById(R.id.imageview_tab2));
        mBottomTabImages.add((ImageView) findViewById(R.id.imageview_tab3));
        mBottomTabImages.add((ImageView) findViewById(R.id.imageview_tab4));
        mBottomTabText = new ArrayList<TextView>();
        mBottomTabText.add((TextView) findViewById(R.id.textview_tab1));
        mBottomTabText.add((TextView) findViewById(R.id.textview_tab2));
        mBottomTabText.add((TextView) findViewById(R.id.textview_tab3));
        mBottomTabText.add((TextView) findViewById(R.id.textview_tab4));
        mCircleImageView = findViewById(R.id.circle_imageview);
        mImageViewMusicState = findViewById(R.id.imageview_music_state);
        mImageViewMusicList = findViewById(R.id.imageview_music_list);
        mTextViewMusicInfo = findViewById(R.id.textview_music_info);
        mViewPagerMain = findViewById(R.id.viewpager_main);
        mProgressView = findViewById(R.id.music_playing_progress_view);
        mMusicListDialogFragment = new MusicListDialogFragment();

        List<Fragment> list = new ArrayList<Fragment>();
        list.add(new MainTab1Fragment());
        list.add(new MainTab2Fragment());
        list.add(new MainTab3Fragment());
        list.add(new MainTab4Fragment());
        mViewPagerMain.setAdapter(new BaseFragmentPagerAdapter(getSupportFragmentManager(), 1, list));
        updateBottomTab(mBottomTabs.size() > 0 ? mBottomTabs.get(0) : null);
        findViewById(R.id.play_state).setVisibility(View.GONE);
    }

    //给组件添加监听器
    private void addListener() {
        mMusicListDialogFragment.setCallBackListener(this);
        mCircleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToMusicPlayActivity();
            }
        });
        mTextViewMusicInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jumpToMusicPlayActivity();
            }
        });
        for (int i = 0; i < mBottomTabs.size(); i++) {
            final View view = mBottomTabs.get(i);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateBottomTab(view);
                }
            });
        }
        mImageViewMusicState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicBinder.changePlayingState();
            }
        });
        mImageViewMusicList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMusicBinder.getMusicCounts() > 0) {
                    Log.d(TAG, "onClick(): start to show musicListDialog");
                    mMusicListDialogFragment.show(getSupportFragmentManager(), "musicListDialog");
                }
            }
        });
        mViewPagerMain.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected(): now selected page is -> " + position);
                if (position < mBottomTabs.size())
                    updateBottomTab(mBottomTabs.get(position));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    //更新页面底部栏的选择状态
    private void updateBottomTab(View view) {
        int position = mBottomTabs.contains(view) ? mBottomTabs.indexOf(view) : 0;
        mViewPagerMain.setCurrentItem(position);
        Log.d(TAG, "updateBottomTab(): bottom tab selected changed to -> " + position);
        for (int i = 0; i < mBottomTabs.size(); i++) {
            if (i < mBottomTabImages.size()) {
                if (i == position)
                    mBottomTabImages.get(i).getDrawable().setTint(getResources().getColor(R.color.spring_green));
                else
                    mBottomTabImages.get(i).getDrawable().setTint(getResources().getColor(R.color.tab_not_selected));
            }
            if (i < mBottomTabText.size()) {
                if (i == position)
                    mBottomTabText.get(i).setTextColor(getResources().getColor(R.color.spring_green));
                else
                    mBottomTabText.get(i).setTextColor(getResources().getColor(R.color.light_gray));
            }
        }
    }

    //更新activity界面
    private void updateView() {
    }

    //更新界面下方的音乐播放栏
    private void updateMusicTab() {
        Log.d(TAG, "updateMusicTab(): music playing state is changed, music list is empty -> " + !mMusicBinder.hasMusic());
        if (mMusicBinder.hasMusic()) {
            Bitmap bitmap = mMusicBinder.getAlbumBitmap();
            String title = mMusicBinder.getTitle();
            String artist = mMusicBinder.getArtist();
            String textViewMusicInfo = title + " - " + artist;
            mCircleImageView.setSquareBitmap(bitmap);
            mTextViewMusicInfo.setText(textViewMusicInfo);
            mTextViewMusicInfo.setTextColor(getResources().getColor(R.color.black));
            mImageViewMusicList.setImageDrawable(getResources().getDrawable(R.drawable.main_activity_play_list));
        } else {
            mCircleImageView.setSquareBitmap(null);
            mTextViewMusicInfo.setText(getString(R.string.main_activity_music_info));
            mTextViewMusicInfo.setTextColor(getResources().getColor(R.color.light_gray));
            mImageViewMusicList.setImageDrawable(getResources().getDrawable(R.drawable.main_activity_no_list));
        }
        updatePlayingStateChanged();
    }

    //更新进度条
    private void updateProgressView() {
        //更新圆形播放进度条
        if (mMusicBinder != null) {
            float duration = (float) mMusicBinder.getDuration();
            if (duration > 0) {
                float sweepAngle = (float) mMusicBinder.getCurrent() / duration * 360;
                mProgressView.setSweepAngle(sweepAngle);
            }
        }
    }

    //当播放状态变化时调用
    private void updatePlayingStateChanged() {
        if (mMusicBinder.hasMusic()) {
            mHandler.removeCallbacks(mRunnable);
            mHandler.post(mRunnable);
            mProgressView.setColorEnable(true);
            setImageViewMusicState(true, mMusicBinder.isPlaying());
        } else {
            setImageViewMusicState(false, true);
            mProgressView.setColorEnable(false);
            mHandler.removeCallbacks(mRunnable);
        }
        updateProgressView();
    }

    //改变播放图标
    private void setImageViewMusicState(boolean hasMusic, boolean state) {
        if (!hasMusic)
            mImageViewMusicState.setImageDrawable(getResources().getDrawable(R.drawable.main_activity_no_playings));
        else if (state)
            mImageViewMusicState.setImageDrawable(getResources().getDrawable(R.drawable.main_activity_playings));
        else
            mImageViewMusicState.setImageDrawable(getResources().getDrawable(R.drawable.main_activity_pauses));
    }

    //绑定到音乐播放服务
    private void bindMusicService() {
        Log.d(TAG, "bindMusicService(): start to bind with MusicService");
        Intent intent = new Intent(this, MusicService.class);
        bindService(intent, mConnection, Service.BIND_AUTO_CREATE);
    }

    //启动音乐后台服务
    private void startService() {
        Log.d(TAG, "startService(): start MusicService");
        Intent intent = new Intent(this, MusicService.class);
        startService(intent);
    }

    @Override
    public MusicService.MusicBinder getBinder() {
        return mMusicBinder;
    }

    @Override
    public void onMusicListShow() {
    }

    @Override
    public void onMusicListDismiss() {
    }

    @Override
    public void onMusicPrepared(MusicInfo musicInfo) {
        updateMusicTab();
        if (mMusicListDialogFragment.isResumed()) {
            mMusicListDialogFragment.updateWhenChangingMusic();
        }
        if (mMusicTabViewBinder != null) {
            mMusicTabViewBinder.updateMusicTab();
        }
    }

    @Override
    public void onMusicEmpty(MusicInfo musicInfo) {
        updateMusicTab();
        if (mMusicTabViewBinder != null) {
            mMusicTabViewBinder.updateMusicTab();
        }
    }

    @Override
    public void onMusicPlayStateChanged(MusicInfo musicInfo, boolean isPlaying) {
        updatePlayingStateChanged();
        if (mMusicTabViewBinder != null) {
            mMusicTabViewBinder.updatePlayingState();
        }
    }

    @Override
    public void jumpToMusicPlayActivity() {
        if (mMusicBinder.getMusicCounts() > 0) {
            Log.d(TAG, "jumpToMusicPlayActivity(): start jump to activity -> MusicPlayActivity");
            mHandler.removeCallbacks(mRunnable);
            super.jumpToMusicPlayActivity();
        }
    }

    //保存音乐播放状态
    public void saveMusicPlayState() {
        if (mMusicBinder != null)
            mMusicBinder.saveMusicPlayState();
    }

    public Binder tryGetMusicBinder() {
        return mMusicBinder;
    }

    public Binder tryGetMusicTabViewBinder() {
        return mMusicTabViewBinder;
    }
}