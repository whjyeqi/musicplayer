package com.example.musicplayer.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.adapter.MusicListAdapter;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.musicTools.MusicListType;
import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.service.MusicService;
import com.example.musicplayer.settings.MusicListSettings;
import com.example.musicplayer.viewTools.ViewAlphaAnimation;
import com.example.musicplayer.windowTools.WindowTools;

import java.util.ArrayList;
import java.util.List;

public class MusicListActivity extends BaseActivity {
    private static final String TAG = "MusicListActivity";
    private MusicListType mMusicListType = MusicListType.TYPE_LOCAL;
    private CardView mBackToTopButton;
    private ListView mListViewMusicList;
    private LinearLayout mListViewContainer;
    private TextView mTextViewMusicListTitle;
    private ImageView mImageViewMusicListShare;
    private MusicListAdapter mMusicListAdapter;
    private List<MusicInfo> mMusicList;
    private ViewAlphaAnimation mViewAlphaAnimation;
    private MusicService.MusicBinder mMusicBinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_list);
        if ((mMusicBinder = getMusicServiceBinder()) == null) {
            finish();
        }
        WindowTools.statusBarLightMode(getWindow());

        initView();
        addListener();
        setToolbar();
        //show music playing info tab
        setEnableMusicTabView(true);
        initListView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy(): ");
        mMusicBinder = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBackPressed(): ");
        overridePendingTransition(R.anim.fake_anim, R.anim.music_list_activity_exit);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Log.d(TAG, "onOptionsItemSelected(): item id is android.R.id.home");
            finish();
            overridePendingTransition(R.anim.fake_anim, R.anim.music_list_activity_exit);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        Log.d(TAG, "initView(): ");
        Toolbar toolbarMusicList = findViewById(R.id.toolbar_music_list);
        setSupportActionBar(toolbarMusicList);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mListViewMusicList = findViewById(R.id.listview_music_list);
        mListViewContainer = findViewById(R.id.activity_music_list_listview_container);
        mTextViewMusicListTitle = findViewById(R.id.textview_music_list_title);
        mImageViewMusicListShare = findViewById(R.id.imageview_music_list_share);
        mBackToTopButton = findViewById(R.id.button_back_to_top);

        mViewAlphaAnimation = new ViewAlphaAnimation();
    }

    //更新列表activity的actionbar的文本
    private void updateMusicListTitle() {
        Log.d(TAG, "updateMusicListTitle(): update the music counts");
        int numOfMusic = 0;
        if (mMusicList != null)
            numOfMusic = mMusicList.size();
        if (mMusicListType == MusicListType.TYPE_FAVORITE) {
            String text = "我喜欢(" + numOfMusic + "首)";
            mTextViewMusicListTitle.setText(text);
        } else {
            String text = "本地歌曲(" + numOfMusic + "首)";
            mTextViewMusicListTitle.setText(text);
        }
        if (numOfMusic == 0)
            mListViewContainer.setVisibility(View.GONE);
        else
            mListViewContainer.setVisibility(View.VISIBLE);
    }

    private void addListener() {
        mImageViewMusicListShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.makeToast("这是分享歌曲的功能");
            }
        });
        //返回顶端按钮的监听事件
        mBackToTopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick(): back to music list top");
                mListViewMusicList.smoothScrollToPositionFromTop(0, 0, 300);
            }
        });
        mListViewMusicList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem >= MusicListSettings.getBackToTopButtonAppear() && mBackToTopButton != null) {
                    Log.d(TAG, "onScroll(): the back to top button appears");
                    mViewAlphaAnimation.appear(mBackToTopButton);
                } else if (mBackToTopButton != null) {
                    Log.d(TAG, "onScroll(): the back to top button disappears");
                    mViewAlphaAnimation.disappear(mBackToTopButton);
                }
            }
        });
    }

    //设置工具栏标题和初始化列表类型
    private void setToolbar() {
        Log.d(TAG, "setToolbar(): update the action bar info");
        Intent intent = getIntent();
        MusicListType type = (MusicListType) intent.getSerializableExtra("musicListType");
        if (type != null) {
            mMusicListType = type;
            updateMusicListTitle();
        }
    }

    //根据展示列表的类型来初始化listview
    private void initListView() {
        Log.d(TAG, "initListView(): start to init listView");
        mMusicList = new ArrayList<MusicInfo>();
        List<MusicInfo> list = MusicInfo.getLocalMusic();
        if (mMusicListType == MusicListType.TYPE_FAVORITE) {
            for (int i = 0; i < list.size(); i++)
                if (list.get(i).getFavorite() == 1)
                    mMusicList.add(list.get(i));
        } else {
            mMusicList = list;
        }
        updateMusicListTitle();

        mMusicListAdapter = new MusicListAdapter(mMusicList, this);
        mListViewMusicList.setAdapter(mMusicListAdapter);
        mListViewMusicList.setEmptyView(findViewById(R.id.music_list_empty));
        mListViewMusicList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MusicInfo music = (MusicInfo) mMusicListAdapter.getItem(position);
                if (music != null)
                    Log.d(TAG, "onItemClick(): change music and playing " + music.getTitle());
                else
                    Log.d(TAG, "onItemClick(): the changed music is null");
                mMusicBinder.start(mMusicListType, null, music);
            }
        });
    }

    //从我喜欢的列表移除
    public void cancelFavorite(MusicInfo music) {
        mMusicBinder.changeMusicFavorite(music);
        updateMusicListTitle();
        if (music != null)
            Log.d(TAG, "cancelFavorite(): change the music favorite, now is favorite -> " + (music.getFavorite() == 1));
    }

    //返回音乐列表的类型
    public MusicListType getMusicListType() {
        Log.d(TAG, "getMusicListType(): music list type is " + mMusicListType);
        return mMusicListType;
    }
}