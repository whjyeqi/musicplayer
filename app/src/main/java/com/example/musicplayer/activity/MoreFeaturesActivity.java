package com.example.musicplayer.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.musicplayer.DataLoadManager;
import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.StringUtil;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.dialogTools.NormalAlertDialog;
import com.example.musicplayer.dialogTools.TimingSetDialog;
import com.example.musicplayer.service.TimedOffService;
import com.example.musicplayer.settings.SettingsItem;
import com.example.musicplayer.view.SettingsView;

public class MoreFeaturesActivity extends BaseSettingsActivity {
    private boolean mEnableTimedOff = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEnableTimedOff = TimedOffService.serviceIsStart();
        initSettingsItem();
    }

    @Override
    protected int setActivityTitle() {
        return R.string.title_more_features_activity;
    }

    @Override
    protected View setSettingsDivideView() {
        @SuppressLint("InflateParams") View divideView = LayoutInflater.from(this)
                .inflate(R.layout.more_features_divide_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.more_features_divide_height));
        divideView.setLayoutParams(lp);
        return divideView;
    }

    private void initSettingsItem() {
        setSettingsItem(SettingsItem.MUSIC_SETTINGS);

        setSettingsDivideItem();
        setSettingsItem(SettingsItem.PERSONAL_DRESS_UP_CENTER);
        setSettingsItem(SettingsItem.OPEN_PLATFORM);
        setSettingsItem(SettingsItem.FREE_SERVICE);
        setSettingsItem(SettingsItem.TICKETING);
        setSettingsItem(SettingsItem.COLOR_RING);

        setSettingsDivideItem();
        setSettingsItem(SettingsItem.TIME_TO_EXIT);
        setSettingsItem(SettingsItem.LISTEN_TO_SONGS);
        //settings exit login
        setExitLoginItem();
    }

    private void setExitLoginItem() {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this)
                .inflate(R.layout.settings_view_exit_login, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.settings_view_height));
        view.setLayoutParams(lp);
        getSettingsContainer().addView(view);
        view.findViewById(R.id.settings_exit_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = getString(R.string.prompt);
                String message = getString(R.string.login_exit);
                final NormalAlertDialog alertDialog = new NormalAlertDialog(MoreFeaturesActivity.this, title, message);
                alertDialog.getCancel().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.getConfirm().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                        //first stop the timed off service
                        notExitTemporarily();
                        MusicPlayerApplication.getInstance().exitLogin();
                    }
                });
                alertDialog.show();
            }
        });
    }

    private void updateTimedOffView(SettingsView settingsView) {
        View expandView = settingsView.getExpandView();
        if (expandView == null) {
            expandView = inflateTimedOffExpandView();
            settingsView.setExpandView(expandView, (int) getResources().getDimension(R.dimen.timed_off_expand_view_height));
        }
        //if enable function
        if (!mDataLoadManager.readOneBaseData(DataLoadManager.TIME_TO_EXIT_ON, false)) {
            settingsView.setSwitchChecked(false);
            initTimedOffExpandView(settingsView, false);
        } else {
            settingsView.setSwitchChecked(true);
            initTimedOffExpandView(settingsView, true);
        }
    }

    private View inflateTimedOffExpandView() {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this)
                .inflate(R.layout.timed_off_expand_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.timed_off_expand_view_height));
        view.setLayoutParams(lp);
        //set click listener for radio button
        view.findViewById(R.id.radio_button_15).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimingSetClick(SettingsItem.TimedOffItem.TIMING_15);
            }
        });
        view.findViewById(R.id.radio_button_30).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimingSetClick(SettingsItem.TimedOffItem.TIMING_30);
            }
        });
        view.findViewById(R.id.radio_button_45).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimingSetClick(SettingsItem.TimedOffItem.TIMING_45);
            }
        });
        view.findViewById(R.id.radio_button_60).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimingSetClick(SettingsItem.TimedOffItem.TIMING_60);
            }
        });
        view.findViewById(R.id.radio_button_set).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTimingSetClick(SettingsItem.TimedOffItem.TIMING_SET);
            }
        });
        view.findViewById(R.id.textview_off_style).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityInSettings(new Intent(MoreFeaturesActivity.this, TimingSetActivity.class));
            }
        });
        return view;
    }

    private void initTimedOffExpandView(SettingsView settingsView, boolean showExpand) {
        View expandView = settingsView.getExpandView();
        if (showExpand && !mEnableTimedOff) {
            mDataLoadManager.writeOneBaseData(DataLoadManager.TIME_TO_EXIT_ON, true);
            updateTimedOffExpandView(expandView);
            settingsView.setEnableExpandView(true);
            //start service for timed off
            int timingSet = mDataLoadManager.readOneBaseData(DataLoadManager.TIMING_SET
                    , SettingsItem.TimedOffItem.TIMING_DEFAULT);
            int duration = SettingsItem.TimedOffItem.getTimeDuration(timingSet);
            //if duration is invalid
            if (duration == 0) {
                mDataLoadManager.writeOneBaseData(DataLoadManager.TIMING_SET, SettingsItem.TimedOffItem.TIMING_DEFAULT);
                duration = SettingsItem.TimedOffItem.getTimeDuration(SettingsItem.TimedOffItem.TIMING_DEFAULT);
            }
            //start timed off service
            startTimedOffServiceWithToast(duration);
            mEnableTimedOff = true;
        } else if (!showExpand && mEnableTimedOff) {
            notExitTemporarily();
        } else if (showExpand) {
            settingsView.setEnableExpandView(true);
            updateTimedOffExpandView(settingsView.getExpandView());
        } else {
            settingsView.setEnableExpandView(false);
        }
    }

    private void updateTimedOffExpandView(View expandView) {
        int timingSet = mDataLoadManager.readOneBaseData(DataLoadManager.TIMING_SET
                , SettingsItem.TimedOffItem.TIMING_DEFAULT);
        boolean timedOffImmediate = mDataLoadManager.readOneBaseData(DataLoadManager.TIMED_OFF_IMMEDIATE
                , true);

        int checkId = -1;
        switch (timingSet) {
            case SettingsItem.TimedOffItem.TIMING_15:
                checkId = R.id.radio_button_15;
                break;
            case SettingsItem.TimedOffItem.TIMING_30:
                checkId = R.id.radio_button_30;
                break;
            case SettingsItem.TimedOffItem.TIMING_45:
                checkId = R.id.radio_button_45;
                break;
            case SettingsItem.TimedOffItem.TIMING_60:
                checkId = R.id.radio_button_60;
                break;
            case SettingsItem.TimedOffItem.TIMING_SET:
                checkId = R.id.radio_button_set;
                break;
            default:
                break;
        }
        ((RadioGroup) expandView.findViewById(R.id.radio_group)).check(checkId);
        TextView textView = expandView.findViewById(R.id.textview_off_style);
        if (timedOffImmediate) {
            textView.setText(R.string.timed_off_style_des_1);
        } else {
            textView.setText(R.string.timed_off_style_des_2);
        }
    }

    private void onTimingSetClick(int timingSet) {
        int duration = SettingsItem.TimedOffItem.getTimeDuration(timingSet);
        if (duration > 0) {
            startTimedOffServiceWithToast(duration);
        }
        if (timingSet == SettingsItem.TimedOffItem.TIMING_SET) {
            showTimingSetDialog();
        } else {
            mDataLoadManager.writeOneBaseData(DataLoadManager.TIMING_SET, timingSet);
        }
    }

    private void showTimingSetDialog() {
        final TimingSetDialog setDialog = new TimingSetDialog(this);
        setDialog.getConfirmButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setDialog.isEmpty()) {
                    ToastUtil.makeToast(getString(R.string.timing_set_empty));
                } else {
                    int duration = setDialog.getTimingSetDuration();
                    if (duration < 0) {
                        ToastUtil.makeToast(getString(R.string.timing_set_too_long));
                    } else if (duration == 0) {
                        ToastUtil.makeToast(getString(R.string.timing_set_too_short));
                    } else {
                        //timing set is successful
                        mDataLoadManager.writeOneBaseData(DataLoadManager.TIMING_SET, SettingsItem.TimedOffItem.TIMING_SET);
                        startTimedOffServiceWithToast(duration * 60 * 1000);
                        setDialog.dismiss();
                    }
                }
            }
        });
        setDialog.show();
    }

    private void startTimedOffServiceWithToast(int duration) {
        TimedOffService.startTimedOffService(this, duration);
    }

    @Override
    protected void updateSettingsItem(SettingsView settingsView) {
        super.updateSettingsItem(settingsView);
        switch (settingsView.getSettingsItem()) {
            case SettingsItem.TIME_TO_EXIT:
                updateTimedOffView(settingsView);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSettingsItemClick(SettingsView settingsView, boolean isChecked) {
        super.onSettingsItemClick(settingsView, isChecked);
        switch (settingsView.getSettingsItem()) {
            case SettingsItem.MUSIC_SETTINGS:
                startActivityInSettings(new Intent(this, MusicSettingsActivity.class));
                break;
            case SettingsItem.PERSONAL_DRESS_UP_CENTER:
                break;
            case SettingsItem.OPEN_PLATFORM:
                break;
            case SettingsItem.FREE_SERVICE:
                break;
            case SettingsItem.TICKETING:
                break;
            case SettingsItem.COLOR_RING:
                break;
            case SettingsItem.TIME_TO_EXIT:
                mDataLoadManager.writeOneBaseData(DataLoadManager.TIME_TO_EXIT_ON, isChecked);
                initTimedOffExpandView(settingsView, isChecked);
                break;
            case SettingsItem.LISTEN_TO_SONGS:
                break;
            default:
                break;
        }
    }

    @Override
    public void notExitTemporarily() {
        super.notExitTemporarily();
        SettingsView settingsView = getSettingsView(SettingsItem.TIME_TO_EXIT);
        if (settingsView != null) {
            settingsView.setSwitchChecked(false);
            settingsView.setEnableExpandView(false);
        }
        mEnableTimedOff = false;
    }
}