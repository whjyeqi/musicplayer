package com.example.musicplayer.activity;

import android.content.Intent;
import android.os.Bundle;

import com.example.musicplayer.R;
import com.example.musicplayer.settings.SettingsItem;
import com.example.musicplayer.user.User;
import com.example.musicplayer.view.SettingsView;

public class PersonalInfoActivity extends BaseSettingsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSettingsItem();
    }

    @Override
    protected int setActivityTitle() {
        return R.string.title_personal_info_activity;
    }

    private void initSettingsItem() {
        setSettingsItem(SettingsItem.USER_PHOTO);
        setSettingsItem(SettingsItem.USER_NAME);
        setSettingsItem(SettingsItem.USER_EMAIL);
    }

    private void updateUserPhotoView(SettingsView settingsView) {

    }

    private void updateUserNameView(SettingsView settingsView) {
        settingsView.setDescription(User.getInstance().getUserName());
    }

    private void updateUserEmailView(SettingsView settingsView) {
        settingsView.setDescription(User.getInstance().getUserEmail());
    }

    @Override
    protected void updateSettingsItem(SettingsView settingsView) {
        super.updateSettingsItem(settingsView);
        switch (settingsView.getSettingsItem()) {
            case SettingsItem.USER_PHOTO:
                updateUserPhotoView(settingsView);
                break;
            case SettingsItem.USER_NAME:
                updateUserNameView(settingsView);
                break;
            case SettingsItem.USER_EMAIL:
                updateUserEmailView(settingsView);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSettingsItemClick(SettingsView settingsView, boolean isChecked) {
        super.onSettingsItemClick(settingsView, isChecked);
        switch (settingsView.getSettingsItem()) {
            case SettingsItem.USER_PHOTO:
                break;
            case SettingsItem.USER_NAME:
                startActivityInSettings(new Intent(this, EditNickNameActivity.class));
                break;
            case SettingsItem.USER_EMAIL:
                break;
            default:
                break;
        }
    }
}