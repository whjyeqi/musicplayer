package com.example.musicplayer.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.musicClass.MusicMenu;
import com.example.musicplayer.view.MusicMenuCoverDisplay;
import com.example.musicplayer.windowTools.WindowTools;

import java.util.ArrayList;
import java.util.List;

public class SelectMusicMenuCoverActivity extends BaseActivity {
    public static final int RESULT_CODE = 500;
    //加载图片的时间间隔
    private static final int LOAD_BITMAP_TIME_GAP = 30;
    private TextView mConfirm;
    private LinearLayout mContainerLeft;
    private LinearLayout mContainerRight;
    private boolean mStateChanged = false;
    //是否有图片被选中
    private boolean mIsCoverSelected = false;
    //当前加载的图片的索引
    private int mCurrentLoadIndex = 0;
    //当前封面图的索引
    private int mStartSelected;
    private MusicMenu mMusicMenu;
    private List<MusicInfo> mMusicInfo = new ArrayList<MusicInfo>();
    private List<MusicMenuCoverDisplay> mDisplay = new ArrayList<MusicMenuCoverDisplay>();
    private final Object mDisplayObject = new Object();
    private Handler mUpdateCoverHandler;
    private Runnable mUpdateCoverRunnable = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mMusicInfo != null) {
                        if (mCurrentLoadIndex < mMusicInfo.size()) {
                            MusicMenuCoverDisplay display = new MusicMenuCoverDisplay();
                            display.update(mMusicInfo.get(mCurrentLoadIndex));
                            addListener(display);
                            synchronized (mDisplayObject) {
                                mDisplay.add(display);
                            }
                            if (mCurrentLoadIndex % 2 == 0)
                                mContainerLeft.addView(display.getView());
                            else
                                mContainerRight.addView(display.getView());
                            if (mCurrentLoadIndex == mStartSelected && !mIsCoverSelected) {
                                display.setSelected(true);
                            }
                            mCurrentLoadIndex++;
                            mUpdateCoverHandler.postDelayed(mUpdateCoverRunnable, LOAD_BITMAP_TIME_GAP);
                        }
                    }
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_music_menu_cover);
        WindowTools.lightStatusBar(getWindow());

        Intent intent = getIntent();
        String menuName = intent.getStringExtra("musicMenuName");
        mMusicMenu = MusicInfo.getMusicMenuByName(menuName);
        initView();
        addListener();
        updateList();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            if (mUpdateCoverHandler == null) {
                mUpdateCoverHandler = new Handler();
            }
            if (mCurrentLoadIndex < mMusicInfo.size()) {
                mUpdateCoverHandler.postDelayed(mUpdateCoverRunnable, 0);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mUpdateCoverHandler.removeCallbacks(mUpdateCoverRunnable);
    }

    @Override
    public void onBackPressed() {
        finishActivity();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finishActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        Toolbar toolbar = findViewById(R.id.toolbar_select_music_menu_cover);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            //添加向上操作
            actionBar.setDisplayHomeAsUpEnabled(true);
            //去除toolbar中的标题
            actionBar.setDisplayShowTitleEnabled(false);
        }

        mConfirm = findViewById(R.id.textview_select_music_menu_cover_confirm);
        mContainerLeft = findViewById(R.id.linear_layout_container_left);
        mContainerRight = findViewById(R.id.linear_layout_container_right);
    }

    private void addListener() {
        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doClickSave();
            }
        });
    }

    private void addListener(final MusicMenuCoverDisplay display) {
        display.getImageViewCover().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!display.getSelected()) {
                    mDisplay.get(getSelectedNumber()).setSelected(false);
                    display.setSelected(true);
                    mIsCoverSelected = true;
                }
            }
        });
    }

    //初始化显示的图片列表
    private void updateList() {
        if (mMusicMenu != null) {
            mMusicInfo.clear();
            mMusicInfo.add(null);
            for (int i = 0; i < mMusicMenu.getCounts(); i++) {
                MusicInfo temp = mMusicMenu.getMusicInfo(i);
                boolean hasThisAlbum = false;
                for (int j = 1; j < mMusicInfo.size(); j++)
                    if (temp.getAlbum().equals(mMusicInfo.get(j).getAlbum())) {
                        hasThisAlbum = true;
                        break;
                    }
                if (!hasThisAlbum)
                    mMusicInfo.add(temp);
            }
            //计算初始选中的索引号
            mStartSelected = 0;
            if (mMusicMenu != null) {
                MusicInfo currentInfo = MusicInfo.getMusicInfoById(mMusicMenu.getBitmapId());
                if (currentInfo != null) {
                    for (int i = 1; i < mMusicInfo.size(); i++)
                        if (currentInfo.getAlbum().equals(mMusicInfo.get(i).getAlbum())) {
                            mStartSelected = i;
                            break;
                        }
                }
            }
        }
    }

    //获取选中的序号
    private int getSelectedNumber() {
        synchronized (mDisplayObject) {
            for (int i = 0; i < mDisplay.size(); i++)
                if (mDisplay.get(i).getSelected())
                    return i;
            mDisplay.get(0).setSelected(true);
            return 0;
        }
    }

    //点击保存
    private void doClickSave() {
        if (!mIsCoverSelected) {
            mStateChanged = false;
        } else {
            int nowSelected = getSelectedNumber();
            if (nowSelected != mStartSelected && mMusicMenu != null) {
                ToastUtil.makeToast(getString(R.string.success_to_change_menu_cover));
                mStateChanged = true;
                if (nowSelected == 0)
                    mMusicMenu.setBitmap(0);
                else
                    mMusicMenu.setBitmap(mDisplay.get(nowSelected).getMusicInfo().getId());
            }
        }
        finishActivity();
    }

    //返回
    private void finishActivity() {
        Intent intent = new Intent();
        intent.putExtra("result", mStateChanged);
        this.setResult(RESULT_CODE, intent);
        finish();
        overridePendingTransition(R.anim.fake_anim, R.anim.activity_vertical_exit);
    }
}