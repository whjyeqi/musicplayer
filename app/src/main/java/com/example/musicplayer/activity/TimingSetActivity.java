package com.example.musicplayer.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.musicplayer.DataLoadManager;
import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.dialogTools.TimingSetDialog;
import com.example.musicplayer.service.TimedOffService;
import com.example.musicplayer.settings.SettingsItem;
import com.example.musicplayer.view.SettingsView;
import com.example.musicplayer.view.TimingSetItemView;

import java.util.ArrayList;
import java.util.List;

public class TimingSetActivity extends BaseSettingsActivity {
    private String mSet15;
    private String mSet30;
    private String mSet45;
    private String mSet60;
    private String mSetSelf;
    private TextView mLabel;
    private TimingSetItemView mSelectedView;
    private List<TimingSetItemView> mList = new ArrayList<TimingSetItemView>();

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if (TimedOffService.serviceIsStart()) {
                if (mSelectedView != null) {
                    mSelectedView.setTimeInfo((int) (TimedOffService.getOffTime() - System.currentTimeMillis()));
                }
                mHandler.postDelayed(mRunnable, TimedOffService.getTimeInterval());
            } else {
                if (mSelectedView != null) {
                    mSelectedView.setSelected(false);
                    mSelectedView.setTimeInfo(-1);
                }
            }
        }
    };

    private TimingSetItemView.ItemClickListener mItemClickListener = new TimingSetItemView.ItemClickListener() {
        @Override
        public void onItemClick(String title) {
            int timedOffItem;
            if (title.equals(mSet15)) {
                timedOffItem = SettingsItem.TimedOffItem.TIMING_15;
            } else if (title.equals(mSet30)) {
                timedOffItem = SettingsItem.TimedOffItem.TIMING_30;
            } else if (title.equals(mSet45)) {
                timedOffItem = SettingsItem.TimedOffItem.TIMING_45;
            } else if (title.equals(mSet60)) {
                timedOffItem = SettingsItem.TimedOffItem.TIMING_60;
            } else if (title.equals(mSetSelf)) {
                timedOffItem = SettingsItem.TimedOffItem.TIMING_SET;
            } else {
                timedOffItem = SettingsItem.TimedOffItem.TIMING_DEFAULT;
            }
            int duration = SettingsItem.TimedOffItem.getTimeDuration(timedOffItem);
            if (duration > 0) {
                startTimedOffServiceWithToast(duration);
            }
            if (timedOffItem == SettingsItem.TimedOffItem.TIMING_SET) {
                showTimingSetDialog();
            } else {
                mDataLoadManager.writeOneBaseData(DataLoadManager.TIMING_SET, timedOffItem);
                updateTimingSetItemView(title);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSet15 = getString(R.string.activity_timing_set_15);
        mSet30 = getString(R.string.activity_timing_set_30);
        mSet45 = getString(R.string.activity_timing_set_45);
        mSet60 = getString(R.string.activity_timing_set_60);
        mSetSelf = getString(R.string.activity_timing_set_self);
        initSettingsItem();
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateTimingSetItemView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mHandler.post(mRunnable);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
    }

    @Override
    protected int setActivityTitle() {
        return R.string.time_to_exit;
    }

    private void initSettingsItem() {
        createItemView(mSet15);
        setSettingsDivideItem();
        createItemView(mSet30);
        setSettingsDivideItem();
        createItemView(mSet45);
        setSettingsDivideItem();
        createItemView(mSet60);
        setSettingsDivideItem();
        createItemView(mSetSelf);
        setSettingsDivideItem();

        setEmptyView();
        setSettingsItem(SettingsItem.TIMED_OFF_IMMEDIATE);
        setSettingsDivideItem();

        setLabelView();
    }

    private void createItemView(String title) {
        TimingSetItemView view = new TimingSetItemView(title);
        mList.add(view);
        getSettingsContainer().addView(view.getView());
        view.setItemClickListener(mItemClickListener);
    }

    private void setEmptyView() {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.empty_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.timing_set_activity_empty_view_height));
        view.setLayoutParams(lp);
        getSettingsContainer().addView(view);
    }

    private void setLabelView() {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.one_text_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.timing_set_activity_label_view_height));
        view.setLayoutParams(lp);
        getSettingsContainer().addView(view);
        mLabel = view.findViewById(R.id.textview_label);
    }

    private void updateTimedOffImmediate(SettingsView settingsView) {
        if (mDataLoadManager.readOneBaseData(DataLoadManager.TIMED_OFF_IMMEDIATE, true)) {
            settingsView.setSwitchChecked(false);
            updateLabel(false);
        } else {
            settingsView.setSwitchChecked(true);
            updateLabel(true);
        }
    }

    private void updateLabel(boolean isChecked) {
        if (isChecked) {
            mLabel.setText(R.string.activity_timing_set_checked);
        } else {
            mLabel.setText(R.string.activity_timing_set_not_checked);
        }
    }

    private void updateTimingSetItemView() {
        int timingSet = mDataLoadManager.readOneBaseData(DataLoadManager.TIMING_SET
                , SettingsItem.TimedOffItem.TIMING_DEFAULT);
        String selectedTitle = "";
        switch (timingSet) {
            case SettingsItem.TimedOffItem.TIMING_15:
                selectedTitle = mSet15;
                break;
            case SettingsItem.TimedOffItem.TIMING_30:
                selectedTitle = mSet30;
                break;
            case SettingsItem.TimedOffItem.TIMING_45:
                selectedTitle = mSet45;
                break;
            case SettingsItem.TimedOffItem.TIMING_60:
                selectedTitle = mSet60;
                break;
            case SettingsItem.TimedOffItem.TIMING_SET:
                selectedTitle = mSetSelf;
                break;
            default:
                break;
        }
        updateTimingSetItemView(selectedTitle);
    }

    private void updateTimingSetItemView(String selectedTitle) {
        //get timing set item and init
        for (int i = 0; i < mList.size(); i++) {
            TimingSetItemView view = mList.get(i);
            if (selectedTitle.equals(view.getTitle())) {
                view.setSelected(true);
                mSelectedView = view;
            } else {
                view.setSelected(false);
                view.setTimeInfo(-1);
            }
        }
        //click and start to show the time info
        mHandler.post(mRunnable);
    }

    private void showTimingSetDialog() {
        final TimingSetDialog setDialog = new TimingSetDialog(this);
        setDialog.getConfirmButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (setDialog.isEmpty()) {
                    ToastUtil.makeToast(getString(R.string.timing_set_empty));
                } else {
                    int duration = setDialog.getTimingSetDuration();
                    if (duration < 0) {
                        ToastUtil.makeToast(getString(R.string.timing_set_too_long));
                    } else if (duration == 0) {
                        ToastUtil.makeToast(getString(R.string.timing_set_too_short));
                    } else {
                        //timing set is successful
                        mDataLoadManager.writeOneBaseData(DataLoadManager.TIMING_SET, SettingsItem.TimedOffItem.TIMING_SET);
                        updateTimingSetItemView(mSetSelf);
                        startTimedOffServiceWithToast(duration * 60 * 1000);
                        setDialog.dismiss();
                    }
                }
            }
        });
        setDialog.show();
    }

    private void startTimedOffServiceWithToast(int duration) {
        TimedOffService.startTimedOffService(this, duration);
    }

    @Override
    protected void updateSettingsItem(SettingsView settingsView) {
        super.updateSettingsItem(settingsView);
        if (settingsView.getSettingsItem() == SettingsItem.TIMED_OFF_IMMEDIATE) {
            updateTimedOffImmediate(settingsView);
        }
    }

    @Override
    protected void onSettingsItemClick(SettingsView settingsView, boolean isChecked) {
        super.onSettingsItemClick(settingsView, isChecked);
        if (settingsView.getSettingsItem() == SettingsItem.TIMED_OFF_IMMEDIATE) {
            mDataLoadManager.writeOneBaseData(DataLoadManager.TIMED_OFF_IMMEDIATE, !isChecked);
            updateLabel(isChecked);
        }
    }

    @Override
    public void notExitTemporarily() {
        super.notExitTemporarily();
        mHandler.removeCallbacks(mRunnable);
        updateTimingSetItemView();
        //init selected view and display nothing
        if (mSelectedView != null) {
            mSelectedView.setSelected(false);
            mSelectedView.setTimeInfo(-1);
        }
    }
}