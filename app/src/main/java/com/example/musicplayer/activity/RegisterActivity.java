package com.example.musicplayer.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.StringUtil;
import com.example.musicplayer.dialogTools.NormalAlertDialog;
import com.example.musicplayer.musicTools.UserTableManager;
import com.example.musicplayer.provider.DBConstants;
import com.example.musicplayer.user.UserManage;
import com.example.musicplayer.windowTools.WindowTools;

public class RegisterActivity extends AppCompatActivity {
    public static final int RESULT_CODE = 600;
    private static final int NAME_LENGTH_LIMIT = 20;
    private EditText mNameEdit;
    private EditText mPasswordEdit;
    private EditText mEmailEdit;
    private TextView mUserNameLimit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        WindowTools.lightStatusBar(getWindow());

        initView();
        addListener();
        mUserNameLimit.setText(String.valueOf(NAME_LENGTH_LIMIT));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.normal_right_enter, R.anim.normal_right_exit);
    }

    private void initView() {
        mNameEdit = findViewById(R.id.register_input_user_name);
        mPasswordEdit = findViewById(R.id.register_input_password);
        mEmailEdit = findViewById(R.id.register_input_email);
        mUserNameLimit = findViewById(R.id.register_user_name_limit);
    }

    private void addListener() {
        findViewById(R.id.register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryRegister();
            }
        });
        mNameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int length = NAME_LENGTH_LIMIT - s.length();
                mUserNameLimit.setText(String.valueOf(length));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void tryRegister() {
        String name;
        String password;
        String email;
        if ((name = checkUserName()) != null && (password = checkPassword()) != null && (email = checkEmail()) != null) {
            int userId = UserManage.registerUser(name, password, email);
            if (userId > 0) {
                if (!UserTableManager.createTableOnRegister(userId)) {
                    UserManage.deleteUser(userId);
                }
            }
            finishActivity(userId, name);
        }
    }

    private String checkUserName() {
        String name = mNameEdit.getText().toString();
        if (name.equals("")) {
            showRegisterFailDialog(getString(R.string.register_name_empty));
            return null;
        }
        if (UserManage.existUserName(name)) {
            showRegisterFailDialog(getString(R.string.register_same_name));
            return null;
        }
        return name;
    }

    private String checkPassword() {
        String password = mPasswordEdit.getText().toString();
        if (password.equals("")) {
            showRegisterFailDialog(getString(R.string.register_password_empty));
            return null;
        }
        return StringUtil.ByteToHexString(password.getBytes());
    }

    private String checkEmail() {
        String email = mEmailEdit.getText().toString();
        if (email.equals("")) {
            showRegisterFailDialog(getString(R.string.register_email_empty));
            return null;
        }
        return email;
    }

    private void showRegisterFailDialog(String message) {
        String title = getString(R.string.prompt);
        NormalAlertDialog alertDialog = new NormalAlertDialog(this, title, message);
        final Dialog dialog = alertDialog.getDialog();
        alertDialog.getCancel().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        alertDialog.getConfirm().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void finishActivity(int id, String name) {
        Intent intent = new Intent();
        intent.putExtra(DBConstants.User.ID, id);
        intent.putExtra(DBConstants.User.NAME, name);
        setResult(RESULT_CODE, intent);
        finish();
        overridePendingTransition(R.anim.normal_right_enter, R.anim.normal_right_exit);
    }
}