package com.example.musicplayer.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.musicplayer.R;
import com.example.musicplayer.settings.SettingsItem;
import com.example.musicplayer.view.SettingsView;

public class MusicSettingsActivity extends BaseSettingsActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initSettingsItem();
    }

    @Override
    protected int setActivityTitle() {
        return R.string.title_music_settings_activity;
    }

    @Override
    protected View setSettingsDivideView(String groupLabel) {
        @SuppressLint("InflateParams") View divideView = LayoutInflater.from(this)
                .inflate(R.layout.music_settings_divide_view, null);
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.music_settings_divide_height));
        divideView.setLayoutParams(lp);
        ((TextView) divideView.findViewById(R.id.settings_view_group_label)).setText(groupLabel);
        return divideView;
    }

    private void initSettingsItem() {
        //network
        setSettingsDivideItem(getString(R.string.settings_label_network));
        setSettingsItem(SettingsItem.ONLY_WIFI);
        setSettingsItem(SettingsItem.FLOW_REMIND);
        //general settings
        setSettingsDivideItem(getString(R.string.settings_label_general_settings));
        setSettingsItem(SettingsItem.FLUENCY_SETTINGS);
        setSettingsItem(SettingsItem.NOTIFICATION);
        setSettingsItem(SettingsItem.PERSONAL_INFO);
    }

    @Override
    protected void updateSettingsItem(SettingsView settingsView) {
        super.updateSettingsItem(settingsView);
    }

    @Override
    protected void onSettingsItemClick(SettingsView settingsView, boolean isChecked) {
        super.onSettingsItemClick(settingsView, isChecked);
        switch (settingsView.getSettingsItem()) {
            case SettingsItem.ONLY_WIFI:
                break;
            case SettingsItem.FLOW_REMIND:
                break;
            case SettingsItem.FLUENCY_SETTINGS:
                break;
            case SettingsItem.NOTIFICATION:
                break;
            case SettingsItem.PERSONAL_INFO:
                startActivityInSettings(new Intent(this, PersonalInfoActivity.class));
                break;
            default:
                break;
        }
    }
}