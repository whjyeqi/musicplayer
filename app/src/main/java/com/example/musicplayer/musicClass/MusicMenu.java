package com.example.musicplayer.musicClass;

import android.graphics.Bitmap;

import com.example.musicplayer.commonUtils.DrawUtil;
import com.example.musicplayer.musicTools.MusicSortType;

import java.util.ArrayList;
import java.util.List;

public class MusicMenu {
    //歌单名，不允许重复
    private String mName;
    //在所有歌单中的顺序
    private int mOrder;
    //歌单点播次数
    private int mListenCounts;
    //歌单的封面图
    private int mBitmap;
    private MusicSortType mMusicSortType;
    //歌单音乐列表，存放的是歌曲的id
    private List<Integer> mMusicList;
    //存放对应歌曲的序号
    private List<Integer> mMusicOrder;

    public MusicMenu(String name, int order) {
        this(name, order, 0, 0);
    }

    public MusicMenu(String name, int order, int listenCounts, int bitmap) {
        mName = name;
        mOrder = order;
        mListenCounts = listenCounts;
        mBitmap = bitmap;
        mMusicSortType = MusicSortType.SORT_DEFAULT;
        mMusicList = new ArrayList<Integer>();
        mMusicOrder = new ArrayList<Integer>();
    }

    public synchronized String getName() {
        return mName;
    }

    public synchronized int getOrder() {
        return mOrder;
    }

    public synchronized int getListenCounts() {
        return mListenCounts;
    }

    public synchronized Bitmap getBitmap() {
        MusicInfo musicInfo = MusicInfo.getMusicInfoById(mBitmap);
        if (musicInfo == null)
            return DrawUtil.getDefaultMusicMenuBitmap();
        return musicInfo.getBitmap();
    }

    public synchronized int getBitmapId() {
        return mBitmap;
    }

    //返回歌单中歌曲数量
    public int getCounts() {
        return mMusicList.size();
    }

    //返回指定索引的音乐信息
    public MusicInfo getMusicInfo(int index) {
        if (index >= 0 && index < mMusicList.size())
            return MusicInfo.getMusicInfoById(mMusicList.get(index));
        return null;
    }

    //得到对歌单的描述信息
    public String getIntroduction() {
        return MusicInfo.getMusicMenuIntroduction(mName);
    }

    //得到歌曲排序类型
    public MusicSortType getMusicSortType() {
        return mMusicSortType;
    }

    public void updateMusicSortType() {
        mMusicSortType = MusicInfo.getMusicSortType(mName);
    }

    //得到指定id对应歌曲的序号
    public int getMusicOrderById(int id) {
        for (int i = 0; i < mMusicList.size(); i++)
            if (mMusicList.get(i) == id)
                return mMusicOrder.get(i);
        return -1;
    }

    public List<Integer> getMusicList() {
        return new ArrayList<Integer>(mMusicList);
    }

    public List<Integer> getMusicOrder() {
        return new ArrayList<Integer>(mMusicOrder);
    }

    //修改歌单名
    public synchronized void setName(String name) {
        if (name != null) {
            if (MusicInfo.hasMusicMenu(name))
                return;
            if (MusicInfo.modifyMusicMenuName(mName, name))
                mName = name;
        }
    }

    //设置顺序编号
    public synchronized void setOrder(int order) {
        if (order >= 0)
            mOrder = order;
    }

    public synchronized void setListenCounts() {
        mListenCounts++;
        MusicInfo.modifyMusicMenuListenCounts(mName, mListenCounts);
    }

    public synchronized void setBitmap(int musicId) {
        mBitmap = musicId;
        MusicInfo.modifyMusicMenuBitmap(mName, mBitmap);
    }

    public void setMusicList(List<Integer> musicList) {
        mMusicList = musicList;
    }

    public void setMusicOrder(List<Integer> musicOrder) {
        mMusicOrder = musicOrder;
    }

    //修改简介
    public void setIntroduction(String introduction) {
        if (introduction != null)
            MusicInfo.modifyMusicMenuIntroduction(mName, introduction);
    }

    public void setMusicSortType(MusicSortType musicSortType) {
        if (musicSortType != null) {
            mMusicSortType = musicSortType;
            MusicInfo.modifyMusicSortType(mName, musicSortType);
        }
    }

    //判断歌单是否包含某首音乐
    public boolean has(int musicId) {
        if (mMusicList != null) {
            for (int i = 0; i < mMusicList.size(); i++)
                if (mMusicList.get(i) == musicId)
                    return true;
        }
        return false;
    }

    //向歌单中新增音乐
    public void insertMusic(List<Integer> musicId, List<Integer> musicOrder) {
        if (musicId != null && musicId.size() > 0) {
            //设置歌单的封面图
            if (mMusicList.size() == 0) {
                mBitmap = musicId.get(0);
                MusicInfo.modifyMusicMenuBitmap(mName, mBitmap);
            }
            mMusicList.addAll(musicId);
            mMusicOrder.addAll(musicOrder);
        }
    }

    //修改部分歌曲的顺序
    public void changeMusicOrder(List<Integer> musicId, List<Integer> musicOrder) {
        if (musicId != null && musicOrder != null && musicId.size() == musicOrder.size()) {
            for (int i = 0; i < musicId.size(); i++) {
                int targetIndex = -1;
                for (int j = 0; j < mMusicList.size(); j++)
                    if (mMusicList.get(j).intValue() == musicId.get(i).intValue()) {
                        targetIndex = j;
                        break;
                    }
                if (targetIndex >= 0)
                    mMusicOrder.set(targetIndex, musicOrder.get(i));
            }
            syncMusicOrder();
        }
    }

    //同步歌曲顺序信息到数据库
    public void syncMusicOrder() {
        MusicInfo.changeMusicMenuMusicOrder(this);
    }

    //从歌单删除一些音乐
    public void deleteMusic(List<Integer> musicId) {
        if (musicId != null && musicId.size() > 0) {
            boolean deleteBitmap = false;
            for (int i = 0; i < musicId.size(); i++) {
                if (musicId.get(i) == mBitmap)
                    deleteBitmap = true;
                //获取当前删除歌曲的序号
                int musicOrder = getMusicOrderById(musicId.get(i));
                for (int j = 0; j < mMusicOrder.size(); j++) {
                    //序号相等，删除；序号大于给定序号，则序号减1
                    if (mMusicOrder.get(j) == musicOrder) {
                        mMusicOrder.remove(j);
                        j--;
                    } else if (mMusicOrder.get(j) > musicOrder) {
                        mMusicOrder.set(j, mMusicOrder.get(j) - 1);
                    }
                }
                mMusicList.remove((Integer) musicId.get(i));
            }
            if (deleteBitmap) {
                changeBitmap();
                MusicInfo.modifyMusicMenuBitmap(mName, mBitmap);
            }
        }
    }

    //更换封面图
    private void changeBitmap() {
        if (mMusicList.size() == 0)
            mBitmap = 0;
        else {
            MusicInfo musicInfo = MusicInfo.getMusicInfoById(mBitmap);
            mBitmap = mMusicList.get(0);
            if (musicInfo != null) {
                for (int i = 0; i < mMusicList.size(); i++) {
                    MusicInfo temp = getMusicInfo(i);
                    if (temp != null && musicInfo.getAlbum().equals(temp.getAlbum())) {
                        mBitmap = mMusicList.get(i);
                        break;
                    }
                }
            }
        }
    }

    public static MusicSortType getMusicSortType(String type) {
        if (type == null)
            return MusicSortType.SORT_DEFAULT;
        try {
            return MusicSortType.valueOf(type);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return MusicSortType.SORT_DEFAULT;
        }
    }
}