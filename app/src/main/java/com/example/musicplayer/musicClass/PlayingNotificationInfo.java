package com.example.musicplayer.musicClass;

import android.graphics.Bitmap;

public class PlayingNotificationInfo {
    private String mContentTitle;
    private String mContentText;
    private int mSmallIcon;
    private Bitmap mLargeIcon;
    private int mDrawableFavorite;
    private int mDrawableLast;
    private int mDrawablePlay;
    private int mDrawableNext;

    public PlayingNotificationInfo(String contentTitle, String contentText, Bitmap largeIcon, int smallIcon
            , int drawableFavorite, int drawableLast, int drawablePlay, int drawableNext) {
        mContentTitle=contentTitle;
        mContentText=contentText;
        mLargeIcon=largeIcon;
        mSmallIcon=smallIcon;
        mDrawableFavorite=drawableFavorite;
        mDrawableLast=drawableLast;
        mDrawablePlay=drawablePlay;
        mDrawableNext=drawableNext;
    }

    public String getContentTitle() {
        return mContentTitle;
    }

    public String getContentText() {
        return mContentText;
    }

    public Bitmap getLargeIcon() {
        return mLargeIcon;
    }

    public int getSmallIcon() {
        return mSmallIcon;
    }

    public int getDrawableFavorite() {
        return mDrawableFavorite;
    }

    public int getDrawableLast() {
        return mDrawableLast;
    }

    public int getDrawablePlay() {
        return mDrawablePlay;
    }

    public int getDrawableNext() {
        return mDrawableNext;
    }
}