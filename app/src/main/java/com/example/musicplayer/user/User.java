package com.example.musicplayer.user;

public class User {
    private static User sInstance;
    private int mUserId;
    private String mUserName;
    private int mUserPhoto;
    private int mUserVip;
    private int mUserGrade;
    private int mUserAuthority;
    private String mUserEmail;

    private User(int id, String name, int photo, int vip, int grade) {
        mUserId = id;
        mUserName = name;
        mUserPhoto = photo;
        mUserVip = vip;
        mUserGrade = grade;
        if (mUserId == 1) {
            mUserAuthority = 1;
        } else {
            mUserAuthority = 0;
        }
        mUserEmail = null;
    }

    public static void initUser(int id, String name, int photo, int vip, int grade, int authority) {
        if (sInstance == null || sInstance.getUserId() != id) {
            sInstance = new User(id, name, photo, vip, grade);
        }
    }

    public static User getInstance() {
        UserManage.setUserLogin(sInstance != null);
        return sInstance;
    }

    public static void exitLoginState() {
        UserManage.setUserLogin(false);
        sInstance = null;
    }

    public int getUserId() {
        return mUserId;
    }

    public String getUserName() {
        return mUserName;
    }

    public String getUserEmail() {
        if (mUserEmail == null) {
            mUserEmail = UserManage.getUserEmail();
        }
        return mUserEmail;
    }

    public boolean setUserName(String newName) {
        if (mUserName.equals(newName)) {
            return true;
        }
        if (UserManage.updateUserName(mUserId, newName)) {
            mUserName = newName;
            return true;
        }
        return false;
    }

    public boolean hasAuthority() {
        return mUserAuthority == 1;
    }
}