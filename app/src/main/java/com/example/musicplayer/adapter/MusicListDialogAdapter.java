package com.example.musicplayer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.R;
import com.example.musicplayer.fragment.dialogFragment.MusicListDialogFragment;

import java.util.List;

public class MusicListDialogAdapter extends BaseAdapter {
    //存储音乐列表的数据
    private List<MusicInfo> mMusicList;
    //布局文件解析器
    private LayoutInflater mInflater;
    private Context mContext;
    //记录正在播放的歌曲在列表里的位置
    private int mNowMusicPosition;
    //列表字体的默认颜色
    private int mDefaultColor;
    //得到对话框fragment的对象
    private MusicListDialogFragment mMusicListDialogFragment;

    public MusicListDialogAdapter(List<MusicInfo> list, Context context, MusicListDialogFragment dialogFragment) {
        this(list, context, 0, dialogFragment);
    }

    public MusicListDialogAdapter(List<MusicInfo> list, Context context, int nowPosition, MusicListDialogFragment dialogFragment) {
        mMusicList = list;
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mNowMusicPosition = nowPosition;
        mMusicListDialogFragment = dialogFragment;
        mDefaultColor = mContext.getResources().getColor(R.color.black);
    }

    //设置正在播放的音乐的位置
    public void setNowMusicPosition(int position) {
        mNowMusicPosition = position;
    }

    //获取当前音乐的索引
    public int getNowMusicPosition() {
        return mNowMusicPosition;
    }

    public void setDefaultColor(int defaultColor) {
        mDefaultColor = defaultColor;
    }

    public int getDefaultColor() {
        return mDefaultColor;
    }

    //返回数据列表大小
    @Override
    public int getCount() {
        return mMusicList.size();
    }

    //得到数据列表某一项的对象
    @Override
    public Object getItem(int position) {
        return mMusicList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.music_list_dialog_item, null);
            viewHolder.textViewMusicInfo = convertView.findViewById(R.id.textview_music_list_dialog_item_info);
            viewHolder.linearLayoutDelete = convertView.findViewById(R.id.linear_layout_music_list_dialog_item);
            viewHolder.imageViewPlaying = convertView.findViewById(R.id.imageview_music_list_dialog_item_playing);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.textViewMusicInfo.setText(mMusicList.get(position).getTitle() + " - " + mMusicList.get(position).getArtist());
        if (position == mNowMusicPosition) {
            viewHolder.textViewMusicInfo.setTextColor(mContext.getResources().getColor(R.color.spring_green));
            viewHolder.textViewMusicInfo.setTextSize(18);
            viewHolder.imageViewPlaying.setVisibility(View.VISIBLE);
        } else {
            viewHolder.textViewMusicInfo.setTextColor(mDefaultColor);
            viewHolder.textViewMusicInfo.setTextSize(15);
            viewHolder.imageViewPlaying.setVisibility(View.INVISIBLE);
        }

        viewHolder.linearLayoutDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicListDialogFragment.deleteOneMusic(position);
            }
        });
        return convertView;
    }

    //定义一个类来加载控件，来保存控件的实例化
    static class ViewHolder {
        TextView textViewMusicInfo;
        ImageView imageViewPlaying;
        LinearLayout linearLayoutDelete;
    }
}