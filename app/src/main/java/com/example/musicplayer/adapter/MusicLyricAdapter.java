package com.example.musicplayer.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.musicplayer.R;

import java.util.List;

public class MusicLyricAdapter extends BaseAdapter {
    private List<String> mLyrics;
    private LayoutInflater mInflater;
    private Context mContext;
    //记录正在播放的歌词在列表的索引
    private int mNowPosition;

    public MusicLyricAdapter(List<String> list, Context context, int nowPosition) {
        mLyrics = list;
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mNowPosition = nowPosition;
    }

    //设置正在播放的歌词的索引
    public void setNowPosition(int position) {
        mNowPosition = position;
    }

    //得到正播放的歌词的索引
    public int getNowPosition() {
        return mNowPosition;
    }

    @Override
    public int getCount() {
        return mLyrics.size();
    }

    @Override
    public Object getItem(int position) {
        return mLyrics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new MusicLyricAdapter.ViewHolder();
            convertView = mInflater.inflate(R.layout.music_play_tab3_lyric_item, null);
            viewHolder.mTextViewLyric = convertView.findViewById(R.id.textview_music_play_tab3_item_lyric);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MusicLyricAdapter.ViewHolder) convertView.getTag();
        }
        if (position == mNowPosition) {
            viewHolder.mTextViewLyric.setTextSize(18);
            viewHolder.mTextViewLyric.setTextColor(mContext.getResources().getColor(R.color.white));
            viewHolder.mTextViewLyric.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        } else {
            viewHolder.mTextViewLyric.setTextSize(15);
            viewHolder.mTextViewLyric.setTextColor(mContext.getResources().getColor(R.color.silver));
            viewHolder.mTextViewLyric.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        }

        viewHolder.mTextViewLyric.setText(mLyrics.get(position));
        return convertView;
    }

    //定义一个类来加载控件，来保存控件的实例化
    class ViewHolder {
        TextView mTextViewLyric;
    }
}