package com.example.musicplayer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.musicTools.MusicListType;
import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.activity.MusicListActivity;
import com.example.musicplayer.R;

import java.util.List;

public class MusicListAdapter extends BaseAdapter {
    //存储音乐列表的数据
    private List<MusicInfo> mMusicList;
    //布局文件解析器
    private LayoutInflater mInflater;
    private Context mContext;
    private MusicListActivity mActivity;

    public MusicListAdapter(List<MusicInfo> list, MusicListActivity parentActivity) {
        mMusicList = list;
        mActivity = parentActivity;
        mContext = (Context) mActivity;
        mInflater = LayoutInflater.from(mContext);
    }

    //返回数据列表大小
    @Override
    public int getCount() {
        return mMusicList.size();
    }

    //得到数据列表某一项的对象
    @Override
    public Object getItem(int position) {
        return mMusicList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.music_list_item, null);
            viewHolder.textViewTitle = convertView.findViewById(R.id.music_list_item_title);
            viewHolder.textViewInfo = convertView.findViewById(R.id.music_list_item_info);
            viewHolder.linearLayoutMv = convertView.findViewById(R.id.music_list_item_mv);
            viewHolder.linearLayoutSecondFeature = convertView.findViewById(R.id.music_list_item_second_feature);
            viewHolder.imageViewSecondFeature = convertView.findViewById(R.id.imageview_music_list_item_second_feature);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String title = mMusicList.get(position).getTitle();
        String info = mMusicList.get(position).getArtist() + " - " + mMusicList.get(position).getAlbum();
        viewHolder.textViewTitle.setText(title);
        viewHolder.textViewInfo.setText(info);
        if (mActivity.getMusicListType() == MusicListType.TYPE_FAVORITE) {
            viewHolder.imageViewSecondFeature.setImageDrawable(mContext.getResources().getDrawable(R.drawable.music_list_item_close));
            viewHolder.linearLayoutSecondFeature.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MusicInfo toDelete = mMusicList.get(position);
                    mMusicList.remove(position);
                    mActivity.cancelFavorite(toDelete);
                    MusicListAdapter.this.notifyDataSetChanged();
                }
            });
        } else {
            viewHolder.imageViewSecondFeature.setImageDrawable(mContext.getResources().getDrawable(R.drawable.music_list_item_more));
            viewHolder.linearLayoutSecondFeature.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToastUtil.makeToast("本地歌曲列表，展开更多功能");
                }
            });
        }
        viewHolder.linearLayoutMv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.makeToast("播放歌曲MV功能");
            }
        });
        return convertView;
    }

    //定义一个类来加载控件，来保存控件的实例化
    static class ViewHolder {
        TextView textViewTitle;
        TextView textViewInfo;
        LinearLayout linearLayoutMv;
        LinearLayout linearLayoutSecondFeature;
        ImageView imageViewSecondFeature;
    }
}