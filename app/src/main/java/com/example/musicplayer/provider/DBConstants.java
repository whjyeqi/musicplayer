package com.example.musicplayer.provider;

public class DBConstants {
    public static class MusicDetail {
        public static final String TABLE_NAME = "music_detail";
        public static final int URI_MATCH_CODE = 1;
        public static final String ID = "_id";
        public static final String ALBUM_ID = "albumId";
        public static final String SIZE = "size";
        public static final String TITLE = "title";
        public static final String ALBUM = "album";
        public static final String ARTIST = "artist";
        public static final String PATH = "path";
        public static final String LYRIC_PATH = "lyricPath";
        public static final String DATA_1 = "data_1";
        public static final String DATA_2 = "data_2";
    }

    public static class User {
        public static final String TABLE_NAME = "user";
        public static final int URI_MATCH_CODE = 2;
        public static final String ID = "_id";
        public static final String NAME = "name";
        public static final String PASSWORD = "password";
        public static final String EMAIL = "email";
        public static final String PHOTO = "photo";
        public static final String VIP = "vip";
        public static final String DATE = "date";
        public static final String GRADE = "grade";
        public static final String AUTHORITY = "authority";
        public static final String DATA_1 = "data_1";
        public static final String DATA_2 = "data_2";
    }

    public static class MusicDetailUser {
        public static final String TABLE_NAME = "music_detail_";
        public static final int URI_MATCH_CODE = 3;
        public static final String ID = "_id";
        public static final String FAVORITE = "favorite";
        public static final String LISTEN_COUNTS = "listenCounts";
        public static final String AUTHORITY = "authority";
        public static final String DATA_1 = "data_1";
        public static final String DATA_2 = "data_2";
    }

    public static class MusicMenuManage {
        public static final String TABLE_NAME = "music_menu_manage_";
        public static final int URI_MATCH_CODE = 4;
        public static final String NAME = "name";
        public static final String MENU_ORDER = "menuOrder";
        public static final String MENU_TABLE_NAME = "menuTableName";
        public static final String MATCH_CODE = "matchCode";
        public static final String INTRODUCTION = "introduction";
        public static final String BITMAP = "bitmap";
        public static final String LISTEN_COUNTS = "listenCounts";
        public static final String MUSIC_SORT_TYPE = "musicSortType";
        public static final String DATA_1 = "data_1";
        public static final String DATA_2 = "data_2";
    }

    public static class MusicMenuDetail {
        public static final String TABLE_NAME = "music_menu_detail_";
        public static final String ID = "_id";
        public static final String MUSIC_ORDER = "musicOrder";
        public static final String DATA_1 = "data_1";
        public static final String DATA_2 = "data_2";
    }
}