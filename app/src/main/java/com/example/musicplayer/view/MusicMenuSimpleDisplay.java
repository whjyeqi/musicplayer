package com.example.musicplayer.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.DisplayUtil;
import com.example.musicplayer.musicClass.MusicMenu;

//音乐歌单列表项
public class MusicMenuSimpleDisplay extends SimpleDisplayView {
    private MusicMenu mMusicMenu;
    private ImageView mImageViewBitmap;
    private ImageView mImageViewSelected;
    private ImageView mImageViewAdjust;
    private TextView mTextViewName;
    private TextView mTextViewCounts;

    public MusicMenuSimpleDisplay(int listType) {
        super(listType);
        Context context = MusicPlayerApplication.getInstance();
        mViewHeight = (int) MusicPlayerApplication.getInstance().getResources()
                .getDimension(R.dimen.activity_music_menu_list_height);
        mView = (RelativeLayout) LayoutInflater.from(context).inflate(
                context.getResources().getLayout(R.layout.music_menu_simple_display), null);
        mImageViewBitmap = mView.findViewById(R.id.imageview_music_menu_list_image);
        ImageView imageViewArrow = mView.findViewById(R.id.imageview_music_menu_list_arrow);
        mImageViewSelected = mView.findViewById(R.id.imageview_music_menu_list_selected);
        mImageViewAdjust = mView.findViewById(R.id.imageview_music_menu_list_adjust);
        mTextViewName = mView.findViewById(R.id.textview_music_menu_list_name);
        mTextViewCounts = mView.findViewById(R.id.textview_music_menu_list_counts);
        if (mType == TYPE_DISPLAY) {
            mImageViewSelected.setVisibility(View.GONE);
            mImageViewAdjust.setVisibility(View.GONE);
        } else if (mType == TYPE_SELECT) {
            mImageViewSelected.setVisibility(View.VISIBLE);
            mImageViewAdjust.setVisibility(View.VISIBLE);
            imageViewArrow.setVisibility(View.GONE);
        } else {
            mImageViewSelected.setVisibility(View.VISIBLE);
            imageViewArrow.setVisibility(View.GONE);
        }
        setImageRoundRect();
        setContainerLayout();
    }

    @Override
    public void update(Object object) {
        mMusicMenu = (MusicMenu) object;
        if (mView != null) {
            mTextViewName.setText(mMusicMenu.getName());
            String textCounts = mMusicMenu.getCounts() + "首";
            mTextViewCounts.setText(textCounts);
            Bitmap bitmap = mMusicMenu.getBitmap();
            if (bitmap != null)
                mImageViewBitmap.setImageBitmap(bitmap);
            else
                mImageViewBitmap.setImageResource(R.mipmap.music_menu_default);
        }
    }

    @Override
    public void setSelected(boolean isSelected) {
        if (mType == TYPE_SELECT || mType == TYPE_CONFIRM) {
            if (isSelected && !mIsSelected)
                mImageViewSelected.setImageResource(R.drawable.icon_round_selected);
            if (!isSelected && mIsSelected)
                mImageViewSelected.setImageResource(R.drawable.icon_round_not_selected);
            mIsSelected = isSelected;
        }
    }

    //返回与视图绑定的歌单对象
    public MusicMenu getMusicMenu() {
        return mMusicMenu;
    }

    //返回列表项的专辑图片对象
    public ImageView getImageViewBitmap() {
        return mImageViewBitmap;
    }

    //返回调整位置的图片的对象
    public ImageView getImageViewAdjust() {
        return mImageViewAdjust;
    }

    //设置图片圆角
    private void setImageRoundRect() {
        final int radius = (int) MusicPlayerApplication.getInstance().getResources().getDimension(R.dimen.small_image_radius);
        final int width = (int) MusicPlayerApplication.getInstance().getResources().getDimension(R.dimen.activity_music_menu_list_image);
        final int height = (int) MusicPlayerApplication.getInstance().getResources().getDimension(R.dimen.activity_music_menu_list_image);
        if (mImageViewBitmap != null)
            DisplayUtil.setRoundRectOutline(mImageViewBitmap, 0, 0, width, height, radius);
    }
}