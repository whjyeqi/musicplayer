package com.example.musicplayer.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class NewScrollView extends ScrollView {
    //设置能否滑动
    private boolean mCanScroll = true;
    private OnScrollListener mOnScrollListener;

    public NewScrollView(Context context) {
        super(context);
    }

    public NewScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NewScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //设置scrollview能否滑动
    public void setCanScroll(boolean canScroll) {
        mCanScroll = canScroll;
    }

    //设置滚动监听
    public void setOnScrollListener(OnScrollListener onScrollListener) {
        mOnScrollListener = onScrollListener;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!mCanScroll)
            return false;
        return super.onInterceptTouchEvent(ev);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mCanScroll)
            return super.onTouchEvent(ev);
        else
            return false;
    }

    @Override
    protected void onScrollChanged(int x, int y, int oldX, int oldY) {
        super.onScrollChanged(x, y, oldX, oldY);
        if (mOnScrollListener != null)
            mOnScrollListener.onScrollChanged(y, oldY);
    }

    public interface OnScrollListener {
        public void onScrollChanged(int y, int oldY);
    }
}