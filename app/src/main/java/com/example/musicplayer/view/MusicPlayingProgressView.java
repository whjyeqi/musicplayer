package com.example.musicplayer.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.example.musicplayer.R;

//显示音乐播放进度的圆环进度视图
public class MusicPlayingProgressView extends View {
    //画笔
    private Paint mPaint;
    //外部圆弧线宽度
    private int mOuterStrokeSize;
    //内部弧线的宽度
    private int mInnerStrokeSize;
    //开始的角度
    private float mStartAngle;
    //结束的角度
    private float mSweepAngle;
    //正常下颜色
    private int mEnableColor;
    //没有歌曲时，画笔颜色
    private int mDisableColor;
    //外部圆显示的区域
    private RectF mOuterRectF;
    //内部圆显示区域
    private RectF mInnerRectF;

    public MusicPlayingProgressView(Context context) {
        this(context, null);
    }

    public MusicPlayingProgressView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MusicPlayingProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    //初始化操作
    private void init() {
        mPaint = new Paint();
        mOuterStrokeSize = (int) getResources().getDimension(R.dimen.music_playing_progress_outer_stroke_width);
        mInnerStrokeSize = (int) getResources().getDimension(R.dimen.music_playing_progress_inner_stroke_width);
        mStartAngle = -90;
        mSweepAngle = 0;
        mEnableColor = getResources().getColor(R.color.spring_green);
        mDisableColor = getResources().getColor(R.color.music_playing_progress_disable);
        int size = (int) getResources().getDimension(R.dimen.music_playing_progress_view);
        int OuterGap = (mOuterStrokeSize + 1) / 2;
        int InnerGap = (mInnerStrokeSize + 1) / 2;
        mOuterRectF = new RectF(OuterGap, OuterGap, size - OuterGap, size - OuterGap);
        mInnerRectF = new RectF(InnerGap, InnerGap, size - InnerGap, size - InnerGap);

        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(mDisableColor);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setStrokeWidth(mOuterStrokeSize);
        canvas.drawArc(mOuterRectF, mStartAngle, 360, false, mPaint);
        //绘制内部表示歌曲进度的弧度
        mPaint.setStrokeWidth(mInnerStrokeSize);
        canvas.drawArc(mInnerRectF, mStartAngle, mSweepAngle, false, mPaint);
    }

    //改变圆弧的宽度
    public void setStrokeWidth(int strokeWidth) {
        if (strokeWidth > 0) {
            mInnerStrokeSize = strokeWidth;
            int size = (int) getResources().getDimension(R.dimen.music_playing_progress_view);
            int InnerGap = (mInnerStrokeSize + 1) / 2;
            mInnerRectF = new RectF(InnerGap, InnerGap, size - InnerGap, size - InnerGap);
            invalidate();
        }
    }

    //设置画笔颜色
    public void setColorEnable(boolean flag) {
        if (flag) {
            mPaint.setColor(mEnableColor);
            invalidate();
        } else {
            mPaint.setColor(mDisableColor);
            setSweepAngle(0);
        }
    }

    //设置内部圆弧扫过的弧度
    public void setSweepAngle(float sweepAngle) {
        mSweepAngle = sweepAngle;
        invalidate();
    }
}