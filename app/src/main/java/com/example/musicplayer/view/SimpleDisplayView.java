package com.example.musicplayer.view;

import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.musicplayer.R;

public abstract class SimpleDisplayView {
    //类型：展示歌单基本信息
    public static final int TYPE_DISPLAY = 1;
    //类型：可操作歌单的功能
    public static final int TYPE_SELECT = 2;
    //类型：选择对象并确定
    public static final int TYPE_CONFIRM = 3;
    //歌单列表项的类型
    protected int mType;
    //当类型为TYPE_SELECT时，保存选中状态
    protected boolean mIsSelected;
    protected RelativeLayout mView;
    protected int mViewHeight;

    public SimpleDisplayView(int listType) {
        switch (listType) {
            case TYPE_DISPLAY:
            case TYPE_SELECT:
            case TYPE_CONFIRM:
                mType = listType;
                break;
            default:
                mType = TYPE_DISPLAY;
                break;
        }
        mIsSelected = false;
    }

    //设置歌单项容器的布局
    protected void setContainerLayout() {
        if (mView != null) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mViewHeight);
            mView.setLayoutParams(lp);
            if (mType == TYPE_DISPLAY)
                mView.setBackgroundResource(R.drawable.ripple_music_menu_item_display);
            else
                mView.setBackgroundResource(R.drawable.ripple_music_menu_item_select);
        }
    }

    //设置背景色透明度
    public void setBackgroundAlpha(int alpha) {
        alpha = alpha < 0 ? 0 : Math.min(alpha, 255);
        mView.getBackground().setAlpha(alpha);
    }

    //返回歌单列表项视图
    public RelativeLayout getView() {
        return mView;
    }

    //返回选中状态
    public boolean getSelected() {
        return mIsSelected;
    }

    //视图的高度
    public int getViewHeight() {
        return mViewHeight;
    }

    //更新视图
    public abstract void update(Object object);

    //设置选中状态
    public abstract void setSelected(boolean isSelected);
}