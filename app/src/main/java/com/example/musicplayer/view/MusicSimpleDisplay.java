package com.example.musicplayer.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.musicClass.MusicInfo;

//展示音乐基本信息列表项
public class MusicSimpleDisplay extends SimpleDisplayView {
    private MusicInfo mMusicInfo;
    private TextView mTextViewTitle;
    private TextView mTextViewText;
    private TextView mTextViewNumber;
    private ImageView mImageViewSelected;
    private ImageView mImageViewAdjust;
    private ImageView mImageViewVideo;
    private ImageView mImageViewMore;
    private View mListenCountsInfo;
    private View mItemIndicator;
    private TextView mTextViewListenCounts;
    private int mNumber;
    //是否标记为正在播放
    private boolean mIsMarkPlaying;

    public MusicSimpleDisplay(int listType, int number) {
        super(listType);
        Context context = MusicPlayerApplication.getInstance();
        mViewHeight = (int) MusicPlayerApplication.getInstance().getResources()
                .getDimension(R.dimen.activity_music_menu_details_list_height);
        mView = (RelativeLayout) LayoutInflater.from(context).inflate(
                context.getResources().getLayout(R.layout.music_simple_display), null);
        mTextViewNumber = mView.findViewById(R.id.textview_music_simple_display_number);
        mTextViewTitle = mView.findViewById(R.id.textview_music_simple_display_title);
        mTextViewText = mView.findViewById(R.id.textview_music_simple_display_text);
        mImageViewSelected = mView.findViewById(R.id.imageview_music_simple_display_selected);
        mImageViewAdjust = mView.findViewById(R.id.imageview_music_menu_details_adjust);
        mImageViewVideo = mView.findViewById(R.id.imageview_music_menu_details_video);
        mImageViewMore = mView.findViewById(R.id.imageview_music_menu_details_more);
        mListenCountsInfo = mView.findViewById(R.id.linear_layout_listen_counts);
        mItemIndicator = mView.findViewById(R.id.view_music_item_indicator);
        mTextViewListenCounts = mView.findViewById(R.id.textview_music_listen_counts);
        mNumber = number;
        mIsMarkPlaying = false;
        if (mType == TYPE_DISPLAY) {
            mImageViewSelected.setVisibility(View.GONE);
            mImageViewAdjust.setVisibility(View.GONE);
        } else {
            mTextViewNumber.setVisibility(View.GONE);
            mImageViewVideo.setVisibility(View.GONE);
            mImageViewMore.setVisibility(View.GONE);
            if (mType == TYPE_CONFIRM)
                mImageViewAdjust.setVisibility(View.GONE);
        }
        enableListenCountsInfo(false);
        setContainerLayout();
    }

    @Override
    public void update(Object object) {
        if (object == null)
            return;
        mMusicInfo = (MusicInfo) object;
        if (mView != null) {
            mTextViewTitle.setText(mMusicInfo.getTitle());
            String text = mMusicInfo.getArtist() + " - " + mMusicInfo.getAlbum();
            mTextViewText.setText(text);
            if (mType == TYPE_DISPLAY)
                mTextViewNumber.setText(String.valueOf(mNumber));
        }
    }

    @Override
    public void setSelected(boolean isSelected) {
        if (mType == TYPE_SELECT || mType == TYPE_CONFIRM) {
            if (isSelected && !mIsSelected)
                mImageViewSelected.setImageResource(R.drawable.icon_round_selected_small);
            if (!isSelected && mIsSelected)
                mImageViewSelected.setImageResource(R.drawable.icon_round_not_selected_small);
            mIsSelected = isSelected;
        }
    }

    //是否显示收听次数的信息
    public void enableListenCountsInfo(boolean isVisible) {
        if (mType == TYPE_DISPLAY) {
            if (isVisible) {
                mListenCountsInfo.setVisibility(View.VISIBLE);
                mImageViewVideo.setVisibility(View.GONE);
                int counts = mMusicInfo == null ? 0 : mMusicInfo.getListenCounts();
                mTextViewListenCounts.setText(String.valueOf(counts));
            } else {
                mListenCountsInfo.setVisibility(View.GONE);
                mImageViewVideo.setVisibility(View.VISIBLE);
            }
        }
    }

    //标记此歌曲项是否播放
    public void markAsPlaying(boolean isPlaying) {
        if (isPlaying != mIsMarkPlaying) {
            mItemIndicator.setVisibility(isPlaying ? View.VISIBLE : View.GONE);
            int colorFirst = MusicPlayerApplication.getInstance().getResources().getColor(
                    R.color.activity_music_menu_details_first_text);
            int colorSecond = MusicPlayerApplication.getInstance().getResources().getColor(
                    R.color.activity_music_menu_details_second_text);
            if (isPlaying)
                colorFirst = colorSecond = MusicPlayerApplication.getInstance().getResources().getColor(R.color.spring_green);
            mTextViewNumber.setTextColor(colorFirst);
            mTextViewTitle.setTextColor(colorFirst);
            mTextViewText.setTextColor(colorSecond);
        }
        mIsMarkPlaying = isPlaying;
    }

    public boolean isMarkAsPlaying() {
        return mIsMarkPlaying;
    }

    public int getNumber() {
        return mNumber;
    }

    public MusicInfo getMusicInfo() {
        return mMusicInfo;
    }

    public ImageView getImageViewAdjust() {
        return mImageViewAdjust;
    }

    public ImageView getImageViewVideo() {
        return mImageViewVideo;
    }

    public ImageView getImageViewMore() {
        return mImageViewMore;
    }
}