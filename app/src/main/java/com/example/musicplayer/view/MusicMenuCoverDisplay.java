package com.example.musicplayer.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.DisplayUtil;
import com.example.musicplayer.commonUtils.DrawUtil;
import com.example.musicplayer.musicClass.MusicInfo;

public class MusicMenuCoverDisplay {
    private boolean mIsSelected;
    private RelativeLayout mView;
    private int mViewHeight;
    private MusicInfo mMusicInfo;
    private ImageView mImageViewCover;
    private ImageView mImageViewSelected;

    public MusicMenuCoverDisplay() {
        Context context = MusicPlayerApplication.getInstance();
        mViewHeight = (int) context.getResources().getDimension(R.dimen.select_music_menu_cover_view_height);
        mIsSelected = false;
        mView = (RelativeLayout) LayoutInflater.from(context).
                inflate(context.getResources().getLayout(R.layout.music_menu_cover_display), null);
        mImageViewCover = mView.findViewById(R.id.imageview_cover);
        mImageViewSelected = mView.findViewById(R.id.imageview_select_cover_selected);
        setContainerLayout();
        setImageRoundRect();
    }

    public void update(MusicInfo musicInfo) {
        mMusicInfo = musicInfo;
        if (mMusicInfo == null)
            mImageViewCover.setImageBitmap(DrawUtil.getDefaultMusicMenuBitmap());
        else
            mImageViewCover.setImageBitmap(mMusicInfo.getBitmap());
    }

    public View getView() {
        return mView;
    }

    public ImageView getImageViewCover() {
        return mImageViewCover;
    }

    //设置是否选中
    public void setSelected(boolean isSelected) {
        if (isSelected && !mIsSelected)
            mImageViewSelected.setImageResource(R.drawable.icon_round_selected);
        if (!isSelected && mIsSelected)
            mImageViewSelected.setImageResource(R.drawable.icon_round_not_selected);
        mIsSelected = isSelected;
    }

    public boolean getSelected() {
        return mIsSelected;
    }

    public MusicInfo getMusicInfo() {
        return mMusicInfo;
    }

    //设置容器的布局
    protected void setContainerLayout() {
        if (mView != null) {
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mViewHeight);
            mView.setLayoutParams(lp);
        }
    }

    //设置图片圆角
    private void setImageRoundRect() {
        final int radius = (int) MusicPlayerApplication.getInstance().getResources().getDimension(R.dimen.big_image_radius);
        final int width = (int) MusicPlayerApplication.getInstance().getResources().getDimension(R.dimen.activity_music_menu_details_image_size);
        final int height = (int) MusicPlayerApplication.getInstance().getResources().getDimension(R.dimen.activity_music_menu_details_image_size);
        if (mImageViewCover != null)
            DisplayUtil.setRoundRectOutline(mImageViewCover, 0, 0, width, height, radius);
    }
}