package com.example.musicplayer.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;

public class SettingsView {
    private static final int CLICK_EVENT_INTERVAL = 500;
    private boolean mResponseClickEvent = true;
    private int mSettingsItem;
    private View mSettingsView;
    private View mExpandView;
    private View mView;
    private int mViewHeight;
    private int mExpandViewHeight;
    private int mExpandViewDelta = 0;
    private boolean mShowExpandView = false;
    private TextView mViewTitle;
    private TextView mViewSummary;
    private TextView mViewDescription;
    private View mDescriptionView;
    private SwitchCompat mSwitchView;
    private SettingsClickListener mListener;

    private Handler mMoveHandler = new Handler();
    private Runnable mMoveRunnable = new Runnable() {
        @Override
        public void run() {
            int currentHeight = mSettingsView.getLayoutParams().height;
            if (mShowExpandView) {
                currentHeight += mExpandViewDelta;
                currentHeight = Math.min(currentHeight, mViewHeight + mExpandViewHeight);
            } else {
                currentHeight -= mExpandViewDelta;
                currentHeight = Math.max(currentHeight, mViewHeight);
            }
            updateViewLayout(currentHeight);
            if (currentHeight > mViewHeight && currentHeight < mViewHeight + mExpandViewHeight) {
                mMoveHandler.postDelayed(mMoveRunnable, MusicPlayerApplication.getRefreshTime());
            }
            if (currentHeight == mViewHeight) {
                mExpandView.setVisibility(View.GONE);
            }
        }
    };

    public SettingsView(boolean showSwitch, int settingsItem) {
        this("", showSwitch, settingsItem);
    }

    public SettingsView(String title, boolean showSwitch, int settingsItem) {
        this(title, null, showSwitch, settingsItem);
    }

    public SettingsView(String title, String summary, boolean showSwitch, int settingsItem) {
        this(title, summary, null, showSwitch, settingsItem);
    }

    @SuppressLint("InflateParams")
    public SettingsView(String title, String summary, String description, boolean showSwitch, int settingsItem) {
        Context context = MusicPlayerApplication.getInstance().getTopActivity();
        mViewHeight = (int) context.getResources().getDimension(R.dimen.settings_view_height);
        mSettingsItem = settingsItem;
        mSettingsView = LayoutInflater.from(context).inflate(R.layout.settings_view, null);
        mExpandView = null;
        mView = mSettingsView.findViewById(R.id.settings_view);
        mViewTitle = mView.findViewById(R.id.textview_settings_title);
        mViewSummary = mView.findViewById(R.id.textview_settings_summary);
        mViewDescription = mView.findViewById(R.id.textview_settings_state);
        mDescriptionView = mView.findViewById(R.id.description_view);
        mSwitchView = mView.findViewById(R.id.switch_view);
        //init view
        setTitle(title);
        setSummary(summary);
        setShowSwitch(showSwitch);
        setDescription(description);
        setViewLayout();
        setListener();
    }

    private void setViewLayout() {
        if (mSettingsView != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mViewHeight);
            mSettingsView.setLayoutParams(lp);
        }
    }

    private void updateViewLayout(int height) {
        if (mSettingsView != null) {
            ViewGroup.LayoutParams lp = mSettingsView.getLayoutParams();
            lp.height = height;
            mSettingsView.setLayoutParams(lp);
        }
    }

    private void setListener() {
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mResponseClickEvent) {
                    boolean isChecked = false;
                    if (mSwitchView.getVisibility() == View.VISIBLE) {
                        isChecked = !mSwitchView.isChecked();
                        mSwitchView.setChecked(isChecked);
                    }
                    if (mListener != null) {
                        mListener.doClick(SettingsView.this, isChecked);
                    }
                    mResponseClickEvent = false;
                    //wait for
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mResponseClickEvent = true;
                        }
                    }, CLICK_EVENT_INTERVAL);
                }
            }
        });
    }

    public void setTitle(String title) {
        if (mViewTitle != null && title != null) {
            mViewTitle.setText(title);
        }
    }

    public void setSummary(String summary) {
        if (mViewSummary != null) {
            if (summary == null) {
                mViewSummary.setVisibility(View.GONE);
            } else {
                mViewSummary.setVisibility(View.VISIBLE);
                mViewSummary.setText(summary);
            }
        }
    }

    public void setShowSwitch(boolean showSwitch) {
        if (showSwitch) {
            mDescriptionView.setVisibility(View.GONE);
            mSwitchView.setVisibility(View.VISIBLE);
        } else {
            mDescriptionView.setVisibility(View.VISIBLE);
            mSwitchView.setVisibility(View.GONE);
        }
    }

    public void setDescription(String description) {
        if (mSwitchView.getVisibility() == View.GONE && description != null) {
            mDescriptionView.setVisibility(View.VISIBLE);
            mViewDescription.setText(description);
        }
    }

    public void setSwitchChecked(boolean checked) {
        if (mSwitchView.getVisibility() == View.VISIBLE) {
            mSwitchView.setChecked(checked);
        }
    }

    public void setEnableExpandView(boolean showExpand) {
        if (mExpandView != null) {
            if (showExpand && mExpandView.getVisibility() == View.GONE) {
                mExpandView.setVisibility(View.VISIBLE);
                mShowExpandView = true;
                mMoveHandler.post(mMoveRunnable);
            } else if (!showExpand && mExpandView.getVisibility() == View.VISIBLE) {
                mShowExpandView = false;
                mMoveHandler.post(mMoveRunnable);
            } else if (showExpand) {
                mExpandView.setVisibility(View.VISIBLE);
                updateViewLayout(mViewHeight + mExpandViewHeight);
            } else {
                mExpandView.setVisibility(View.GONE);
                updateViewLayout(mViewHeight);
            }
        }
    }

    public void setExpandView(View view, int height) {
        mExpandView = view;
        if (mExpandView != null) {
            ((LinearLayout) mSettingsView.findViewById(R.id.settings_view_expand)).addView(mExpandView);
            mExpandViewHeight = height;
            //get the move delta
            int counts = MusicPlayerApplication.getSettingsExpandViewAnimTime() / MusicPlayerApplication.getRefreshTime();
            mExpandViewDelta = mExpandViewHeight / counts;
            if (mExpandViewDelta == 0) {
                mExpandViewDelta = 1;
            }
        }
    }

    public void setClickListener(SettingsClickListener listener) {
        mListener = listener;
    }

    public View getSettingsView() {
        return mSettingsView;
    }

    public View getExpandView() {
        return mExpandView;
    }

    public int getSettingsItem() {
        return mSettingsItem;
    }

    public interface SettingsClickListener {
        void doClick(SettingsView settingsView, boolean isChecked);
    }
}