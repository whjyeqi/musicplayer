package com.example.musicplayer.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.StringUtil;

public class TimingSetItemView {
    public static final int CLICK_EVENT_INTERVAL = 500;
    private View mView;
    private View mContentView;
    private int mViewHeight;
    private TextView mTitle;
    private TextView mTimeInfo;
    private ImageView mImageSelected;
    private boolean mIsSelected = false;
    private ItemClickListener mListener;

    @SuppressLint("InflateParams")
    public TimingSetItemView(String title) {
        Context context = MusicPlayerApplication.getInstance().getTopActivity();
        mViewHeight = (int) context.getResources().getDimension(R.dimen.settings_view_height);
        mView = LayoutInflater.from(context).inflate(R.layout.timing_set_item_view, null);
        mContentView = mView.findViewById(R.id.content_view);
        mTitle = mView.findViewById(R.id.textview_title);
        mTimeInfo = mView.findViewById(R.id.textview_time);
        mImageSelected = mView.findViewById(R.id.image_selected);
        mTitle.setText(title);
        setTimeInfo(-1);
        setSelected(false);
        setListener();
    }

    public void setTimeInfo(int time) {
        if (time < 0) {
            mTimeInfo.setVisibility(View.GONE);
        } else {
            mTimeInfo.setVisibility(View.VISIBLE);
            mTimeInfo.setText(StringUtil.getFormatTime(time));
        }
    }

    public void setSelected(boolean selected) {
        mIsSelected = selected;
        mImageSelected.setVisibility(mIsSelected ? View.VISIBLE : View.GONE);
    }

    private void setListener() {
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onItemClick((String) mTitle.getText());
                }
            }
        });
    }

    public View getView() {
        return mView;
    }

    public String getTitle() {
        return (String) mTitle.getText();
    }

    public void setItemClickListener(ItemClickListener listener) {
        mListener = listener;
    }

    public interface ItemClickListener {
        void onItemClick(String title);
    }
}