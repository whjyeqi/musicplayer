package com.example.musicplayer;

import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.service.MusicService;

//音乐状态发生改变的一些处理
public interface MusicStateChangedListener {
    //音乐播放结束
    public void musicCompletion(MusicInfo musicInfo);

    //音乐准备完成
    public void musicPrepared(MusicInfo musicInfo);

    //音乐列表清空
    public void musicEmpty(MusicInfo musicInfo);

    //音乐喜爱值改变
    public void musicFavoriteChanged(MusicInfo musicInfo, boolean result);

    //播放状态改变
    public void musicPlayStateChanged(MusicInfo musicInfo, boolean isPlaying);

    //音乐循环模式
    public void musicPlayModeChanged(MusicInfo musicInfo, MusicService.PlayMode mode);

    //播放下一首
    public void musicPlayNext(MusicInfo beforeMusic, MusicInfo afterMusic);

    //播放上一首
    public void musicPlayLast(MusicInfo beforeMusic, MusicInfo afterMusic);
}