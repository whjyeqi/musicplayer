package com.example.musicplayer.musicTools;

//音乐列表类型
public enum MusicListType {
    /**
     * 类型：标记为我喜欢的歌曲
     */
    TYPE_FAVORITE,
    /**
     * 类型：本地的歌曲
     */
    TYPE_LOCAL,
    /**
     * 类型：歌单的歌曲列表
     */
    TYPE_MENU,
    /**
     * 类型：已购买的歌曲
     */
    TYPE_PURCHASED;
}