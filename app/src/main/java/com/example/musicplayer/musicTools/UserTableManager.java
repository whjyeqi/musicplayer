package com.example.musicplayer.musicTools;

import com.example.musicplayer.provider.DBConstants;
import com.example.musicplayer.provider.MusicContentProvider;
import com.example.musicplayer.user.User;

import java.util.ArrayList;
import java.util.List;

//管理自建歌单的相关数据表
public class UserTableManager {
    private static final String TABLE_NAME = "music_";
    private static final int startMatchCode = 1000;
    private static String sMusicDetailUserName;
    private static String sMusicMenuManageName;
    private static List<String> sMusicMenuName = new ArrayList<String>();
    private static List<String> sTableName = new ArrayList<String>();
    private static List<Integer> sMatchCode = new ArrayList<Integer>();

    public static boolean createTableOnRegister(int userId) {
        sMusicDetailUserName = DBConstants.MusicDetailUser.TABLE_NAME + userId;
        sMusicMenuManageName = DBConstants.MusicMenuManage.TABLE_NAME + userId;
        int codeDetailUser = DBConstants.MusicDetailUser.URI_MATCH_CODE;
        int codeMenuManage = DBConstants.MusicMenuManage.URI_MATCH_CODE;
        return MusicContentProvider.createMusicDetailUser(sMusicDetailUserName, codeDetailUser)
                && MusicContentProvider.createMusicMenuManage(sMusicMenuManageName, codeMenuManage);
    }

    public static void initTableManager() {
        User user = User.getInstance();
        sMusicDetailUserName = DBConstants.MusicDetailUser.TABLE_NAME + user.getUserId();
        sMusicMenuManageName = DBConstants.MusicMenuManage.TABLE_NAME + user.getUserId();
        updateMusicContentProvider();
    }

    //添加已有的歌单数据
    public static boolean addMusicMenuDetailData(String musicMenuName, String tableName, int matchCode) {
        if (musicMenuName == null) {
            return false;
        }
        for (int i = 0; i < sMusicMenuName.size(); i++) {
            if (sMusicMenuName.get(i).equals(musicMenuName)) {
                return false;
            }
        }
        sMusicMenuName.add(musicMenuName);
        sTableName.add(tableName);
        sMatchCode.add(matchCode);
        return true;
    }

    public static void removeTableForExitLogin() {
        sMusicDetailUserName = null;
        sMusicMenuManageName = null;
        sMusicMenuName = new ArrayList<String>();
        sTableName = new ArrayList<String>();
        sMatchCode = new ArrayList<Integer>();
        updateMusicContentProvider();
    }

    public static void updateMusicContentProvider() {
        MusicContentProvider.resetMatchCode(true);
    }

    //新增数据库表
    public static boolean createMusicMenuTable(String musicMenuName) {
        int newCode = createMatchCode();
        String newTableName = DBConstants.MusicMenuDetail.TABLE_NAME + User.getInstance().getUserId() + "_" + newCode;
        if (MusicContentProvider.createMusicMenuDetail(newTableName, newCode)) {
            sMusicMenuName.add(musicMenuName);
            sTableName.add(newTableName);
            sMatchCode.add(newCode);
            return true;
        }
        return false;
    }

    //删除数据库表
    public static boolean deleteMusicMenuTable(String musicMenuName) {
        int index = -1;
        for (int i = 0; i < sMusicMenuName.size(); i++) {
            if (sMusicMenuName.get(i).equals(musicMenuName)) {
                index = i;
                break;
            }
        }
        if (index >= 0 && MusicContentProvider.deleteTable(sTableName.get(index))) {
            sMusicMenuName.remove(index);
            sTableName.remove(index);
            sMatchCode.remove(index);
            return true;
        }
        return false;
    }

    //更新歌单名
    public static boolean updateMusicMenuName(String oldName, String newName) {
        if (oldName != null && newName != null) {
            for (int i = 0; i < sMusicMenuName.size(); i++)
                if (sMusicMenuName.get(i).equals(oldName)) {
                    sMusicMenuName.set(i, newName);
                    return true;
                }
            return false;
        }
        return false;
    }

    public static List<String> getMusicMenuName() {
        return new ArrayList<>(sMusicMenuName);
    }

    public static List<String> getTableName() {
        return new ArrayList<>(sTableName);
    }

    public static List<Integer> getMatchCode() {
        return new ArrayList<>(sMatchCode);
    }

    public static String getMusicDetailUserName() {
        return sMusicDetailUserName;
    }

    public static String getMusicMenuManageName() {
        return sMusicMenuManageName;
    }

    //根据匹配码，返回数据表名
    public static String getTableNameByMatchCode(int matchCode) {
        if (matchCode == DBConstants.MusicDetailUser.URI_MATCH_CODE) {
            return sMusicDetailUserName;
        }
        if (matchCode == DBConstants.MusicMenuManage.URI_MATCH_CODE) {
            return sMusicMenuManageName;
        }
        for (int i = 0; i < sMatchCode.size(); i++) {
            if (sMatchCode.get(i) == matchCode) {
                return sTableName.get(i);
            }
        }
        return null;
    }

    //根据歌单名，得到数据表名
    public static String getTableNameByMenuName(String musicMenuName) {
        for (int i = 0; i < sMusicMenuName.size(); i++) {
            if (sMusicMenuName.get(i).equals(musicMenuName)) {
                return sTableName.get(i);
            }
        }
        return null;
    }

    //根据歌单名，得到歌单表的匹配码
    public static int getMatchCodeByMenuName(String musicMenuName) {
        for (int i = 0; i < sMusicMenuName.size(); i++) {
            if (sMusicMenuName.get(i).equals(musicMenuName)) {
                return sMatchCode.get(i);
            }
        }
        return 0;
    }

    //得到歌单名
    public static String getMenuNameByTableName(String tableName) {
        for (int i = 0; i < sTableName.size(); i++)
            if (sTableName.get(i).equals(tableName))
                return sMusicMenuName.get(i);
        return null;
    }

    //生成一个新的匹配码
    private static int createMatchCode() {
        int newCode = startMatchCode;
        boolean flag = true;
        do {
            flag = true;
            for (int i = 0; i < sMatchCode.size(); i++)
                if (sMatchCode.get(i) == newCode) {
                    newCode++;
                    flag = false;
                    break;
                }
        } while (!flag);
        return newCode;
    }
}