package com.example.musicplayer.musicTools;

import com.example.musicplayer.musicClass.MusicInfo;
import com.example.musicplayer.musicClass.MusicMenu;

import java.util.ArrayList;
import java.util.List;

//处理歌曲排序的工具类
public class MusicSortTool {
    public static List<Integer> sort(String musicMenuName) {
        List<Integer> result = new ArrayList<Integer>();
        MusicMenu musicMenu = MusicInfo.getMusicMenuByName(musicMenuName);
        if (musicMenu == null)
            return result;
        //获取歌曲id
        result = musicMenu.getMusicList();
        //歌曲数量少于两首，不同排序
        if (result.size() <= 1)
            return result;
        //获取每首歌曲对应的顺序
        List<Integer> musicOrder = musicMenu.getMusicOrder();
        MusicSortType musicSortType = musicMenu.getMusicSortType();
        List<MusicInfo> musicInfo = new ArrayList<MusicInfo>();
        for (int i = 0; i < result.size(); i++)
            musicInfo.add(MusicInfo.getMusicInfoById(result.get(i)));
        //排序
        if (musicInfo.size() <= 3000)
            selectionSort(musicInfo, musicOrder, musicSortType);
        else
            quickSort(musicInfo, musicOrder, musicSortType, 0, musicInfo.size() - 1);

        result.clear();
        for (int i = 0; i < musicInfo.size(); i++)
            result.add(musicInfo.get(i).getId());
        return result;
    }

    //采用选择排序方式
    private static void selectionSort(List<MusicInfo> musicInfo, List<Integer> musicOrder, MusicSortType sortType) {
        for (int i = 0; i <= musicInfo.size() - 2; i++) {
            int index = i;
            for (int j = i + 1; j <= musicInfo.size() - 1; j++)
                if (shouldSwap(musicInfo, musicOrder, sortType, index, j))
                    index = j;
            if (index != i)
                swap(musicInfo, musicOrder, index, i);
        }
    }

    //采用快速排序方式
    private static void quickSort(List<MusicInfo> musicInfo, List<Integer> musicOrder, MusicSortType sortType
            , int low, int high) {
        if (low > high)
            return;
        int sentryLeft = low;
        int sentryRight = high;

        while (sentryLeft < sentryRight) {
            while (!shouldSwap(musicInfo, musicOrder, sortType, low, sentryRight) && sentryLeft < sentryRight)
                sentryRight--;
            while (shouldSwap(musicInfo, musicOrder, sortType, low, sentryLeft) && sentryLeft < sentryRight)
                sentryLeft++;
            if (sentryLeft < sentryRight)
                swap(musicInfo, musicOrder, sentryLeft, sentryRight);
        }

        swap(musicInfo, musicOrder, low, sentryLeft);
        quickSort(musicInfo, musicOrder, sortType, low, sentryRight - 1);
        quickSort(musicInfo, musicOrder, sortType, sentryRight + 1, high);
    }

    //根据歌单排序方式，比较musicInfo对象大小
    private static boolean shouldSwap(List<MusicInfo> musicInfo, List<Integer> musicOrder, MusicSortType sortType
            , int low, int high) {
        if (musicInfo == null || musicOrder == null || low >= high)
            return false;
        if (low < 0 || low >= musicInfo.size())
            return false;
        if (high >= musicInfo.size())
            return false;
        switch (sortType) {
            case SORT_DEFAULT_REVERSE:
                return musicOrder.get(low) < musicOrder.get(high);
            case SORT_TITLE:
            case SORT_ARTIST:
                return false;
            case SORT_LISTEN_COUNTS:
                if (musicInfo.get(low).getListenCounts() < musicInfo.get(high).getListenCounts())
                    return true;
                else if (musicInfo.get(low).getListenCounts() == musicInfo.get(high).getListenCounts())
                    return musicOrder.get(low) > musicOrder.get(high);
                else
                    return false;
            case SORT_DEFAULT:
            default:
                return musicOrder.get(low) > musicOrder.get(high);
        }
    }

    //交换两首歌曲信息的位置
    private static void swap(List<MusicInfo> musicInfo, List<Integer> musicOrder, int a, int b) {
        MusicInfo temp = musicInfo.get(a);
        int number = musicOrder.get(a);
        musicInfo.set(a, musicInfo.get(b));
        musicOrder.set(a, musicOrder.get(b));
        musicInfo.set(b, temp);
        musicOrder.set(b, number);
    }
}