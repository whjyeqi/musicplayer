package com.example.musicplayer.musicTools;

import android.content.res.Resources;

import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.ChineseUtil;
import com.example.musicplayer.settings.MusicPlaySettings;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Lyric {
    //表示用简体字
    public static final int SIMPLIFIED = 0;
    //表示用繁体字
    public static final int TRADITIONAL = 1;
    private static List<String> lyrics;
    private static List<Integer> lyricTime;

    //给定路径构建歌词
    public static boolean create(String lyricPath, Resources resources) {
        lyrics = new ArrayList<String>();
        lyricTime = new ArrayList<Integer>();
        BufferedReader bw;
        try {
            bw = new BufferedReader(new InputStreamReader(new FileInputStream(lyricPath), "GBK"));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        String str = null;
        try {
            while ((str = bw.readLine()) != null) {
                checkLyric(str, resources);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    //处理每次读进来的字符串
    private static void checkLyric(String string, Resources resources) {
        int index = string.lastIndexOf("]");
        String string1 = string.substring(0, index + 1);
        String string2 = string.substring(index + 1);
        if (string1.equals("[00:00:00]")) {
            addLyric(resources.getString(R.string.music_lyric_empty));
            lyricTime.add(0);
        } else if (string2.length() > 0) {
            addLyric(string2);
            lyricTime.add(convertToTime(string1));
        }
    }

    //把歌词文件的时间戳转换成毫秒数
    private static int convertToTime(String timeStamp) {
        int minute = Integer.parseInt(timeStamp.substring(1, 3));
        int second = Integer.parseInt(timeStamp.substring(4, 6));
        int millisecond = Integer.parseInt(timeStamp.substring(7, 9));
        return minute * 60000 + second * 1000 + millisecond * 10;
    }

    //得到歌词数组
    public static List<String> getLyrics() {
        return lyrics;
    }

    //得到存放歌词的数组的大小
    public static int getSize() {
        if (lyrics == null)
            return 0;
        return lyrics.size();
    }

    //得到指定位置的歌词
    public static String getLyric(int index) {
        if (index >= 0 && index < lyrics.size())
            return lyrics.get(index);
        return "";
    }

    //得到指定索引歌词的开始时间
    public static int getStartTime(int position) {
        if (position < 0 || position >= lyricTime.size())
            return Integer.MAX_VALUE;
        return lyricTime.get(position);
    }

    //简体繁体格式转换
    public static void changeChinese() {
        if (MusicPlaySettings.getLyricChinese() == SIMPLIFIED) {
            MusicPlaySettings.setLyricChinese(TRADITIONAL);
            changeToTraditional();
        } else if (MusicPlaySettings.getLyricChinese() == TRADITIONAL) {
            MusicPlaySettings.setLyricChinese(SIMPLIFIED);
            changeToSimplified();
        }
    }

    //歌词简转繁
    private static void changeToTraditional() {
        for (int i = 0; i < lyrics.size(); i++) {
            String string = lyrics.get(i);
            string = ChineseUtil.J2F(string);
            lyrics.set(i, string);
        }
    }

    //歌词繁转简
    private static void changeToSimplified() {
        for (int i = 0; i < lyrics.size(); i++) {
            String string = lyrics.get(i);
            string = ChineseUtil.F2J(string);
            lyrics.set(i, string);
        }
    }

    //初始化歌词并添加歌词时，判断显示的字体样式
    private static void addLyric(String string) {
        if (MusicPlaySettings.getLyricChinese() == TRADITIONAL)
            string = ChineseUtil.J2F(string);
        lyrics.add(string);
    }
}