package com.example.musicplayer.musicTools;

//音乐格式的类
public class MusicFormat {
    public static final int UNKNOWN = 0;
    public static final int MP3 = 1;
    public static final int FLAC = 2;
    private int mMusicFormat = UNKNOWN;

    public MusicFormat(String filePath) {
        String string = filePath.substring(filePath.lastIndexOf("."));
        //根据文件后缀名识别
        switch (string) {
            case ".mp3":
                mMusicFormat = MP3;
                break;
            case ".flac":
                mMusicFormat = FLAC;
                break;
            default:
                mMusicFormat = UNKNOWN;
                break;
        }
    }

    //返回音乐格式
    public int getMusicFormat() {
        return mMusicFormat;
    }

    //返回音乐格式的后缀名
    public String getMusicFormatSuffix() {
        switch (mMusicFormat) {
            case MP3:
                return ".mp3";
            case FLAC:
                return ".flac";
            default:
                return null;
        }
    }

    //判断是否格式无法识别
    public boolean isUnknown() {
        return mMusicFormat == UNKNOWN;
    }
}