package com.example.musicplayer.musicTools;

//音乐排序的类型
public enum MusicSortType {
    /**
     * 类型：默认方式
     */
    SORT_DEFAULT,
    /**
     * 类型：默认方式倒序
     */
    SORT_DEFAULT_REVERSE,
    /**
     * 类型：按歌曲名首字母
     */
    SORT_TITLE,
    /**
     * 类型：按歌手名首字母
     */
    SORT_ARTIST,
    /**
     * 类型：按播放量高低
     */
    SORT_LISTEN_COUNTS;
}