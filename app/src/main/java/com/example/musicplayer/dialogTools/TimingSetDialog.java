package com.example.musicplayer.dialogTools;

import android.app.Dialog;
import android.content.Context;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.musicplayer.R;

public class TimingSetDialog {
    private static final int NUMBER_LENGTH_LIMIT = 4;
    private static final int TIMING_SET_MAX = 24 * 60;
    private Dialog mDialog;
    private EditText mEditText;
    private Button mConfirm;

    public TimingSetDialog(Context context) {
        mDialog = new Dialog(context, R.style.NormalDialogStyle_Bottom);
        View view = View.inflate(context, R.layout.dialog_timing_set, null);
        mEditText = view.findViewById(R.id.dialog_edittext);
        mConfirm = view.findViewById(R.id.dialog_confirm);
        mDialog.setContentView(view);
        mDialog.setCanceledOnTouchOutside(true);
        Window dialogWindow = mDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = (int) context.getResources().getDimension(R.dimen.timing_set_dialog_height);
            lp.gravity = Gravity.BOTTOM;
            dialogWindow.setAttributes(lp);
        }
        mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(NUMBER_LENGTH_LIMIT)});
    }

    public int getTimingSetDuration() {
        String content = mEditText.getText().toString();
        try {
            int duration = Integer.parseInt(content);
            if (duration > TIMING_SET_MAX) {
                return -1;
            } else {
                return Math.max(duration, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public boolean isEmpty() {
        return mEditText.getText().toString().equals("");
    }

    public View getConfirmButton() {
        return mConfirm;
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }
}