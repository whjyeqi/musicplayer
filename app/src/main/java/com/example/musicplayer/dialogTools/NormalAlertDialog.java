package com.example.musicplayer.dialogTools;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.musicplayer.R;

//普通的提示对话框
public class NormalAlertDialog {
    private TextView mCancel;
    private TextView mConfirm;
    private Dialog mDialog;

    public NormalAlertDialog(Context context, String title, String text) {
        mDialog = new Dialog(context, R.style.NormalDialogStyle);
        View view = View.inflate(context, R.layout.dialog_normal_alert, null);
        mCancel = view.findViewById(R.id.dialog_normal_alert_cancel);
        mConfirm = view.findViewById(R.id.dialog_normal_alert_confirm);
        TextView textViewTitle = view.findViewById(R.id.dialog_normal_alert_title);
        TextView textViewText = view.findViewById(R.id.dialog_normal_alert_text);
        textViewTitle.setText(title);
        textViewText.setText(text);
        mDialog.setContentView(view);
        //设置点击对话框外部是否取消对话框
        mDialog.setCanceledOnTouchOutside(true);

        Window dialogWindow = mDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = (int) context.getResources().getDimension(R.dimen.dialog_normal_alert_width);
            //lp.height = (int) context.getResources().getDimension(R.dimen.dialog_normal_alert_height);
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER;
            dialogWindow.setAttributes(lp);
        }
    }

    public NormalAlertDialog(Context context, String title, String text, String leftButton, String rightButton) {
        this(context, title, text);
        mCancel.setText(leftButton);
        mConfirm.setText(rightButton);
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public TextView getCancel() {
        return mCancel;
    }

    public TextView getConfirm() {
        return mConfirm;
    }

    public View getLeftButton() {
        return mCancel;
    }

    public View getRightButton() {
        return mConfirm;
    }

    public void show() {
        if (mDialog != null) {
            mDialog.show();
        }
    }

    public void dismiss() {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }
}