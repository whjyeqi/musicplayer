package com.example.musicplayer.dialogTools;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.musicplayer.R;
import com.example.musicplayer.commonUtils.DisplayUtil;
import com.example.musicplayer.musicTools.MusicSortType;

//选择歌曲排序方式的对话框
public class SortMusicDialog {
    private Dialog mDialog;
    private View mSortDefault;
    private View mSortDefaultReverse;
    private View mSortTitle;
    private View mSortArtist;
    private View mSortListenCounts;
    private View mCancel;

    public SortMusicDialog(Context context, MusicSortType mCurrentSortType) {
        mDialog = new Dialog(context, R.style.NormalDialogStyle_Bottom);
        View view = View.inflate(context, R.layout.dialog_sort_music, null);
        mSortDefault = view.findViewById(R.id.sort_as_default);
        mSortDefaultReverse = view.findViewById(R.id.sort_as_default_reverse);
        mSortTitle = view.findViewById(R.id.sort_as_title);
        mSortArtist = view.findViewById(R.id.sort_as_artist);
        mSortListenCounts = view.findViewById(R.id.sort_as_listen_counts);
        mCancel = view.findViewById(R.id.dialog_sort_music_cancel);
        mDialog.setContentView(view);

        //设置点击对话框外部是否取消对话框
        mDialog.setCanceledOnTouchOutside(true);
        Window dialogWindow = mDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = (int) context.getResources().getDimension(R.dimen.dialog_sort_music_height);
            lp.gravity = Gravity.BOTTOM;
            dialogWindow.setAttributes(lp);
        }

        View imageView = null;
        switch (mCurrentSortType) {
            case SORT_DEFAULT_REVERSE:
                if ((imageView = view.findViewById(R.id.imageview_default_reverse)) != null)
                    imageView.setVisibility(View.VISIBLE);
                break;
            case SORT_TITLE:
                if ((imageView = view.findViewById(R.id.imageview_title)) != null)
                    imageView.setVisibility(View.VISIBLE);
                break;
            case SORT_ARTIST:
                if ((imageView = view.findViewById(R.id.imageview_artist)) != null)
                    imageView.setVisibility(View.VISIBLE);
                break;
            case SORT_LISTEN_COUNTS:
                if ((imageView = view.findViewById(R.id.imageview_listen_counts)) != null)
                    imageView.setVisibility(View.VISIBLE);
                break;
            case SORT_DEFAULT:
            default:
                if ((imageView = view.findViewById(R.id.imageview_default)) != null)
                    imageView.setVisibility(View.VISIBLE);
                break;
        }
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public View getSortDefault() {
        return mSortDefault;
    }

    public View getSortDefaultReverse() {
        return mSortDefaultReverse;
    }

    public View getSortTitle() {
        return mSortTitle;
    }

    public View getSortArtist() {
        return mSortArtist;
    }

    public View getSortListenCounts() {
        return mSortListenCounts;
    }

    public View getCancel() {
        return mCancel;
    }
}