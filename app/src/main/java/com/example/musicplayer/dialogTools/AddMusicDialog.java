package com.example.musicplayer.dialogTools;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.musicplayer.R;

//添加音乐的选择对话框
public class AddMusicDialog {
    private TextView mFavoriteMusic;
    private TextView mMusicMenu;
    private TextView mPlayList;
    private TextView mCancel;
    private Dialog mDialog;

    public AddMusicDialog(Context context) {
        mDialog = new Dialog(context, R.style.NormalDialogStyle_Bottom);
        View view = View.inflate(context, R.layout.dialog_add_music, null);
        mFavoriteMusic = view.findViewById(R.id.dialog_add_music_favorite);
        mMusicMenu = view.findViewById(R.id.dialog_add_music_music_menu);
        mPlayList = view.findViewById(R.id.dialog_add_music_play_list);
        mCancel = view.findViewById(R.id.dialog_add_music_cancel);
        mDialog.setContentView(view);
        //设置点击对话框外部是否取消对话框
        mDialog.setCanceledOnTouchOutside(true);
        Window dialogWindow = mDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = (int) context.getResources().getDimension(R.dimen.dialog_add_music_height);
            lp.gravity = Gravity.BOTTOM;
            dialogWindow.setAttributes(lp);
        }
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public TextView getFavoriteMusic() {
        return mFavoriteMusic;
    }

    public TextView getMusicMenu() {
        return mMusicMenu;
    }

    public TextView getPlayList() {
        return mPlayList;
    }

    public TextView getCancel() {
        return mCancel;
    }
}