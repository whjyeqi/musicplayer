package com.example.musicplayer.viewTools;

import android.view.View;
import android.widget.LinearLayout;

import com.example.musicplayer.view.SimpleDisplayView;

import java.util.ArrayList;
import java.util.List;

//拖拽改变view的排列顺序
public class AdjustViewOrder {
    private List<SimpleDisplayView> mDisplay = new ArrayList<SimpleDisplayView>();
    private int mTargetNumber;
    //调整结束时的位置
    private int mEndNumber;
    private int mDragAreaHeight;
    private int mViewHeight;
    //最大和最小的marginTop
    private int maxMarginTop;
    private int minMarginTop;

    public AdjustViewOrder(List<SimpleDisplayView> list, int targetNumber, int dragAreaHeight) {
        mDisplay = list;
        mTargetNumber = targetNumber;
        mEndNumber = mTargetNumber;
        mDragAreaHeight = dragAreaHeight;
        mViewHeight = mDisplay.get(0).getViewHeight();
        maxMarginTop = (mDisplay.size() - 1 - targetNumber) * mViewHeight;
        if (targetNumber > 0)
            minMarginTop = -(targetNumber + 1) * mViewHeight;
        else
            minMarginTop = 0;
    }

    //拖动事件触发时调用
    public int adjustView(float startY, float nowY, boolean isMove) {
        float dy = nowY - startY;
        int targetViewMarginTop = adjustMarginTop(dy);
        int counts = Math.abs(targetViewMarginTop) / mViewHeight;
        int restDistance = Math.abs(targetViewMarginTop) - mViewHeight * counts;
        if (targetViewMarginTop > 0) {
            if (counts > 0) {
                if (isMove) {
                    adjustMarginTop(mTargetNumber + 1, -((counts + 1) * mViewHeight + restDistance));
                    adjustMarginTop(mTargetNumber + 1 + counts, mViewHeight - restDistance);
                    adjustMarginTop(mTargetNumber + 2 + counts, restDistance);
                } else {
                    if (restDistance <= mViewHeight / 2) {
                        adjustMarginTop(mTargetNumber, counts * mViewHeight);
                        adjustMarginTop(mTargetNumber + 1, -(counts + 1) * mViewHeight);
                        adjustMarginTop(mTargetNumber + 1 + counts, mViewHeight);
                        adjustMarginTop(mTargetNumber + 2 + counts, 0);
                        mEndNumber = mTargetNumber + counts;
                    } else {
                        adjustMarginTop(mTargetNumber, (counts + 1) * mViewHeight);
                        adjustMarginTop(mTargetNumber + 1, -(counts + 2) * mViewHeight);
                        adjustMarginTop(mTargetNumber + 1 + counts, 0);
                        adjustMarginTop(mTargetNumber + 2 + counts, mViewHeight);
                        mEndNumber = mTargetNumber + counts + 1;
                    }
                }
            } else {
                if (isMove) {
                    adjustMarginTop(mTargetNumber + 1, -restDistance * 2);
                    adjustMarginTop(mTargetNumber + 2, restDistance);
                } else {
                    if (restDistance <= mViewHeight / 2) {
                        adjustMarginTop(mTargetNumber, 0);
                        adjustMarginTop(mTargetNumber + 1, 0);
                        adjustMarginTop(mTargetNumber + 2, 0);
                        mEndNumber = mTargetNumber;
                    } else {
                        adjustMarginTop(mTargetNumber, mViewHeight);
                        adjustMarginTop(mTargetNumber + 1, -mViewHeight * 2);
                        adjustMarginTop(mTargetNumber + 2, mViewHeight);
                        mEndNumber = mTargetNumber + 1;
                    }
                }
            }
        } else {
            if (counts > 1) {
                if (isMove) {
                    adjustMarginTop(mTargetNumber + 1, Math.abs(targetViewMarginTop) - mViewHeight);
                    adjustMarginTop(mTargetNumber - counts + 1, mViewHeight - restDistance);
                    adjustMarginTop(mTargetNumber - counts, restDistance);
                } else {
                    if (restDistance <= mViewHeight / 2) {
                        adjustMarginTop(mTargetNumber, -counts * mViewHeight);
                        adjustMarginTop(mTargetNumber + 1, (counts - 1) * mViewHeight);
                        adjustMarginTop(mTargetNumber - counts + 1, mViewHeight);
                        adjustMarginTop(mTargetNumber - counts, 0);
                        mEndNumber = mTargetNumber - counts + 1;
                    } else {
                        adjustMarginTop(mTargetNumber, -(counts + 1) * mViewHeight);
                        adjustMarginTop(mTargetNumber + 1, counts * mViewHeight);
                        adjustMarginTop(mTargetNumber - counts + 1, 0);
                        adjustMarginTop(mTargetNumber - counts, mViewHeight);
                        mEndNumber = mTargetNumber - counts;
                    }
                }
            } else {
                restDistance = -targetViewMarginTop;
                if (isMove) {
                    adjustMarginTop(mTargetNumber + 1, restDistance / 2);
                    adjustMarginTop(mTargetNumber - 1, restDistance / 2);
                } else {
                    if (restDistance <= mViewHeight) {
                        adjustMarginTop(mTargetNumber, 0);
                        adjustMarginTop(mTargetNumber + 1, 0);
                        adjustMarginTop(mTargetNumber - 1, 0);
                        mEndNumber = mTargetNumber;
                    } else {
                        adjustMarginTop(mTargetNumber, mViewHeight * 2);
                        adjustMarginTop(mTargetNumber + 1, mViewHeight);
                        adjustMarginTop(mTargetNumber - 1, mViewHeight);
                        mEndNumber = mTargetNumber - 1;
                    }
                }
            }
        }
        if (!isMove) {
            adjustMarginTop();
        }
        return mEndNumber;
    }

    //调整拖拽的view的margin
    private int adjustMarginTop(float dy) {
        if (dy > 0)
            dy = dy > maxMarginTop ? maxMarginTop : dy;
        else {
            dy = dy >= -mViewHeight ? dy * 2 : (dy - mViewHeight);
            dy = dy < minMarginTop ? minMarginTop : dy;
        }
        adjustMarginTop(mTargetNumber, (int) dy);
        return (int) dy;
    }

    //调整上间距
    private void adjustMarginTop(int index, int marginTop) {
        if (index >= 0 && index < mDisplay.size()) {
            View view = mDisplay.get(index).getView();
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
            lp.topMargin = marginTop;
            view.setLayoutParams(lp);
        }
    }

    //结束时调整间距
    private void adjustMarginTop() {
        for (int i = 0; i < mDisplay.size(); i++)
            adjustMarginTop(i, 0);
    }
}