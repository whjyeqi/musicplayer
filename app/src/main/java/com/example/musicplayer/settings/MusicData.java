package com.example.musicplayer.settings;

import com.example.musicplayer.musicTools.MusicListType;
import com.example.musicplayer.service.MusicService;

import java.util.ArrayList;
import java.util.List;

//记录当前音乐数据信息
public class MusicData {
    private String mMusicListType;
    private String mPlayMode;
    private String mMusicMenuName;
    private int mMusicId;
    private int mCurrentTime;
    private List<Integer> mPlayList = new ArrayList<Integer>();

    public MusicData(String musicListType, String playMode, String musicMenuName, int musicId,
                     int currentTime, List<Integer> playList) {
        mMusicListType = musicListType == null ? MusicListType.TYPE_LOCAL.toString() : musicListType;
        mPlayMode = playMode == null ? MusicService.PlayMode.RANDOM.toString() : playMode;
        mMusicMenuName = musicMenuName == null ? " " : musicMenuName;
        mMusicId = musicId;
        mCurrentTime = currentTime;
        if (playList != null)
            mPlayList = playList;
    }

    public String getMusicListType() {
        if (mMusicListType == null || (!mMusicListType.equals(MusicListType.TYPE_FAVORITE.toString()) &&
                !mMusicListType.equals(MusicListType.TYPE_LOCAL.toString()) &&
                !mMusicListType.equals(MusicListType.TYPE_MENU.toString()) &&
                !mMusicListType.equals(MusicListType.TYPE_PURCHASED.toString())))
            mMusicListType = MusicListType.TYPE_LOCAL.toString();
        return mMusicListType;
    }

    public String getPlayMode() {
        if (mPlayMode == null || (!mPlayMode.equals(MusicService.PlayMode.RANDOM.toString()) &&
                !mPlayMode.equals(MusicService.PlayMode.REPEAT.toString()) &&
                !mPlayMode.equals(MusicService.PlayMode.REPEAT_ONE.toString())))
            mPlayMode = MusicService.PlayMode.RANDOM.toString();
        return mPlayMode;
    }

    public String getMusicMenuName() {
        return mMusicMenuName;
    }

    public int getMusicId() {
        return mMusicId;
    }

    public int getCurrentTime() {
        return Math.max(0, mCurrentTime);
    }

    public List<Integer> getPlayList() {
        return mPlayList;
    }
}