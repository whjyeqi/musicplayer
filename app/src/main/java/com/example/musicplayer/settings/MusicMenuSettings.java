package com.example.musicplayer.settings;

//歌单相关的设置
public class MusicMenuSettings {
    //歌单名的最大长度
    private static int sLengthOfMenuName = 20;
    //歌单详情使用location的最小歌曲数
    private static int sLeastMusicCountsUseLocation = 20;

    public synchronized static void setLengthOfMenuName(int lengthOfMenuName) {
        if (lengthOfMenuName > 0)
            sLengthOfMenuName = lengthOfMenuName;
    }

    public synchronized static int getLengthOfMenuName() {
        return sLengthOfMenuName;
    }

    public synchronized static void setLeastMusicCountsUseLocation(int leastMusicCountsUseLocation) {
        if (leastMusicCountsUseLocation >= 1)
            sLeastMusicCountsUseLocation = leastMusicCountsUseLocation;
    }

    public synchronized static int getLeastMusicCountsUseLocation() {
        return sLeastMusicCountsUseLocation;
    }
}