package com.example.musicplayer.settings;

import android.content.res.Resources;

import com.example.musicplayer.MusicPlayerApplication;
import com.example.musicplayer.R;
import com.example.musicplayer.view.SettingsView;

public class SettingsItem {
    public static final int MUSIC_SETTINGS = 0;
    public static final int PERSONAL_DRESS_UP_CENTER = 1;
    public static final int OPEN_PLATFORM = 2;
    public static final int FREE_SERVICE = 3;
    public static final int TICKETING = 4;
    public static final int COLOR_RING = 5;
    public static final int TIME_TO_EXIT = 6;
    public static final int LISTEN_TO_SONGS = 7;

    public static final int ONLY_WIFI = 100;
    public static final int FLOW_REMIND = 101;
    public static final int FLUENCY_SETTINGS = 102;
    public static final int NOTIFICATION = 103;
    public static final int PERSONAL_INFO = 104;

    public static final int TIMED_OFF_IMMEDIATE = 200;

    public static final int USER_PHOTO = 300;
    public static final int USER_NAME = 301;
    public static final int USER_EMAIL = 302;

    public static SettingsView createSettingsItem(int settingsItem) {
        Resources resources = MusicPlayerApplication.getInstance().getResources();
        SettingsView view = null;
        String title = "";
        String summary = null;
        String description = null;
        boolean showSwitch = false;
        switch (settingsItem) {
            case MUSIC_SETTINGS:
                title = resources.getString(R.string.music_settings);
                break;
            case PERSONAL_DRESS_UP_CENTER:
                title = resources.getString(R.string.personal_dress_up_center);
                description = resources.getString(R.string.personal_dress_up_center_des);
                break;
            case OPEN_PLATFORM:
                title = resources.getString(R.string.open_platform);
                break;
            case FREE_SERVICE:
                title = resources.getString(R.string.free_service);
                description = resources.getString(R.string.free_service_des);
                break;
            case TICKETING:
                title = resources.getString(R.string.ticketing);
                break;
            case COLOR_RING:
                title = resources.getString(R.string.color_ring);
                description = resources.getString(R.string.color_ring_des);
                break;
            case TIME_TO_EXIT:
                title = resources.getString(R.string.time_to_exit);
                showSwitch = true;
                break;
            case LISTEN_TO_SONGS:
                title = resources.getString(R.string.listen_to_songs);
                break;
            case ONLY_WIFI:
                title = resources.getString(R.string.only_wifi);
                showSwitch = true;
                break;
            case FLOW_REMIND:
                title = resources.getString(R.string.flow_remind);
                showSwitch = true;
                break;
            case FLUENCY_SETTINGS:
                title = resources.getString(R.string.fluency_settings);
                break;
            case NOTIFICATION:
                title = resources.getString(R.string.notification_settings);
                break;
            case PERSONAL_INFO:
                title = resources.getString(R.string.personal_info);
                break;
            case TIMED_OFF_IMMEDIATE:
                title = resources.getString(R.string.timed_off_style_des_2);
                showSwitch = true;
                break;
            case USER_PHOTO:
                title = resources.getString(R.string.user_photo);
                break;
            case USER_NAME:
                title = resources.getString(R.string.user_nick_name);
                break;
            case USER_EMAIL:
                title = resources.getString(R.string.user_email);
                break;
            default:
                title = null;
                break;
        }
        if (title != null) {
            view = new SettingsView(title, summary, description, showSwitch, settingsItem);
        }
        return view;
    }

    public static class TimedOffItem {
        public static final int TIMING_SET = 0;
        public static final int TIMING_15 = 1;
        public static final int TIMING_30 = 2;
        public static final int TIMING_45 = 3;
        public static final int TIMING_60 = 4;

        public static final int TIMING_DEFAULT = TIMING_30;

        public static int getTimeDuration(int timingSet) {
            switch (timingSet) {
                case TIMING_15:
                    return 15 * 60 * 1000;
                case TIMING_30:
                    return 30 * 60 * 1000;
                case TIMING_45:
                    return 45 * 60 * 1000;
                case TIMING_60:
                    return 60 * 60 * 1000;
                case TIMING_SET:
                default:
                    return 0;
            }
        }
    }
}