package com.example.musicplayer.settings;

public class MainActivitySettings {
    //表示顶部tab栏中，选中的tab距离屏幕左右边的最小距离
    private static int sGapToEdge = 100;
    //歌曲播放次数增加的最小收听时间
    private static int sTimeToIncreaseListenCounts = 30 * 1000;
    //用户名的最大长度
    private static int sLengthOfUserName = 20;

    public synchronized static void setGapToEdge(int gapToEdge) {
        sGapToEdge = gapToEdge;
    }

    public synchronized static int getGapToEdge() {
        return sGapToEdge;
    }

    public synchronized static void setTimeToIncreaseListenCounts(int timeToIncreaseListenCounts) {
        if (timeToIncreaseListenCounts > 0)
            sTimeToIncreaseListenCounts = timeToIncreaseListenCounts;
    }

    public synchronized static int getTimeToIncreaseListenCounts() {
        return sTimeToIncreaseListenCounts;
    }

    public synchronized static void setLengthOfUserName(int lengthOfUserName) {
        if (lengthOfUserName > 0) {
            sLengthOfUserName = lengthOfUserName;
        }
    }

    public synchronized static int getLengthOfUserName() {
        return sLengthOfUserName;
    }
}