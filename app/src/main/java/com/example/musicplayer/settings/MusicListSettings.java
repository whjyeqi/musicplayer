package com.example.musicplayer.settings;

//MusicListActivity的一些数据设置
public class MusicListSettings {
    //返回顶端按钮出现的条件，listview的第一个可见item的索引值
    private static int sBackToTopButtonAppear = 10;

    public synchronized static void setBackToTopButtonAppear(int backToTopButtonAppear) {
        if (backToTopButtonAppear >= 0)
            sBackToTopButtonAppear = backToTopButtonAppear;
    }

    public synchronized static int getBackToTopButtonAppear() {
        return sBackToTopButtonAppear;
    }
}