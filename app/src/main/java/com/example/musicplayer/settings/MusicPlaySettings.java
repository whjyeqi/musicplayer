package com.example.musicplayer.settings;

import com.example.musicplayer.musicTools.Lyric;

//MusicPlayActivity的一些数据设置
public class MusicPlaySettings {
    //图片主色的透明度
    private static int sAlphaOfPictureMainColor = 222;
    //记录歌词的简体和繁体状态
    private static int sLyricChinese = Lyric.SIMPLIFIED;

    public synchronized static void setAlphaOfPictureMainColor(int alpha) {
        if (alpha >= 0 && alpha <= 255)
            sAlphaOfPictureMainColor = alpha;
    }

    public synchronized static int getAlphaOfPictureMainColor() {
        return sAlphaOfPictureMainColor;
    }

    public synchronized static void setLyricChinese(int lyricChinese) {
        if (lyricChinese == Lyric.SIMPLIFIED || lyricChinese == Lyric.TRADITIONAL)
            sLyricChinese = lyricChinese;
    }

    public synchronized static int getLyricChinese() {
        return sLyricChinese;
    }
}