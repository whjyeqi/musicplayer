package com.example.musicplayer;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.musicplayer.activity.BaseActivity;
import com.example.musicplayer.activity.LoginActivity;
import com.example.musicplayer.activity.MainActivity;
import com.example.musicplayer.activity.MusicPlayActivity;
import com.example.musicplayer.commonUtils.DrawUtil;
import com.example.musicplayer.commonUtils.ToastUtil;
import com.example.musicplayer.dialogTools.NormalAlertDialog;
import com.example.musicplayer.service.MusicTabViewService;
import com.example.musicplayer.user.UserManage;

public class MusicPlayerApplication extends Application {
    private static final String TAG = "MusicPlayerApplication";
    private static int sRefreshTime;
    private static int sAnimationTime;
    private static int sAppAnimationTime;
    private static MusicPlayerApplication sInstance;
    private MusicPlayerLifecycleCallbacks mLifecycleCallbacks;
    private Bitmap mScreenShot;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        sRefreshTime = sInstance.getResources().getInteger(R.integer.music_tab_view_refresh_time);
        sAnimationTime = sInstance.getResources().getInteger(R.integer.music_tab_view_animation_time);
        sAppAnimationTime = sInstance.getResources().getInteger(R.integer.app_animation_time);
        //注册回调监听
        mLifecycleCallbacks = new MusicPlayerLifecycleCallbacks();
        this.registerActivityLifecycleCallbacks(mLifecycleCallbacks);
    }

    public static MusicPlayerApplication getInstance() {
        return sInstance;
    }

    //判断应用是否位于前台
    public boolean isForeground() {
        return mLifecycleCallbacks.getForeground();
    }

    //退出应用
    public void exit() {
        //退出前保存数据
        if (DataLoadManager.getInstance() != null) {
            DataLoadManager.getInstance().writeBaseData();
        }
        ActivityStackManager.getInstance().exit();
    }

    //exit login state
    public void exitLogin() {
        UserManage.exitLoginState();
        Activity activity;
        ActivityStackManager manager = ActivityStackManager.getInstance();
        while (manager.getStackSize() > 0) {
            activity = manager.getTopActivity();
            if (activity != null) {
                if (activity instanceof MainActivity) {
                    activity.startActivity(new Intent(activity, LoginActivity.class));
                }
                if (!activity.isFinishing()) {
                    activity.finish();
                }
                manager.removeActivity(activity);
            }
        }
    }

    public void jumpToMusicPlayActivity() {
        Activity currentActivity = ActivityStackManager.getInstance().getTopActivity();
        if (currentActivity != null) {
            if (!(currentActivity instanceof BaseActivity) || !((BaseActivity) currentActivity).isEnableMusicTabView()) {
                ToastUtil.makeToast(getString(R.string.jump_to_music_play_activity_error));
            } else {
                mScreenShot = DrawUtil.getScreenShot(currentActivity);
                Intent intent = new Intent(currentActivity, MusicPlayActivity.class);
                currentActivity.startActivity(intent);
                currentActivity.overridePendingTransition(R.anim.music_play_activity_enter, R.anim.fake_anim);
            }
        }
    }

    public MainActivity getMainActivity() {
        Activity activity = ActivityStackManager.getInstance().getMainActivity();
        if (activity instanceof MainActivity) {
            return (MainActivity) activity;
        }
        return null;
    }

    public Activity getTopActivity() {
        return ActivityStackManager.getInstance().getTopActivity();
    }

    public Binder getMusicServiceBinder() {
        MainActivity mainActivity = getMainActivity();
        if (mainActivity != null) {
            return mainActivity.tryGetMusicBinder();
        }
        return null;
    }

    public Binder getMusicTabViewServiceBinder() {
        MainActivity mainActivity = getMainActivity();
        if (mainActivity != null) {
            return mainActivity.tryGetMusicTabViewBinder();
        }
        return null;
    }

    public Bitmap getScreenShot() {
        return mScreenShot;
    }

    public void clearScreenShot() {
        mScreenShot = null;
    }

    public static int getRefreshTime() {
        return sRefreshTime;
    }

    public static int getAnimationTime() {
        return sAnimationTime;
    }

    public static int getAppAnimationTime() {
        return sAppAnimationTime;
    }

    public static int getSettingsExpandViewAnimTime() {
        return sInstance.getResources().getInteger(R.integer.settings_expand_view_anim_time);
    }

    public static void showTimedOffPromptDialog() {
        final Activity activity = sInstance.getTopActivity();
        if (activity instanceof BaseActivity) {
            final NormalAlertDialog dialog = new NormalAlertDialog(activity
                    , activity.getString(R.string.prompt)
                    , activity.getString(R.string.timing_set_achieve)
                    , activity.getString(R.string.timing_set_achieve_dialog_left)
                    , activity.getString(R.string.timing_set_achieve_dialog_right));
            dialog.getLeftButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    sInstance.exit();
                }
            });
            dialog.getRightButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    ((BaseActivity) activity).notExitTemporarily();
                }
            });
            dialog.show();
        }
    }

    public static boolean canDrawOverlays() {
        if (Build.VERSION.SDK_INT >= 23) {
            return Settings.canDrawOverlays(getInstance());
        }
        return false;
    }

    //activity声明周期监听的类
    private static class MusicPlayerLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
        private int mCounts = 0;
        //表示应用是否位于前台
        private boolean mIsForeground = true;

        @Override
        public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
            Log.d(TAG, "onActivityCreated(): " + activity.getLocalClassName());
            ActivityStackManager.getInstance().addActivity(activity);
        }

        @Override
        public void onActivityStarted(@NonNull Activity activity) {
            Log.d(TAG, "onActivityStarted(): " + activity.getLocalClassName());
            changeCounts(true);
            setForeground(true);
        }

        @Override
        public void onActivityResumed(@NonNull Activity activity) {
            Log.d(TAG, "onActivityResumed(): " + activity.getLocalClassName());
        }

        @Override
        public void onActivityPaused(@NonNull Activity activity) {
            Log.d(TAG, "onActivityPaused(): " + activity.getLocalClassName());
        }

        @Override
        public void onActivityStopped(@NonNull Activity activity) {
            Log.d(TAG, "onActivityStopped(): " + activity.getLocalClassName());
            if (changeCounts(false) == 0) {
                setForeground(false);
                String text = getInstance().getString(R.string.app_name) +
                        getInstance().getString(R.string.app_switch_to_background);
                //ToastUtil.makeToast(text);
                Binder binder = getInstance().getMusicTabViewServiceBinder();
                if (binder != null) {
                    ((MusicTabViewService.MusicTabViewBinder) binder).shouldShowView(false);
                }
            }
        }

        @Override
        public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
            Log.d(TAG, "onActivitySaveInstanceState(): " + activity.getLocalClassName());
            if (getInstance().getMainActivity() != null) {
                getInstance().getMainActivity().saveMusicPlayState();
            }
        }

        @Override
        public void onActivityDestroyed(@NonNull Activity activity) {
            Log.d(TAG, "onActivityDestroyed(): " + activity.getLocalClassName());
            ActivityStackManager.getInstance().removeActivity(activity);
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).saveMusicPlayState();
                //ToastUtil.makeToast("退出QQ音乐");
            }
        }

        private synchronized int changeCounts(boolean isIncrease) {
            if (isIncrease)
                mCounts++;
            else
                mCounts--;
            return mCounts;
        }

        private synchronized void setForeground(boolean isForeground) {
            mIsForeground = isForeground;
        }

        public synchronized boolean getForeground() {
            return mIsForeground;
        }
    }
}